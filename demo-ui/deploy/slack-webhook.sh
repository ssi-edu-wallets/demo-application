#!/bin/bash
PROJECT="assessment-frontend"
EMOJI=":construction_worker:"
CHANNEL="C029JLHFC6M"
IMG="https://www.my.gov.sa/wps/wcm/connect/453f310f-62ec-4141-8e58-b0f4dbde0de3/H3gliEvl_400x400.jpg?MOD=AJPERES"
#REPOSITORY_URI=""
#DATE_IMG_TAG=""
curl --request POST \
  --url https://hooks.slack.com/services/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 \
  --header 'Content-Type: application/json' \
  --data "{
  \"text\": \"BUILD ${PROJECT} ${DATE_IMG_TAG}\",
  \"username\": \"BUILD ${PROJECT}\",
  \"icon_emoji\": \"${EMOJI}\",
  \"channel\": \"${CHANNEL}\",
  \"blocks\": [
    {
      \"type\": \"section\",
      \"block_id\": \"section567\",
      \"text\": {
        \"type\": \"mrkdwn\",
        \"text\": \"Build finished. \n :white_check_mark: \`${REPOSITORY_URI}\` : ${DATE_IMG_TAG} \n <https://eu-west-1.console.aws.amazon.com/codesuite/codepipeline/pipelines/${PROJECT}/view?region=eu-west-1|check AWS pipeline>\"
      },
      \"accessory\": {
        \"type\": \"image\",
        \"image_url\": \"${IMG}\",
        \"alt_text\": \"Alt image text\"
      }
    }
  ]
  }"
