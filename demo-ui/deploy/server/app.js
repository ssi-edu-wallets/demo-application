const express = require('express');
const app = express();
const path = require('path');
const fs = require('fs');
const cheerio = require('cheerio');

const basePath = path.join(__dirname, '../../dist')
const appPath = '/' + process.env.VUE_APP_SUBPATH

const appEnv = JSON.stringify({
  VUE_APP_SUBPATH: process.env.VUE_APP_SUBPATH,
  VUE_APP_PLATFORM: process.env.VUE_APP_PLATFORM
})

app.get(appPath + '*', function (req, res) {
  fs.readFile(path.join(basePath, 'index.html'), 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    const $cheerio = cheerio.load(data)
    $cheerio('#customBeforeApp').replaceWith('<div id="replacedBeforeApp" style="display:none;" data-env=\'' + appEnv + '\'></div>')
    res.set('Content-Type', 'text/html; charset=utf-8');
    return res.send($cheerio.html());
  });
});

app.use(express.static(basePath));

app.set('port', process.env.PORT || 8080);
app.listen(app.get('port'), function () {
  console.log('Express app listening on ' + appPath + ':' + app.get('port'));
});
