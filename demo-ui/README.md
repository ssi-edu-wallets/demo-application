# Module Occupation Matching 
## ASSESSMENT frontend

Competency analysis tool (KYC)

## General info

#### Global dependencies:
- Docker and [docker-compose](https://docs.docker.com/compose/) Use docker images or run with node.js natively
- [Node.js](https://nodejs.org/en/) Recommended to install using [NVM](https://github.com/nvm-sh/nvm) and running `nvm install`
- [yarn](https://classic.yarnpkg.com/en/docs/install/#debian-stable) (package manager used instead of npm)
- **git** as version control system using [rebase](https://git-scm.com/book/en/v2/Git-Branching-Rebasing) instead of merge.
- The backend API is available in this same repository

#### Frameworks:
- [VueJS](https://vuejs.org/) main javascript framework
- [Vuetify](https://vuetifyjs.com/en/introduction/why-vuetify/) UI framework with handy components
- [TypeScript](https://www.typescriptlang.org/) As programming language

#### Environment variables:
- `VUE_APP_API` Location for the API service
- `VUE_APP_SUBPATH` Subdomain where the app is going to be served
- `VUE_APP_PLATFORM` To be used as platform identifier (currently not used)
- `VUE_APP_TARGET_PROXY` Real domain to connect to the backend API after proxy forward
- `DATE_IMG_TAG` Version number

#### Platform customizations:
- **Configuration endpoint** `/api/v2/configs/mom_front` should return from database a list of entries to allow platform configuration
- Custom **themes** can be added in the config endpoint using the key `color_theme`, adding the files to `/pluings/themes/<lightTheme>.ts` and importing them into `const themesIndex` from `/pluings/themes/index.ts`
- Custom **translations** can be added in the config endpoint using the key `custom_locale` and adding the files to `/pluings/i18n/<lang>-<platform>.ts`

## Build Setup

Useful notes when running development built:

- API requests are been sent via a proxy forward defined with `VUE_APP_TARGET_PROXY` to avoid CORS issues.
- Login action in production is different that in development
- Logout action is not implemented in development (in order to logout, cookies have to be removed manually)
- Related links are going to work different (while in development they are going to look like localhost:8080, when deployed to production those relative links are going to change to the real production domain)


#### With Docker:

[Docker compose](https://docs.docker.com/compose/) file is ready to use, just run `docker-compose -f ./docker-compose.yml up -d`.

When working with `docker-compose.yml` it is important to change the environment variable `VUE_APP_TARGET_PROXY` to the correct domain where the API endpoints are available.

You can access the login page by default going to http://localhost:8080/assess/en/login where `assess` is defined with environment variable `VUE_APP_SUBPATH`.

#### Without Docker:

Alternatively you can work without docker with the following commands:

Install dependencies:
``` bash
yarn install
```

Build and hot-reload for development:
``` bash
yarn dev
```

Build and minify for production:
``` bash
yarn build
```

Lint:
``` bash
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## License

The source code for the site is licensed under the **MIT license**, which you can find in the LICENSE file included in this repository.
