import lordIconNames from './lord-icon-names'

const baseUrl = 'https://dot0z5gt50bw8.cloudfront.net/s/lordi/'

const formatName = (name: string): string => {
  const regex = /^[0-9-]*|-outline|-morph|.json$/gi
  return name.replace(regex, '')
}

const buildObjet = () => {
  const objNames: any = {}
  lordIconNames.names?.forEach((name: string) => {
    objNames[formatName(name)] = {
      src: baseUrl + name
    }
  })
  return objNames
}

const toRet = buildObjet()

export default {
  ...toRet
}
