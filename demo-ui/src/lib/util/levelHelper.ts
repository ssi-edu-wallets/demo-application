import LevelIndicator from '../../assets/interfaces/LevelIndicator'
import store from '../../store'
import { ADVANCE_STATUS, HIGH_STATUS, LOW_STATUS, MED_STATUS } from '../../services/constants'
import { Questionnaire } from '../../store/modules/questionnaire/types'

const levelOptions = (context: any, value: number, questionnaire?: Questionnaire): LevelIndicator => {
  const map = {
    kiraw: {
      breakpoints: [0, 25, 75],
      low: {
        arrayColor: [254, 74, 49],
        img: 'https://d1dwcvwxyxvs12.cloudfront.net/s/img/thumbs/thumb_down_red.png',
        rotate: 90
      },
      mid: {
        arrayColor: [247, 168, 49],
        img: 'https://d1dwcvwxyxvs12.cloudfront.net/s/img/thumbs/thumb_right_yellow.png',
        rotate: 180
      },
      adv: {
        arrayColor: [48, 135, 89],
        img: 'https://d1dwcvwxyxvs12.cloudfront.net/s/img/thumbs/kira_icon_learn_green.png',
        rotate: 0
      },
      hig: {
        arrayColor: [130, 177, 255],
        img: 'https://d1dwcvwxyxvs12.cloudfront.net/s/img/thumbs/thumb_down_blue.png',
        rotate: 225
      }
    },
    default: {
      breakpoints: [50, 75, 90],
      low: {
        arrayColor: [255, 72, 72],
        img: '',
        rotate: 180
      },
      mid: {
        arrayColor: [255, 192, 0],
        img: '',
        rotate: 225
      },
      adv: {
        arrayColor: [112, 173, 71],
        img: '',
        rotate: 45
      },
      hig: {
        arrayColor: [112, 173, 71],
        img: '',
        rotate: 0
      }
    }
  }
  const isSimpleUi = store.getters['config/getConfig'].is_simple_ui === '1'
  const current = isSimpleUi ? map.kiraw : map.default
  if (value > Number.NEGATIVE_INFINITY && value <= current.breakpoints[0]) {
    return {
      arrayColor: current.low.arrayColor,
      color: 'low',
      rotate: current.low.rotate,
      img: current.low.img,
      label: context.$t('low'),
      desc: questionnaire?.level_1 || '',
      status: LOW_STATUS
    }
  } else if (value > current.breakpoints[0] && value <= current.breakpoints[1]) {
    return {
      arrayColor: current.mid.arrayColor,
      color: 'med',
      rotate: current.mid.rotate,
      img: current.mid.img,
      label: context.$t('medium'),
      desc: questionnaire?.level_2 || '',
      status: MED_STATUS
    }
  } else if (value > current.breakpoints[1] && value <= current.breakpoints[2]) {
    return {
      arrayColor: current.adv.arrayColor,
      color: 'adv',
      rotate: current.adv.rotate,
      img: current.adv.img,
      label: context.$t('advance'),
      desc: questionnaire?.level_3 || '',
      status: ADVANCE_STATUS
    }
  } else if (value > current.breakpoints[2] && value <= Number.POSITIVE_INFINITY) {
    return {
      arrayColor: current.hig.arrayColor,
      color: 'high',
      rotate: current.hig.rotate,
      img: current.hig.img,
      label: context.$t('high'),
      desc: questionnaire?.level_4 || '',
      status: HIGH_STATUS
    }
  } else {
    throw new Error('Invalid breakpoints in levelHelper')
  }
}
export default levelOptions
