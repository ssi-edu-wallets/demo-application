import Vue from 'vue'
import Vuex from 'vuex'
import { user } from './modules/user'
import { questionnaire } from './modules/questionnaire'
import { admin } from './modules/admin'
import { config } from '@/store/modules/config'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    demoStep: 0,
    isLoading: false
  },
  getters: {
    demoStep (state) {
      return state.demoStep
    },
    isLoading (state) {
      return state.isLoading
    }
  },
  mutations: {
    increaseDemoStep (state) {
      state.demoStep++
    },
    setDemoStep (state, payload) {
      state.demoStep = payload
    },
    setIsLoading (state, payload) {
      state.isLoading = payload
    }
  },
  actions: {
    increaseDemoStep ({ commit }): void {
      commit('increaseDemoStep')
    },
    setDemoStep ({ commit }, payload): void {
      commit('setDemoStep', payload)
    },
    setIsLoading ({ commit }, payload) {
      commit('setIsLoading', payload)
    }
  },
  modules: {
    user,
    questionnaire,
    admin,
    config
  }
})
