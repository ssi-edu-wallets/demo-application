import { Module } from 'vuex'
import { getters } from './getters'
import { actions } from './actions'
import { mutations } from './mutations'
import { adminDataState } from './types'

export const state: adminDataState = {
  error: false,
  questions: [],
  answers: [],
  initiatives: [],
  initiativesScoreboard: [],
  initiativesScoreboardByDate: [],
  initiativesGroupState: [],
  initiativesGroupStateByDate: [],
  tags: [],
  topPerformers: [],
  totalRespondents: [],
  totalRespondentsState: [],
  scoreGrouped: []
}

const namespaced: boolean = true

export const admin: Module<adminDataState, any> = {
  namespaced,
  state,
  getters,
  actions,
  mutations
}
