import { MutationTree } from 'vuex'
import { adminDataState, RespondentEntity } from './types'
import InitiativeScore from '../../../assets/interfaces/InitiativeScore'

export const mutations: MutationTree<adminDataState> = {
  setQuestions (state, payload: Array<any>) {
    state.questions = payload
  },
  setInitiatives (state, payload: Array<any>) {
    state.initiatives = payload
  },
  setInitiativesScoreboard (state, payload: Array<any>) {
    state.initiativesScoreboard = payload
  },
  setInitiativesScoreboardByDate (state, payload: InitiativeScore[]) {
    state.initiativesScoreboardByDate = payload
  },
  setInitiativesGroupState (state, payload: Array<any>) {
    state.initiativesGroupState = payload
  },
  setInitiativesGroupStateByDate (state, payload: Array<any>) {
    state.initiativesGroupStateByDate = payload
  },
  setAnswers (state, payload: Array<any>) {
    state.answers = payload
  },
  setTags (state, payload: Array<any>) {
    state.tags = payload
  },
  setTopPerformers (state, payload: Array<any>) {
    state.topPerformers = payload
  },
  setTotalRespondents (state, payload: Array<any>) {
    state.totalRespondents = payload
  },
  setTotalRespondentsState (state, payload: RespondentEntity[]) {
    state.totalRespondentsState = payload
  },
  setScoreGrouped (state, payload: Array<any>) {
    state.scoreGrouped = payload
  }
}
