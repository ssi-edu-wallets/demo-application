import InitiativeScore from '../../../assets/interfaces/InitiativeScore'

export interface RespondentEntity {
  amount: number,
  state: string
}

export interface adminDataState {
    questions?: Array<any>;
    answers?: Array<any>;
    tags?: Array<any>;
    initiatives?:Array<any>;
    initiativesScoreboard?: Array<any>;
    initiativesScoreboardByDate?: InitiativeScore[];
    initiativesGroupState?: Array<any>;
    initiativesGroupStateByDate?: Array<any>;
    topPerformers?: Array<any>;
    totalRespondents?: Array<any>;
    totalRespondentsState?: RespondentEntity[];
    scoreGrouped?: Array<any>;
    error: boolean;
  }
