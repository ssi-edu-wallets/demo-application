import { GetterTree } from 'vuex'
import { adminDataState, RespondentEntity } from './types'
import InitiativeScore from '../../../assets/interfaces/InitiativeScore'

export const getters: GetterTree<adminDataState, any> = {
  getQuestions (questionnaire) {
    return questionnaire.questions
  },
  getAnswers (questionnaire) {
    return questionnaire.answers
  },
  getInitiatives (questionnaire) {
    return questionnaire.initiatives
  },
  getInitiativesScoreboard (questionnaire) {
    return questionnaire.initiativesScoreboard
  },
  getInitiativesScoreboardByDate (questionnaire): InitiativeScore[] {
    return questionnaire.initiativesScoreboardByDate!
  },
  getInitiativesGroupState (questionnaire) {
    return questionnaire.initiativesGroupState
  },
  getInitiativesGroupStateByDate (questionnaire) {
    return questionnaire.initiativesGroupStateByDate
  },
  getTags (questionnaire) {
    return questionnaire.tags
  },
  getTopPerformers (questionnaire) {
    return questionnaire.topPerformers
  },
  getTotalRespondents (questionnaire) {
    return questionnaire.totalRespondents
  },
  getTotalRespondentsState (questionnaire): RespondentEntity[] {
    return questionnaire.totalRespondentsState!
  },
  getScoreGrouped (questionnaire) {
    return questionnaire.scoreGrouped
  },
  getTagsByLanguage (questionnaire) {
    return function (lang: any) {
      if (questionnaire.tags) {
        if (lang === 'ar') {
          return questionnaire.tags.filter((tag: any) => {
            return tag.locale === 'arb'
          }).map((tag: any) => {
            return tag.display_name
          })
        } else if (lang === 'de') {
          return questionnaire.tags.filter((tag: any) => {
            return tag.locale === 'deu'
          }).map((tag: any) => {
            return tag.display_name
          })
        } else {
          return questionnaire.tags.filter((tag: any) => {
            return tag.locale === 'eng'
          }).map((tag: any) => {
            return tag.display_name
          })
        }
      }

      return []
    }
  }
}
