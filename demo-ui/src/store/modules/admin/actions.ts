import { ActionTree } from 'vuex'
import { adminDataState, RespondentEntity } from './types'
import QuestionnaireService from '@/services/questionnaireService'
import InitiativesService from '@/services/initiativesService'
import Question from '../../../assets/interfaces/Question'
import InitiativeScore from '../../../assets/interfaces/InitiativeScore'

export const actions: ActionTree<adminDataState, any> = {
  async loadQuestionnaireData ({ commit }): Promise<void> {
    try {
      const questionnaireService = new QuestionnaireService()
      const response = await questionnaireService.getQuestions()
      const questions: Question[] = response.data.data
      commit('setQuestions', questions)
    } catch (err) {
      commit('setQuestions', [])
    }
  },
  async loadAnswersData ({ commit }, id) : Promise<void> {
    try {
      const questionnaireService = new QuestionnaireService()
      const response = await questionnaireService.getQuestion(id)
      const answers = response.data.data
      commit('setAnswers', answers)
    } catch (err) {
      commit('setAnswers', [])
    }
  },
  async loadInitiativesData ({ commit }): Promise<void> {
    const initiativeService = new InitiativesService()
    const response = await initiativeService.getInitiatives()
    const initiatives = response.data.data
    if (!initiatives) {
      throw new Error('Initiatives are not defined in the API')
    }
    commit('setInitiatives', initiatives)
  },
  async loadTagsData ({ commit }): Promise<void> {
    try {
      const initiativeService = new InitiativesService()
      const response = await initiativeService.getTags()
      const tags = response.data.data
      commit('setTags', tags)
    } catch (error) {
      commit('setTags', [])
    }
  },
  async loadInitiativesScoreboard ({ commit }, questionnaireId: number) {
    try {
      const initiativeService = new InitiativesService()
      const response = await initiativeService.getInitiativeScoreboard(questionnaireId)
      const initiativesScoreboard = response.data.data
      commit('setInitiativesScoreboard', initiativesScoreboard)
    } catch (error) {
      commit('setInitiativesScoreboard', [])
    }
  },
  async loadInitiativesScoreboardByDate ({ commit }, date) {
    try {
      const initiativeService = new InitiativesService()
      const response = await initiativeService.getInitiativeScoreboardByDate(date.gte, date.lte, date.questionnaireId, date.historic, date.tags)
      const initiativesScoreboard: InitiativeScore[] = response.data.data
      commit('setInitiativesScoreboardByDate', initiativesScoreboard)
    } catch (error) {
      commit('setInitiativesScoreboardByDate', [])
    }
  },
  async loadUserScoreboard ({ commit }, payload) {
    try {
      const initiativeService = new InitiativesService()
      const response = await initiativeService.getUserScoreboard(payload)
      const initiativesScoreboard = response.data.data
      commit('setInitiativesScoreboard', initiativesScoreboard)
    } catch (error) {
      commit('setInitiativesScoreboard', [])
    }
  },
  async loadUserScoreboardByDate ({ commit }, payload) {
    try {
      const initiativeService = new InitiativesService()
      const response = await initiativeService.getUserScoreboardByDate(payload)
      const initiativesScoreboard = response.data.data
      commit('setInitiativesScoreboard', initiativesScoreboard)
    } catch (error) {
      commit('setInitiativesScoreboard', [])
    }
  },
  async loadInitiativesGroupState ({ commit }, questionnaireId: number) {
    try {
      const initiativeService = new InitiativesService()
      const response = await initiativeService.getInitiativeGroupState(questionnaireId)
      const initiativesGroupState = response.data.data
      commit('setInitiativesGroupState', initiativesGroupState)
    } catch (error) {
      commit('setInitiativesGroupState', [])
    }
  },
  async loadInitiativesGroupStateByDate ({ commit }, date) {
    try {
      const initiativeService = new InitiativesService()
      const response = await initiativeService.getInitiativeGroupStateByDate(date.gte, date.lte, date.questionnaireId)
      const initiativesGroupState = response.data.data
      commit('setInitiativesGroupStateByDate', initiativesGroupState)
    } catch (error) {
      commit('setInitiativesGroupStateByDate', [])
    }
  },
  async loadTopPerformers ({ commit }, questionnaireId: number) {
    try {
      const initiativeService = new InitiativesService()
      const response = await initiativeService.getTopPerformers(questionnaireId)
      const topPerformers = response.data.data
      commit('setTopPerformers', topPerformers)
    } catch (error) {
      commit('setTopPerformers', [])
    }
  },
  async loadTotalRespondents ({ commit }, questionnaireId: number) {
    try {
      const initiativeService = new InitiativesService()
      const response = await initiativeService.getTotalRespondents(questionnaireId)
      const totalRespondents = response.data.data
      commit('setTotalRespondents', totalRespondents)
    } catch (error) {
      commit('setTotalRespondents', [])
    }
  },
  async loadTotalRespondentsState ({ commit }, qnrId: number) {
    try {
      const initiativeService = new InitiativesService()
      const res: RespondentEntity[] = await initiativeService.getTotalRespondentsState(qnrId)
      commit('setTotalRespondentsState', res)
    } catch (error) {
      commit('setTotalRespondentsState', [])
    }
  },
  async loadScoreGrouped ({ commit }, questionnaireId: number) {
    try {
      const initiativeService = new InitiativesService()
      const response = await initiativeService.getScoreGrouped(questionnaireId)
      const scoreGrouped = response.data.data
      commit('setScoreGrouped', scoreGrouped)
    } catch (error) {
      commit('setScoreGrouped', [])
    }
  },
  async loadScoreGroupedByDate ({ commit }, date) {
    try {
      const initiativeService = new InitiativesService()
      const response = await initiativeService.getScoreGroupedByDate(date.gte, date.lte, date.questionnaireId)
      const scoreGrouped = response.data.data
      commit('setScoreGrouped', scoreGrouped)
    } catch (error) {
      commit('setScoreGrouped', [])
    }
  },
  async addQuestion ({ commit }, question): Promise<void> {
    const questionnaireService = new QuestionnaireService()
    await questionnaireService.resisterQuestion(question)
  },
  async updateQuestion ({ commit }, question): Promise<void> {
    const questionnaireService = new QuestionnaireService()
    await questionnaireService.updateQuestion(question.id, question.data)
  },
  async addAnswer ({ commit }, answer): Promise<void> {
    try {
      const questionnaireService = new QuestionnaireService()
      await questionnaireService.registerAnswer(answer.idQuestion, answer.data)
    } catch (err) {
      console.log(err)
    }
  },
  async updateAnswer ({ commit }, answer) {
    try {
      const questionnaireService = new QuestionnaireService()
      await questionnaireService.updateAnswer(answer.idQuestion, answer.idAnswer, answer.data)
    } catch (err) {
      console.log(err)
    }
  },
  async deleteAnswer ({ commit }, answer) {
    const questionnaireService = new QuestionnaireService()
    await questionnaireService.deleteAnswer(answer.idQuestion, answer.idAnswer)
  },
  async deleteQuestion ({ commit }, id): Promise<void> {
    const questionnaireService = new QuestionnaireService()
    await questionnaireService.deleteQuestion(id)
  },
  async addInitiative ({ commit }, initiative): Promise<void> {
    try {
      const initiativeService = new InitiativesService()
      await initiativeService.registerInitiative(initiative)
    } catch (err) {
      console.log(err)
    }
  },
  async updateInitiative ({ commit }, initiative): Promise<void> {
    try {
      const initiativeService = new InitiativesService()
      await initiativeService.updateInitiative(initiative.id, initiative.data)
    } catch (err) {
      console.log(err)
    }
  },
  async deleteInitiative ({ commit }, id): Promise<void> {
    try {
      const initiativeService = new InitiativesService()
      await initiativeService.deleteInitiative(id)
    } catch (err) {
      console.log(err)
    }
  }
}
