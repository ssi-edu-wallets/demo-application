import { GetterTree } from 'vuex'
import { Progress, Questionnaire, QuestionnaireState, Tag } from './types'
import Question from '@/assets/interfaces/Question'
import Category from '../../../assets/interfaces/Category'
import FieldsMap from '../../../assets/interfaces/FieldsMap'
import AnswersProgress from '../../../assets/interfaces/AnswersProgress'
import QnrCategory from '../../../assets/interfaces/QnrCategory'
import UserJobPosition from '../../../assets/interfaces/UserJobPosition'

export const getters: GetterTree<QuestionnaireState, any> = {
  getCategoryIndex (questionnaire: QuestionnaireState) {
    return questionnaire.steps
  },
  getProgress (questionnaire: QuestionnaireState): Progress {
    return questionnaire.progress!
  },
  getTag (questionnaire: QuestionnaireState) {
    return questionnaire.tags
  },
  getStateForm (questionnaire: QuestionnaireState) {
    return questionnaire.stateForm
  },
  getFormState (questionnaire: QuestionnaireState) {
    return questionnaire.formsState
  },
  getFieldsMap (questionnaire: QuestionnaireState) {
    const toRet: FieldsMap = { ...questionnaire.fieldsMap }
    if (Object.entries(toRet).length === 0) {
      // tslint:disable-next-line:no-console
      console.error('No available categories (fieldsMap: maybe you need to properly assign link_cloud in your categories):', toRet)
    }
    return toRet
  },
  getQuestionnaire (questionnaire: QuestionnaireState): Questionnaire {
    return questionnaire.questionnaire!
  },
  getQuestions (questionnaire: QuestionnaireState) {
    return questionnaire.questions
  },
  getAnswersProgress (questionnaire: QuestionnaireState) {
    return questionnaire.answersProgress
  },
  getProgressAnswers (questionnaire: QuestionnaireState): AnswersProgress[] {
    return questionnaire.progressAnswers!
  },
  getEmptyAnswers (questionnaire: QuestionnaireState) {
    return questionnaire.emptyAnswers
  },
  getTagsByFunction (questionnaire: QuestionnaireState) {
    return (fieldSlug: string): Tag[][] => {
      let fieldsMap: Category[] = []
      if (questionnaire.fieldsMap && fieldSlug) {
        fieldsMap = [...questionnaire.fieldsMap[fieldSlug]]
      }
      if (fieldsMap.length) {
        return fieldsMap.map((e: Category) => {
          return e.landing_page_tags
        })
      }
      return []
    }
  },
  getQuestionsByTags (questionnaire: QuestionnaireState): (tags: Tag[]) => Question[] {
    return (tags: Tag[]): Question[] => {
      const _questions: Question[] = []
      if (tags && questionnaire.questions) {
        for (const question of questionnaire.questions) {
          tags.forEach((tag: Tag) => { // Tags
            if (question.tag === tag.tag) {
              _questions.push(question) // more one question per tag
            }
          })
        }
      }
      return _questions
    }
  },
  getQnrCategories (questionnaire: QuestionnaireState): QnrCategory[] {
    return questionnaire.qrnCategories || []
  },
  getNeedsTrack (questionnaire: QuestionnaireState): boolean {
    return questionnaire.needsTrack
  },
  getUserJobPosition (questionnaire: QuestionnaireState): UserJobPosition {
    return questionnaire.userJobPosition
  }
}
