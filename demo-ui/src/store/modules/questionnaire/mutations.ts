import { MutationTree } from 'vuex'
import { QuestionnaireState, Progress, Tag, StateForm, Questionnaire } from './types'
import Question from '@/assets/interfaces/Question'
import FieldsMap from '../../../assets/interfaces/FieldsMap'
import AnswersProgress from '../../../assets/interfaces/AnswersProgress'
import QnrCategory from '../../../assets/interfaces/QnrCategory'
import UserJobPosition from '../../../assets/interfaces/UserJobPosition'

export const mutations: MutationTree<QuestionnaireState> = {
  clearUserJobPosition (state) {
    state.userJobPosition.job_position = ''
    state.userJobPosition.home_office = false
    state.userJobPosition.location = ''
    state.userJobPosition.occupation = ''
    state.userJobPosition.working_times = ''
  },
  setStep (state, payload: number) {
    state.error = false
    state.steps = payload
  },
  setProgress (state, payload: Progress) {
    state.error = false
    state.progress = payload
  },
  setTag (state, payload: Tag) {
    state.error = false
    state.tags = payload
  },
  setStateForm (state, payload: StateForm) {
    state.error = false
    state.stateForm = payload
  },
  setFormState (state, payload: boolean[]) {
    state.error = false
    state.formsState = payload
  },
  setError (state) {
    state.error = true
  },
  setFieldsMap (state, payload: FieldsMap) {
    state.fieldsMap = payload
  },
  setQuestionnaire (state, payload: Questionnaire) {
    state.questionnaire = payload
  },
  setQuestions (state, payload: Question[]) {
    state.questions = payload
  },
  setAnswer (state, payload: any[]) {
    state.answersProgress = payload
  },
  setAnswers (state, payload: AnswersProgress[]) {
    state.progressAnswers = payload
  },
  setEmptyAnswers (state, payload: boolean) {
    state.emptyAnswers = payload
  },
  setQnrCategories (state, qrnCategories: QnrCategory[]): void {
    state.qrnCategories = qrnCategories
  },
  setUserJobPosition (state, jobPosition: UserJobPosition): void {
    state.userJobPosition = jobPosition
  },
  setNeedsTrack (state, needsTrack: boolean): void {
    state.needsTrack = needsTrack
  }
}
