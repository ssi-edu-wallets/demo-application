import Field from '../../../assets/interfaces/Field'
import FieldsMap from '../../../assets/interfaces/FieldsMap'
import AnswersProgress from '../../../assets/interfaces/AnswersProgress'
import QnrCategory from '../../../assets/interfaces/QnrCategory'
import UserJobPosition from '../../../assets/interfaces/UserJobPosition'

export interface Tag {
  display_name: string;
  id: number;
  tag: string;
}

export interface StateForm{
  form1?: boolean;
  form2?: boolean;
  form3?: boolean;
}

export interface Progress {
  question: number;
  currentQuestion: number;
  numberQuestion: number;
  progress: number;
}

export interface Questionnaire {
  id: number,
  checkbox_label?: string,
  email_label?: string,
  slug: string,
  hide_user_fields?: boolean,
  icon: string,
  level_1_title?: string,
  level_1?: string,
  level_2_title?: string,
  level_2?: string,
  level_3_title?: string,
  level_3?: string,
  level_4_title?: string,
  level_4?: string,
  pdf_title?: string,
  pdf_description_1?: string,
  pdf_image?: string,
  pdf_description_2?: string,
  question_info?: string,
  title: string,
  type: string,
  selector_text: string,
  user_fields_component?: string,
  user_info?: string,
  user_submission_component?: string,
  fields: Field[],
  related_questionnaire_id?: number,
  signup_description?: string,
  single_submit?: boolean
}

export interface QuestionnaireState {
  steps?: number;
  tags?: Tag;
  progress?: Progress;
  stateForm?: StateForm;
  formsState?: boolean[];
  fieldsMap?: FieldsMap;
  questionnaire?: Questionnaire;
  questions?: any[];
  answersProgress?: any[];
  progressAnswers?: AnswersProgress[];
  emptyAnswers?: boolean;
  error: boolean;
  qrnCategories?: QnrCategory[],
  needsTrack: boolean,
  userJobPosition: UserJobPosition
}
