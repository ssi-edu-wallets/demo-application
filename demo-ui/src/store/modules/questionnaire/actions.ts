import { ActionTree } from 'vuex'
import { Progress, Questionnaire, QuestionnaireState } from './types'
import QuestionnaireService from '@/services/questionnaireService'
import Question from '@/assets/interfaces/Question'
import Category from '../../../assets/interfaces/Category'
import FieldsMap from '../../../assets/interfaces/FieldsMap'
import AnswersProgress from '../../../assets/interfaces/AnswersProgress'
import QnrCategory from '../../../assets/interfaces/QnrCategory'
import UserJobPosition from '../../../assets/interfaces/UserJobPosition'

export const actions: ActionTree<QuestionnaireState, any> = {
  clearJobPosition ({ commit }): void {
    commit('clearUserJobPosition')
  },
  registerQnrCategories ({ commit }, qnrCategories: QnrCategory[]): void {
    commit('setQnrCategories', qnrCategories)
  },
  registerUserJobPosition ({ commit }, jobPosition: UserJobPosition): void {
    commit('setUserJobPosition', jobPosition)
  },
  registerStep ({ commit }, step: number): void {
    commit('setStep', step)
  },
  registerNeedsTrack ({ commit }, needsTrack: boolean): void {
    commit('setNeedsTrack', needsTrack)
  },
  registerProgress ({ commit }, progress: Progress): void {
    commit('setProgress', progress)
  },
  registerTag ({ commit }, tags): void {
    commit('setTag', { ...tags })
  },
  registerStateForm ({ commit }, stateForm): void {
    commit('setStateForm', stateForm)
  },
  registerFormState ({ commit }, formState): void {
    commit('setFormState', formState)
  },
  registerAnswer ({ commit }, answer): void {
    commit('setAnswer', { ...answer })
  },
  registerAnswers ({ commit }, answer: AnswersProgress[]): void {
    commit('setAnswers', answer)
  },
  registerEmptyAnswer ({ commit }, answers: boolean): void {
    commit('setEmptyAnswers', answers)
  },
  async loadQuestionnaire ({ commit }, slug: string): Promise<void> {
    try {
      const questionnaireService = new QuestionnaireService()
      const response = await questionnaireService.getQuestionnaire(slug)
      const qnr: Questionnaire = response.data.data
      commit('setQuestionnaire', qnr)
    } catch (err) {
      // tslint:disable-next-line:no-console
      console.error('Error loading Questionnaire', err)
    }
  },
  async loadQuestionnaireData ({ commit }): Promise<void> {
    try {
      const questionnaireService = new QuestionnaireService()
      // Functions
      let response = await questionnaireService.getCategories()
      const categories: Category[] = response.data.data
      const fieldsMap: FieldsMap = categories.reduce((r: any, cat: Category) => {
        if (cat.link_cloud) {
          r[cat.link_cloud] = [...r[cat.link_cloud] || [], cat]
        }
        return r
      }, {})
      commit('setFieldsMap', fieldsMap)

      // Questions
      response = await questionnaireService.getQuestions()
      const questions: Question[] = response.data.data
      commit('setQuestions', questions)
    } catch (err) {
      commit('setFieldsMap', {})
      commit('setQuestions', [])
    }
  }
}
