import { Module } from 'vuex'
import { getters } from './getters'
import { actions } from './actions'
import { mutations } from './mutations'
import { QuestionnaireState } from './types'

export const state: QuestionnaireState = {
  steps: undefined,
  tags: undefined,
  progress: undefined,
  stateForm: undefined,
  formsState: [],
  error: false,
  fieldsMap: {},
  questionnaire: undefined,
  questions: [],
  answersProgress: [],
  progressAnswers: [],
  emptyAnswers: undefined,
  qrnCategories: [],
  needsTrack: false,
  userJobPosition: {
    job_position: '',
    home_office: false,
    location: '',
    occupation: '',
    working_times: ''
  }
}

const namespaced: boolean = true

export const questionnaire: Module<QuestionnaireState, any> = {
  namespaced,
  state,
  getters,
  actions,
  mutations
}
