import { GetterTree } from 'vuex'
import { configApp } from './types'

export const getters: GetterTree<configApp, any> = {
  getConfig (configuration) {
    return configuration.config
  },
  getLocale (configuration) {
    return configuration.locale
  },
  getToPath (configuration) {
    return configuration.toPath
  }
}
