import { Module } from 'vuex'
import { getters } from './getters'
import { actions } from './actions'
import { mutations } from './mutations'
import { configApp } from './types'

export const state: configApp = {
  config: [],
  locale: '',
  toPath: ''
}

const namespaced: boolean = true

export const config: Module<configApp, any> = {
  namespaced,
  state,
  getters,
  actions,
  mutations
}
