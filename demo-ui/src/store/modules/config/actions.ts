import { ActionTree } from 'vuex'
import { configApp } from './types'
import ConfigService from '@/services/configService'

export const actions: ActionTree<configApp, any> = {
  async loadConfigData ({ commit }): Promise<void> {
    try {
      const configService = new ConfigService()
      const response = await configService.getConfig()
      const config = response.data.data
      commit('setConfig', config)
    } catch (err) {
      commit('setConfig', [])
    }
  },
  registerLocaleData ({ commit }, locale): any {
    commit('setLocale', locale)
  },
  registerToPath ({ commit }, path): any {
    commit('setToPath', path)
  }
}
