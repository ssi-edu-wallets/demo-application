import { MutationTree } from 'vuex'
import { configApp } from './types'

export const mutations: MutationTree<configApp> = {
  setConfig (state, payload: any[]) {
    state.config = payload
  },
  setLocale (state, payload: string) {
    state.locale = payload
  },
  setToPath (state, payload: string) {
    state.toPath = payload
  }
}
