import InitiativesUser from '../../../assets/interfaces/InitiativesUser'
import { Questionnaire } from '../questionnaire/types'
import Submission from '../../../assets/interfaces/Submission'
import UserProfile from '../../../assets/interfaces/UserProfile'

export interface User {
  // firstName?: string;
  // lastName?: string;
  id: number;
  email: string;
  token: any;
  group_id: any;
  grant_level: number,
  parent_id: number
}

export interface UserInitiatives {
  initiatives?: any
  initiativesByDate?: any
}

export interface UserState {
  user?: User;
  initiatives?: InitiativesUser[];
  initiativesByDate?: InitiativesUser[];
  questionnaires?: Questionnaire[]
  error: boolean;
  submissions: Submission[],
  userProfile?: UserProfile
}
