import { Module } from 'vuex'
import { getters } from './getters'
import { actions } from './actions'
import { mutations } from './mutations'
import { UserState } from './types'

export const state: UserState = {
  user: {
    id: 0,
    email: '',
    token: {
      access_token: ''
    },
    group_id: 0,
    grant_level: 0,
    parent_id: 0
  },
  initiatives: [],
  initiativesByDate: [],
  questionnaires: [],
  error: false,
  submissions: [],
  userProfile: undefined
}

const namespaced: boolean = true

export const user: Module<UserState, any> = {
  namespaced,
  state,
  getters,
  actions,
  mutations
}
