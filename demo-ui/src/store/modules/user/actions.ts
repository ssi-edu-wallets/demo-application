import { ActionTree } from 'vuex'
import { UserState } from './types'
import UserService from '@/services/userService'
import InitiativesService from '@/services/initiativesService'
import InitiativesUser from '../../../assets/interfaces/InitiativesUser'
import { Questionnaire } from '../questionnaire/types'
import SubmissionsService from '../../../services/submissionsService'
import Submission from '../../../assets/interfaces/Submission'
import UserProfile from '../../../assets/interfaces/UserProfile'

export const actions: ActionTree<UserState, any> = {
  async loadUserToken ({ commit }): Promise<any> {
    const userService = new UserService([])
    try {
      const response = await userService.getAccessToken()
      await commit('setUser', { ...response.data.data })
    } catch (e) {
      await commit('setError', {
        id: 0,
        email: '',
        token: {
          access_token: ''
        },
        group_id: 0,
        grant_level: 0,
        parent_id: 0
      })
    }
  },
  setUserProfile ({ commit }, userProfile: UserProfile) {
    commit('setUserProfile', userProfile)
  },
  setUserToken ({ commit }, token) {
    commit('setUserToken', token)
  },
  async loadUserInitiatives ({ commit }, filters: object): Promise<void> {
    const initiativesServices = new InitiativesService()
    try {
      const response = await initiativesServices.getUserInitiatives(filters)
      const initiatives: InitiativesUser[] = response?.data.data
      await commit('setUserInitiatives', { ...initiatives })
    } catch (e) {
      // await commit('setError', {})
    }
  },
  async loadUserInitiativesByDate ({ commit }, payload: object): Promise<any> {
    const initiativesServices = new InitiativesService()
    try {
      const response = await initiativesServices.getUserInitiativesByDate(payload)
      const initiatives: InitiativesUser[] = response?.data.data
      await commit('setUserInitiativesByDate', { ...initiatives })
    } catch (e) {
      // await commit('setError', {})
    }
  },
  registerUser ({ commit }, user): any {
    commit('setUser', { ...user.data.data })
  },
  async loadUserQuestionnaires ({ commit }) {
    const userService = new UserService([])
    const response = await userService.getCompletedQuestionnaires()
    const qnr: Questionnaire[] = response.data.data
    commit('setUserQuestionnaires', qnr)
  },
  async loadUserSubmissions ({ commit }, userId) {
    const submissionsService = new SubmissionsService()
    const response = await submissionsService.getSubmissions(userId)
    const submissions: Submission[] = response.data.data
    commit('setUserSubmissions', submissions)
  },
  async registerStateInitiative ({ commit }, payload: any): Promise<void> {
    try {
      const initiativeService = new InitiativesService()
      await initiativeService.registerStateInitiative(payload)
    } catch (err) {
      console.log(err)
    }
  }
}
