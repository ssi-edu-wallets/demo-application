import { GetterTree } from 'vuex'
import { User, UserState } from './types'
import InitiativesUser from '../../../assets/interfaces/InitiativesUser'
import {
  GRANT_LEVEL_SUPERVISOR,
  GROUP_ADMIN,
  GROUP_SELLER,
  GROUP_TRAINER,
  GROUP_BUYER
} from '../../../services/constants'
import { Questionnaire } from '../questionnaire/types'
import Submission from '../../../assets/interfaces/Submission'
import UserProfile from '../../../assets/interfaces/UserProfile'

export type Getters = {
  getUser(user: UserState): any;
};

export const getters: GetterTree<UserState, any> & Getters = {
  getUser (user: UserState): User {
    return user.user!
  },
  getUserInitiatives (user: UserState): InitiativesUser[] {
    return user.initiatives!
  },
  getUserInitiativesByDate (user: UserState): InitiativesUser[] {
    return user.initiativesByDate!
  },
  getUserProfile (user: UserState): UserProfile | undefined {
    return user.userProfile!
  },
  getUserQuestionnaires (user: UserState): Questionnaire[] {
    return user.questionnaires!
  },
  getUserSubmissions (user: UserState): Submission[] {
    return user.submissions!
  },
  getCurrentSubmission (user: UserState) {
    return (route: any): Submission | undefined => {
      const queryId: any = route?.query?.submission
      const submissionId = parseInt(queryId)
      const qnrSlug = route?.params?.slug
      const submissionsArray: Submission[] = [...user.submissions].reverse()
      let foundSubmission
      if (submissionId) {
        foundSubmission = submissionsArray.find((submission: Submission) => {
          return submission.id === submissionId
        })
      } else if (qnrSlug) {
        foundSubmission = submissionsArray.find((submission: Submission) => {
          return submission.questionnaire.slug === qnrSlug
        })
      }
      return foundSubmission
    }
  },
  isBuyer (user: UserState): boolean {
    const usr = user.user!
    return usr.group_id === GROUP_BUYER
  },
  isConsultant (user: UserState): boolean {
    const usr = user.user!
    return usr.group_id === GROUP_TRAINER && usr.grant_level === GRANT_LEVEL_SUPERVISOR
  },
  isManager (user: UserState): boolean {
    const usr = user.user!
    const isAdmin = usr.group_id === GROUP_ADMIN
    const isSme = usr.group_id === GROUP_SELLER
    return isAdmin || isSme
  }
}
