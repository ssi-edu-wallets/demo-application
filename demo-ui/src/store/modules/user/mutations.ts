import { MutationTree } from 'vuex'
import { UserState, User } from './types'
import InitiativesUser from '../../../assets/interfaces/InitiativesUser'
import { Questionnaire } from '../questionnaire/types'
import Submission from '../../../assets/interfaces/Submission'
import UserProfile from '../../../assets/interfaces/UserProfile'

export const mutations: MutationTree<UserState> = {
  setUser (state: UserState, payload: User) {
    state.error = false
    state.user = payload
  },
  setUserToken (state, token: string) {
    if (state.user) {
      state.user.token.access_token = token
    }
  },
  setError (state, payload: User) {
    state.error = true
    state.user = payload
  },
  setUserInitiatives (state: UserState, payload: InitiativesUser[]) {
    state.initiatives = payload
    state.error = true
  },
  setUserInitiativesByDate (state: UserState, payload: InitiativesUser[]) {
    state.initiativesByDate = payload
    state.error = true
  },
  setUserQuestionnaires (state: UserState, payload: Questionnaire[]) {
    state.questionnaires = payload
  },
  setUserSubmissions (state: UserState, payload: Submission[]) {
    state.submissions = payload
  },
  setUserProfile (state: UserState, payload: UserProfile) {
    state.userProfile = payload
  }
}
