import Answer from './Answer'

export default interface Question {
  id: number,
  position: number
  question: string,
  questionnaire_id: number,
  tag: string,
  answers: Answer[],
  alt1: string,
  alt2: string
}
