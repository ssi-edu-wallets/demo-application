export default interface LevelIndicator {
  arrayColor: number[],
  color: string,
  desc: string,
  img: string,
  label: string,
  rotate: number,
  status: number
}
