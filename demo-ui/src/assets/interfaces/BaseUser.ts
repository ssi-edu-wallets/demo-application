export default interface BaseUser {
  benefits?: string,
  id: number
  email: string
  company?: string,
  company_size?: string,
  cities?: any[]
  employees_amount?: string
  ongoing_projects?: string
  sector?: string,
  total_projects?: string
  default_questionnaire?: string
  age_range?: string
  education?: string
  employment?: string
  gender?: string
}
