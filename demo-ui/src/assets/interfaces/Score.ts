export default interface Score {
  initiative_id: number,
  score: number | null,
  state: string | undefined,
  max_score: number,
  created: string,
}
