export default interface UserJobPosition {
  job_position: string,
  home_office: boolean,
  location: string,
  occupation: string,
  working_times: string
}
