export default interface Field {
  id: number,
  title: string,
  slug: string,
  icon: string,
  position: number
}
