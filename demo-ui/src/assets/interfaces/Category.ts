import { Tag } from '../../store/modules/questionnaire/types'

export default interface Category {
  id: number,
  landing_page_tags: Tag[],
  link_cloud: string,
  slug: string,
  relevance: null | number,
  icon: string,
  description: string,
  display_name: string,
}
