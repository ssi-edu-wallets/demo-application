import Initiative from './Initiative'

export default interface Scorecard {
  avg_completed: number
  avg_in_progress: number
  avg_not_started: number
  completed: number
  in_progress: number
  initiative: Initiative
  not_started: number
}
