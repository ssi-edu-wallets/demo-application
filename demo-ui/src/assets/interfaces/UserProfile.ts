import ProfileSkill from './ProfileSkill'

export default interface UserProfile {
  current_occupation: string,
  goal_description: string,
  industry: string,
  main_goal: string,
  profile: string,
  profiles_skills: ProfileSkill[]
  target_occupation: string,
  user_id: string
}
