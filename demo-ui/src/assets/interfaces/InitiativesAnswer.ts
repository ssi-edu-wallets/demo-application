import UserJobPosition from './UserJobPosition'
import Score from './Score'

export default interface InitiativesAnswer {
  initiatives: Score[],
  user: UserJobPosition
}
