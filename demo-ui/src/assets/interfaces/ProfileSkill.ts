export default interface ProfileSkill {
  id: number,
  user_profile_id: number,
  skill: string,
  esco_uri: string,
  status: string
}
