import Category from './Category'

export default interface FieldsMap {
  [name: string]: Category[];
}
