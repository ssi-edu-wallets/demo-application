import { TranslateResult } from 'vue-i18n'

import PdfExportInitiative from './PdfExportInitiative'

export default interface PdfExportField {
  title: TranslateResult,
  badge: string,
  description: TranslateResult,
  rows: PdfExportInitiative[]
}
