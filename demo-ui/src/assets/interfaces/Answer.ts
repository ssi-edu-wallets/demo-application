export default interface Answer {
  id: number,
  answer: string,
  question_id: number,
  weight: number
}
