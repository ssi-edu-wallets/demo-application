import { Tag } from '../../store/modules/questionnaire/types'

export default interface CategoryData {
  categoryTitle: string,
  tags: Tag[]
}
