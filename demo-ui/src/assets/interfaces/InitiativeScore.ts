import Initiative from './Initiative'

// check InitiativesUser
export default interface InitiativeScore {
  avg_percentage: number,
  initiative_id: number,
  initiative: Initiative,
}
