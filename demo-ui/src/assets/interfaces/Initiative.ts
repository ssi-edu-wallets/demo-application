export default interface Initiative {
  created: string,
  description: string,
  id: number
  modified: string,
  tag_slug?: string,
  tags_list: string[],
  title: string,
}
