import Initiative from './Initiative'

// check InitiativeScore
export default interface InitiativesUser {
  avg_percentage?: number,
  id: number,
  initiative: Initiative,
  is_flagged: boolean,
  max_score: number,
  percentage: number,
  state: string | null,
  combined_amount: number,
  score: number
}
