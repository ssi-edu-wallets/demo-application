import Answer from './Answer'
import Question from './Question'

export default class QuestionWithAnswer implements Question {
  public __class: string = 'QuestionWithAnswer'
  public id: number
  public position: number
  public question: string
  public questionnaire_id: number
  public tag: string
  public answers: Answer[]
  public alt1: string
  public alt2: string

  public alternativeQuestions: string[]
  public indexAnswered?: number
  public isSkipped: boolean = false

  public currentAlternativeQuestionIndex: number = 0
  public skippedAmount: number = 0

  constructor (question: Question) {
    this.id = question.id
    this.position = question.position
    this.question = question.question
    this.questionnaire_id = question.questionnaire_id
    this.tag = question.tag
    this.answers = question.answers
    this.alt1 = question.alt1
    this.alt2 = question.alt2

    const alternativeQuestions: string[] = []
    alternativeQuestions.push(question.question)
    if (question.alt1) {
      alternativeQuestions.push(question.alt1)
    }
    if (question.alt2) {
      alternativeQuestions.push(question.alt2)
    }
    this.alternativeQuestions = alternativeQuestions
  }

  public getCurrentQuestionText (): string {
    return this.alternativeQuestions[this.currentAlternativeQuestionIndex]
  }

  public hasAlternativeQuestions (): boolean {
    return this.alternativeQuestions.length > 1
  }

  public getAnswer (): Answer | undefined {
    if (this.indexAnswered || this.indexAnswered === 0) {
      return this.answers[this.indexAnswered]
    }
    return undefined
  }

  public getScore (): number {
    if (this.indexAnswered || this.indexAnswered === 0) {
      return this.answers[this.indexAnswered].weight
    }
    return 0
  }

  public hasAnswer (): boolean {
    return !!this.indexAnswered || this.indexAnswered === 0
  }

  public isUnanswered (): boolean {
    return !this.isSkipped && !this.indexAnswered && this.indexAnswered !== 0
  }

  public saveIndexAnswer (index: number): void {
    this.isSkipped = false
    this.indexAnswered = index
  }

  public setIsSkippedQuestion (skipped: boolean): void {
    this.isSkipped = skipped
    if (skipped) {
      this.indexAnswered = undefined
    }
  }
}
