export default interface AnswersProgress {
  answer: string
  question: string
  tag: string
  weight: number
}
