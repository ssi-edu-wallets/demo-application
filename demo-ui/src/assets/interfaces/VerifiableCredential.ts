export default interface VerifiableCredential {
  description: string,
  id: string,
  name: string,
  type: string
}
