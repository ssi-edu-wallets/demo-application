import { Questionnaire } from '../../store/modules/questionnaire/types'

export default interface Submission {
  created: string,
  job_position: string,
  home_office: boolean,
  location: string,
  id: number,
  occupation: string,
  questionnaire: Questionnaire,
  questionnaire_id: number,
  user_id: number,
  working_times: string
}
