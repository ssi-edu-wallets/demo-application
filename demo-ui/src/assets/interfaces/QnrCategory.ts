import Category from './Category'
import QuestionWithAnswer from './QuestionWithAnswer'

export default class QnrCategory {
  public questions: QuestionWithAnswer[]
  public category: Category

  public currentQuestionIndex: number = 0

  constructor (category: Category, questions: QuestionWithAnswer[]) {
    this.questions = questions
    this.category = category
  }

  public isUnanswered (): boolean {
    return this.progressPercentage() === 0
  }

  public isAllAnswered (): boolean {
    return this.progressPercentage() === 100
  }

  public amountAnswered (): number {
    let answered = 0
    this.questions.forEach((question: QuestionWithAnswer) => {
      if (!question.isUnanswered()) {
        answered++
      }
    })
    return answered
  }

  public totalQuestions (): number {
    return this.questions.length
  }

  public progressPercentage (): number {
    if (this.totalQuestions() === 0) {
      return 100
    }
    return this.amountAnswered() / this.totalQuestions() * 100
  }

  public getCurrentQuestion (): QuestionWithAnswer {
    return this.questions[this.currentQuestionIndex]
  }

  public getTotalPreviousSkipped (): number {
    let total = 0
    for (let i = 0; i < this.currentQuestionIndex; i++) {
      if (this.questions[i]?.skippedAmount) {
        total += this.questions[i].skippedAmount
      }
    }
    return total
  }

  public isLastQuestion (): boolean {
    return this.currentQuestionIndex === this.questions.length - 1
  }

  public nextQuestion (): void {
    if (!this.isLastQuestion()) {
      this.currentQuestionIndex++
    }
  }

  public resetCurrentQuestion () :void {
    this.currentQuestionIndex = 0
  }

  public eq (compare: QnrCategory): boolean {
    return this.category.slug === compare.category.slug
  }
}
