import { axiosClient } from './axiosClient'
import UserProfile from '../assets/interfaces/UserProfile'

export default class UserProfileService {
  getUserProfile (uid: number) {
    return axiosClient.get('/edu/api/v1/profiles/' + uid)
  }

  saveUserProfile (payload: UserProfile) {
    return axiosClient.post('/edu/api/v1/profiles/', payload)
  }
}
