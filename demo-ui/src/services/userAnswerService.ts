import { axiosClient } from './axiosClient'

export default class UserAnswerService {
  saveUserAnswer (questionnaireId: number, userId: number, data: any) {
    return axiosClient.post(
      `/edu/api/v1/questionnaires/${questionnaireId}/users/${userId}/answers`,
      data
    )
  }
}
