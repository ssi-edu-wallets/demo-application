import { axiosClient, axiosClientCookie } from './axiosClient'
import BaseUser from '../assets/interfaces/BaseUser'

export default class UserService {
  private store

  constructor (store: any) {
    this.store = store
  }

  async getAccessToken () {
    return await axiosClientCookie.get('/api/v2/me')
  }

  loginUser (email: string, password: string) {
    const payload = {
      grant_type: 'password',
      client_id: '5185',
      device_token: 'edu-front',
      password: password,
      username: email,
      remember_me: null
    }
    return axiosClientCookie.post('/oauth/token', payload)
  }

  registerUser (user: any) {
    return axiosClient.post('/api/v3/public/users', user)
  }

  async getUserData (userId: any): Promise<BaseUser> {
    const res: any = await axiosClient.get(`/api/v3/users/${userId}`)
    return res.data.data
  }

  updateUser (user: any, uid: number) {
    return axiosClient.patch(`/api/v3/users/${uid}`, user)
  }

  async getUsers (): Promise<BaseUser[]> {
    const res = await axiosClient.get('/api/v3/users')
    return res.data.data
  }

  async getUsersByQuestionnaires (answeredQnrId: number, unAnsweredQnrId: number): Promise<any[]> {
    const params = `questionnaire_answered=${answeredQnrId}&questionnaire_unanswered=${unAnsweredQnrId}`
    const url = '/edu/api/v1/usersQuestionnaires?' + params
    const res = await axiosClient.get(url)
    return res.data.data
  }

  async getUsersSearch (search: string): Promise<BaseUser[]> {
    const res = await axiosClient.get(`/api/v3/users?text=${search}&limit=10&page=1`)
    return res.data.data
  }

  async getSmeUsersSearch (smeId: number, search: string, userIds?: number[]): Promise<BaseUser[]> {
    let url = `/api/v3/sme/${smeId}/buyersManagement?text=${search}&page=1&limit=10`
    if (userIds?.length) {
      const stringIds = userIds.join(',')
      url += '&user_ids=' + stringIds
    }
    const res = await axiosClient.get(url)
    return res.data.data
  }

  getGenericPdf (data: any) {
    return axiosClient.post('/api/v3/genericPdf', data, {
      responseType: 'blob'
    })
  }

  getCompletedQuestionnaires () {
    return axiosClient.get('/edu/api/v1/questionnaires?only_answered=1')
  }

  renewToken () {
    return axiosClientCookie
      .post('/oauth/renew')
  }
}

export { UserService }
