import axios from 'axios'
import { BASE_ENDPOINT } from './constants'
import { state } from '@/store/modules/user'
import i18n from '@/plugins/i18n'
import router from '@/router'
import store from '@/store'

const axiosClientCookie = axios.create({
  baseURL: BASE_ENDPOINT,
  withCredentials: true,
  headers: {
    'Content-Type': 'application/json'
  }
})

const axiosClient = axios.create({
  baseURL: BASE_ENDPOINT,
  withCredentials: true,
  headers: {
    'Content-Type': 'application/json'
  }
})

axiosClient.interceptors.request.use(
  function (config) {
    const accessToken = state.user!.token.access_token // dynamic token
    const language = i18n.locale
    if (accessToken) {
      config.headers!.Authorization = `Bearer ${accessToken}`
    }
    if (language) {
      config.headers!['Accept-Language'] = language
    }
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)

axiosClient.interceptors.response.use((response: any) => {
  return response
}, (error: any) => {
  if (error.response.status === 401) {
    return router.replace({ name: 'login', params: { locale: store.getters['config/getLocale'] } })
  }
  return Promise.reject(error)
})

axiosClientCookie.interceptors.response.use((response: any) => {
  return response
}, (error: any) => {
  if ((error.response.status === 401 && router.currentRoute.name !== 'home') && (error.response.status === 401 && router.currentRoute.name !== 'signup')) {
    state.user = {
      id: 0,
      email: '',
      token: {
        access_token: ''
      },
      group_id: 0,
      grant_level: 0,
      parent_id: 0
    }
    return router.replace({ name: 'login', params: { locale: store.getters['config/getLocale'] } })
  }
})

export { axiosClient, axiosClientCookie }
