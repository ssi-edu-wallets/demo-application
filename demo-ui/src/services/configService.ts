import { axiosClient } from './axiosClient'

export default class ConfigService {
  getConfig () {
    return axiosClient.get('/api/v3/configs/mom_front')
  }
}
