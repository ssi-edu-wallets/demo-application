import { axiosClient } from './axiosClient'
import { RespondentEntity } from '../store/modules/admin/types'

export default class InitiativesService {
  getInitiatives () {
    return axiosClient.get('/edu/api/v1/initiatives/')
  }

  getUserInitiatives (payload: any) {
    const queryFilters = payload.submission_id ? `?submission_id=${payload.submission_id}` : ''
    if (payload.questionnaireId) {
      let url = `/edu/api/v1/questionnaires/${payload.questionnaireId}/users/${payload.userId}/initiatives/`
      if (queryFilters) {
        url += queryFilters
      }
      return axiosClient.get(url)
    }
    // return axiosClient.get(`/edu/api/v1/users/${payload.userId}/initiatives/`) // OLD
  }

  getUserInitiativesByDate (payload: any) {
    if (payload.questionnaireId) {
      return axiosClient.get(`/edu/api/v1/questionnaires/${payload.questionnaireId}/users/${payload.userId}/initiatives/?date:gte=${payload.lte}&date:lte=${payload.gte}`)
    }

    // return axiosClient.get(`/edu/api/v1/users/${userData.userId}/initiatives/?date:gte=${userData.lte}&date:lte=${userData.gte}`) // OLD
  }

  registerInitiative (initiative: any) {
    // return axiosClient.post('/edu/api/v1/questionnaires/${initiative.questionnaire_id}/initiatives/', initiative)
    return axiosClient.post('/edu/api/v1/initiatives/', initiative)
  }

  updateInitiative (id: any, initiative: any) {
    // return axiosClient.patch(`/edu/api/v1/questionnaires/${initiative.questionnaire_id}/initiatives/${id}`, initiative)
    return axiosClient.patch(`/edu/api/v1/initiatives/${id}`, initiative)
  }

  deleteInitiative (id: any) {
    // return axiosClient.delete(`/edu/api/v1/questionnaires/${initiative.questionnaire_id}/initiatives/${id}`)
    return axiosClient.delete(`/edu/api/v1/initiatives/${id}`)
  }

  registerStateInitiative (payload: any) {
    return axiosClient.patch(`/edu/api/v1/questionnaires/${payload.questionnaireId}/users/${payload.userId}/initiatives/${payload.initiativeId}`, payload.state)
  }

  registerFlagInitiative (initiative: any) {
    return axiosClient.patch(`/edu/api/v1/questionnaires/${initiative.questionnaireId}/users/${initiative.userId}/initiatives/${initiative.initiativeId}`, initiative.is_flagged)
  }

  getTags () {
    return axiosClient.get('/api/v1/tags')
  }

  getInitiativeScoreboard (questionnaireId: number) {
    // return axiosClient.get('/edu/api/v1/scorecard')
    return axiosClient.get(`/edu/api/v1/scorecard?questionnaire_id=${questionnaireId}`)
  }

  getInitiativeScoreboardByDate (gte: any, lte: any, questionnaireId: number, isHistoric: boolean, tags: string) {
    let tagsParam: string = ''
    if (tags) {
      tagsParam = '&tags=' + tags
    }
    let historicParam: string = ''
    if (isHistoric) {
      historicParam = '&historic=1'
    }
    return axiosClient.get(`/edu/api/v1/scorecard?date:gte=${lte}&date:lte=${gte}&questionnaire_id=${questionnaireId}` + tagsParam + historicParam)
  }

  getUserScoreboard (payload: any) {
    return axiosClient.get(`/edu/api/v1/scorecard?user_ids=${payload.id}&questionnaire_id=${payload.questionnaireId}`)
  }

  getUserScoreboardByDate (payload: any) {
    return axiosClient.get(`/edu/api/v1/scorecard?user_ids=${payload.id}&date:gte=${payload.lte}&date:lte=${payload.gte}&questionnaire_id=${payload.questionnaireId}`)
  }

  getInitiativeGroupState (questionnaireId: number) {
    return axiosClient.get(`/edu/api/v1/scorecard?group=stat&questionnaire_id=${questionnaireId}`)
  }

  getInitiativeGroupStateByDate (gte: any, lte: any, questionnaireId: number) { // **
    return axiosClient.get(`/edu/api/v1/scorecard?group=state&date:gte=${lte}&date:lte=${gte}&questionnaire_id=${questionnaireId}`)
  }

  getTopPerformers (questionnaireId: number) {
    // return axiosClient.get('/edu/api/v1/dashboard/top_performers')
    return axiosClient.get(`/edu/api/v1/dashboard/top_performers?questionnaire_id=${questionnaireId}`)
  }

  getTotalRespondents (questionnaireId: number) {
    // return axiosClient.get('/edu/api/v1/dashboard/total_score')
    return axiosClient.get(`/edu/api/v1/dashboard/total_score?questionnaire_id=${questionnaireId}`)
  }

  async getTotalRespondentsState (questionnaireId: number): Promise<RespondentEntity[]> {
    const res = await axiosClient.get(`/edu/api/v1/dashboard/total_state?questionnaire_id=${questionnaireId}`)
    return res.data.data
  }

  getScoreGrouped (questionnaireId: number) {
    return axiosClient.get(`/edu/api/v1/dashboard/score_grouped?questionnaire_id=${questionnaireId}`)
  }

  getScoreGroupedByDate (gte: any, lte: any, questionnaireId: number) {
    return axiosClient.get(`/edu/api/v1/dashboard/score_grouped?date:gte=${lte}&date:lte=${gte}&questionnaire_id=${questionnaireId}`)
    // return axiosClient.get(`/edu/api/v1/questionnaires/${questionnaireId}/dashboard/score_grouped?date:gte=${lte}&date:lte=${gte}`)
  }
}
