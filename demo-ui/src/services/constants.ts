const dataEnvElem = document.getElementById('replacedBeforeApp')
const dataEnv = JSON.parse(dataEnvElem?.getAttribute('data-env') || '{}')
const getFromDataEnv = function (varName:string) {
  return dataEnv[varName]
}
export const BASE_ENDPOINT = process.env.VUE_APP_API
export const SUB_PATH = process.env.VUE_APP_SUBPATH || getFromDataEnv('VUE_APP_SUBPATH')
export const PLATFORM = process.env.VUE_APP_PLATFORM || getFromDataEnv('VUE_APP_PLATFORM')

export const GROUP_ADMIN = 1
export const GROUP_SELLER = 4
export const GROUP_TRAINER = 5
export const GROUP_BUYER = 3

export const GRANT_LEVEL_SUPERVISOR = 2

export const ALL_USERS = 'const_all_users'
export const GO_TO_QUESTIONNAIRE = 'firstFieldQnr'
// filter dates

export const MONTH = 0
export const WEEK = 1
export const TO_DATE = 2

export const ALL_OPTIONS = 0
export const LOW_STATUS = 1
export const MED_STATUS = 2
export const ADVANCE_STATUS = 3
export const HIGH_STATUS = 4

export const NOT_STARTED = { label: 'not_started', id: 3, idAnother: 0 }
export const IN_PROGRESS = { label: 'in_progress', id: 1 }
export const COMPLETED = { label: 'completed', id: 2 }

export const EXTRA_LOW = 1
export const LOW = 2
export const MED = 3
export const ADVANCE = 4
export const HIGH = 5
export const CONSULTANT_QUESTIONNAIRE_SLUG = 'kombinierteselbst'
export const MAIN_QUESTIONNAIRE_SLUG = 'main'

export const THUMBS_PDF = 'paragraphs'
export const DEMO_MODAL_STORAGE_KEY = 'ct-demo-modal-step'
export const USER_PROFILE = {
  GOALS: [
    // {
    //   value: 'get_first_job',
    //   text: 'userProfile.goals.getFirstJob'
    // },
    {
      value: 'grow_current_role',
      text: 'userProfile.goals.growCurrentJob'
    },
    {
      value: 'switch_role',
      text: 'userProfile.goals.switchRole'
    }
    // {
    //   value: 'learn_outside_work',
    //   text: 'userProfile.goals.learnOutsideWork'
    // }
  ],
  SKILL_STATUS: {
    HAVE: 'have',
    WANT: 'want'
  }
}
export const XAPI = {
  objects: {
    types: {
      assessments: 'http://adlnet.gov/expapi/activities/assessment'
    }
  },
  verbs: {
    scored: {
      display: {
        'en-US': 'scored'
      },
      id: 'http://adlnet.gov/expapi/verbs/scored'
    }
  }
}
