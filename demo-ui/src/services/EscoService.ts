import axios from 'axios'
const axiosClientXApi = axios.create({
  baseURL: 'https://ec.europa.eu/esco/api/',
  headers: {
    'Content-Type': 'application/json'
  }
})
const occupationsSchema = 'http://data.europa.eu/esco/concept-scheme/occupations'
const skillsSchema = 'http://data.europa.eu/esco/concept-scheme/skills'
export default class EscoService {
  getOccupations (page: number = 0, limit: number = 100, query: string = '') {
    const filters = `?limit=${limit}&offset=${page}&text=${query}&isInScheme=${occupationsSchema}`
    return axiosClientXApi.get('/suggest2' + filters)
  }

  getSkills (page = 0, limit = 100, query = '') {
    const filters = `?limit=${limit}&offset=${page}&text=${query}&isInScheme=${skillsSchema}`
    return axiosClientXApi.get('/suggest2' + filters)
  }
}
