import { axiosClient } from './axiosClient'
import router from '@/router'
import store from '@/store'
import { Questionnaire } from '../store/modules/questionnaire/types'
import Field from '../assets/interfaces/Field'
import InitiativesAnswer from '../assets/interfaces/InitiativesAnswer'

export default class QuestionnaireService {
  getQuestionnaireId () {
    const qnr: Questionnaire = store.getters['questionnaire/getQuestionnaire']
    return (qnr)
      ? qnr.id
      : router.currentRoute.query.qnrId
  }

  getCategories () {
    return axiosClient.get('/api/v2/categories?sort_by=relevance')
  }

  async getPublicQuestionnaires (): Promise<Questionnaire[]> {
    const res = await axiosClient.get('/edu/api/v1/public/questionnaires')
    return res.data.data
  }

  async getQuestionnaires (): Promise<Questionnaire[]> {
    const res = await axiosClient.get('/edu/api/v1/questionnaires')
    return res.data.data
  }

  async getFields (): Promise<Field[]> {
    const res = await axiosClient.get('/edu/api/v1/fields')
    return res.data.data
  }

  registerQuestionnaire (questionnaire: any) {
    return axiosClient.post('/edu/api/v1/questionnaires', questionnaire)
  }

  updateQuestionnaire (questionnaire: any, id: number) {
    return axiosClient.patch(`/edu/api/v1/questionnaires/${id}`, questionnaire)
  }

  deleteQuestionnaire (id: number) {
    return axiosClient.delete(`/edu/api/v1/questionnaires/${id}`)
  }

  getQuestionnaire (slug: string) {
    return axiosClient.get(`/edu/api/v1/questionnaires/${slug}`)
  }

  getQuestions () {
    return axiosClient.get(`/edu/api/v1/questionnaires/${this.getQuestionnaireId()}/questions?sort_by=position`)
  }

  getQuestion (id: any) {
    return axiosClient.get(`/edu/api/v1/questionnaires/${this.getQuestionnaireId()}/questions/${id}/answers/`)
  }

  registerAnswer (idQuestion: any, answer: any) {
    return axiosClient.post(`/edu/api/v1/questionnaires/${this.getQuestionnaireId()}/questions/${idQuestion}/answers/`, answer)
  }

  updateAnswer (idQuestion: any, idAnswer: any, answer: any) {
    return axiosClient.patch(`/edu/api/v1/questionnaires/${this.getQuestionnaireId()}/questions/${idQuestion}/answers/${idAnswer}`, answer)
  }

  deleteAnswer (idQuestion: any, idAnswer: any) {
    return axiosClient.delete(`/edu/api/v1/questionnaires/${this.getQuestionnaireId()}/questions/${idQuestion}/answers/${idAnswer}`)
  }

  resisterQuestion (question: any) {
    return axiosClient.post(`/edu/api/v1/questionnaires/${this.getQuestionnaireId()}/questions/`, question)
  }

  updateQuestion (id: any, question: any) {
    return axiosClient.patch(`/edu/api/v1/questionnaires/${this.getQuestionnaireId()}/questions/${id}`, question)
  }

  deleteQuestion (id: any) {
    return axiosClient.delete(`/edu/api/v1/questionnaires/${this.getQuestionnaireId()}/questions/${id}`)
  }

  registerAnswerScore (questionnaireId: number, userId: number, data: InitiativesAnswer) {
    return axiosClient.post(`/edu/api/v1/questionnaires/${questionnaireId}/users/${userId}/initiatives`, data)
  }
}
