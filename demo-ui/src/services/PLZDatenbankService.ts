import axios from 'axios'
const axiosClientXApi = axios.create({
  baseURL: 'https://public.opendatasoft.com',
  headers: {
    'Content-Type': 'application/json'
  }
})
export default class PLZDatenbankService {
  getRecords (page: number = 0, limit: number = 100, query: string = '') {
    let filters = `&rows=${limit}&start=${page}`
    if (query) {
      filters += `&q=${query}`
    }
    return axiosClientXApi.get('/api/records/1.0/search/?dataset=georef-germany-postleitzahl' + filters)
  }
}
