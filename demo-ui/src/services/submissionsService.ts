import { axiosClient } from './axiosClient'

export default class SubmissionsService {
  getSubmissions (userId: number) {
    return axiosClient.get(
      `/edu/api/v1/users/${userId}/submissions`)
  }
}
