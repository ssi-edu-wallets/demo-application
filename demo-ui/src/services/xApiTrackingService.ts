import axios from 'axios'
import { BASE_ENDPOINT } from './constants'
const axiosClientXApi = axios.create({
  baseURL: BASE_ENDPOINT,
  withCredentials: true,
  headers: {
    'Content-Type': 'application/json',
    'X-Experience-API-Version': '1.0.1'
  }
})
export default class XApiTrackingService {
  postStatement (statement: any, userId: number, userToken: string) {
    const userCredentials = btoa(userId + ':' + userToken)
    const config = {
      headers: {
        Authorization: 'Basic ' + userCredentials
      }
    }
    return axiosClientXApi.post('/edu/api/v1/xAPIProxy/statements', statement, config)
  }

  getStatements (userId: number, userToken: string, filters: string) {
    const userCredentials = btoa(userId + ':' + userToken)
    const config = {
      headers: {
        Authorization: 'Basic ' + userCredentials
      }
    }
    return axiosClientXApi.get('/edu/api/v1/xAPIProxy/statements?' + filters, config)
  }
}
