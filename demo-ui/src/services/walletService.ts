import { axiosClient } from './axiosClient'
export default class WalletService {
  getVcList (uid: number) {
    return axiosClient.get('/edu/api/v1/wallet/users/' + uid + '/issuance')
  }

  issueVc (uid: number, vcId: string, sessionId: any, xDevice: boolean) {
    let isPreAuthorized
    if (sessionId) {
      isPreAuthorized = undefined
    } else {
      sessionId = undefined
      isPreAuthorized = true
    }
    const fullPayload = {
      vc_id: vcId,
      session_id: sessionId,
      isPreAuthorized,
      xDevice: xDevice
    }
    return axiosClient.post('/edu/api/v1/wallet/users/' + uid + '/issuance', fullPayload)
  }

  verifyVc (token: any) {
    const payload = { wallet_token: token }
    return axiosClient.post('/edu/api/v1/wallet/verifier/store', payload)
  }
}
