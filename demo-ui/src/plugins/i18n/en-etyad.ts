export default {
  Step: {
    developerData: 'Trainee Data'
  },
  Form: {
    sectionTitle: 'Trainee Data',
    companyName: 'Trainee Name'
  },
  Scoreboard: {
    selectDeveloper: 'Select Trainee',
    barchartTitle: 'Number of Trainee Progressing with Initiatives',
    doughnutchartTitle: 'Trainee Response',
    selectUser: 'Select a Trainee',
    allDevelopers: 'All Trainee',
    takeSurvey: 'Take assessment as Trainee'
  },
  pdf: {
    header: {
      title: 'Ertyad',
      subtitle: 'Transitional plan to support the trainee'
    },
    meta: {
      title: 'Transitional plan to support the trainee'
    },
    report_p: {
      report_1: 'In line with the vision of the Kingdom 2030, and as part of the programs of the Ministry of Municipal and Rural Affairs and Housing in upgrading the trainee training sector, we present to you a report planned to support your trainee,',
      report_2: 'A set of initiatives has been identified within a transitional plan designed with the aim of addressing weaknesses and helping the trainee to raise the level of efficiency in the identified areas as weak or medium-level, which emerged through the diagnostic review that was carried out with your participation.'
    }
  },
  footerText: 'Ertyad Copyright 2023'
}
