export const defaultLocale = 'en'

export const locales = [
  {
    code: 'en',
    name: 'English',
    rtl: false
  },
  {
    code: 'ar',
    name: 'لغة_عربية',
    rtl: true
  },
  {
    code: 'de',
    name: 'Deutsch',
    rtl: false
  }
]
