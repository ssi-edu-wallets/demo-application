export default {
  Esco: {
    disclaimer: 'Dieser Dienst verwendet die ESCO-Klassifizierung der Europäischen Kommission.'
  },
  User: {
    email: 'E-mail Adresse',
    password: 'Passwort',
    selector: 'Wie definieren Sie sich selbst?',
    errorPassword: 'Passwörter müssen übereinstimmen.',
    errorEmail: 'Diese E-Mail Adresse wird bereits verwendet'
  },
  Navbar: {
    initiatives: 'Kompetenzen',
    questions: 'Fragen',
    scoreboard: 'Scoreboard',
    dashboard: 'Übersicht',
    home: 'zurück zur Lernplattform',
    logout: 'Abmelden',
    questionnaires: 'Assessments',
    fields: 'Bereiche'
  },
  Home: {
    title: 'Educational Plattformen',
    personality: 'Personality',
    dashboard: 'Dashboard',
    diaganosis_survey: 'Diaganosis Survey',
    analysis: 'Analysis',
    actions: 'Actions',
    read_more: 'Mehr anzeigen'
  },
  Signup: {
    signUp: 'Registrieren',
    confirmPassword: 'Passwort bestätigen',
    helpSection: {
      sectionOne: {
        title: '',
        desc: ''
      },
      sectionTwo: {
        title: '',
        desc: ''
      }
    },
    legal: 'Datenschutz',
    messageRegister: 'Erfolgreich registrierter Nutzer',
    privacyTerms: {
      start: 'Ich erteile meine Einwilligung zur Verarbeitung meiner Daten. Mehr Informationen dazu finden Sie in unserer',
      link: 'Datenschutzerklärung'
    },
    serviceTerms: {
      start: 'Ich erkläre mich mit den',
      link: 'AGB',
      end: 'einverstanden'
    },
    registerNew: 'Jetzt registrieren',
    clear: 'CLEAR'
  },
  Login: {
    login: 'Anmelden',
    messageLogin: 'Erfolgreich angemeldet'
  },
  Step: {
    consultantReport: 'Reports',
    warning: 'WARNUNG',
    formIncomplete: 'Sie haben das Assessment noch nicht abgeschlossen. Alle Eingaben gehen verloren.',
    noContent: 'Inhalt nicht gefunden',
    developerData: 'Benutzerdaten',
    personalAdvice: '',
    transitionPlans: 'Ergebnis',
    report: 'Report',
    submissionForm: 'Zu besetzende Stelle'
  },
  Form: {
    edit: 'Bearbeiten',
    nextStep: 'nächster Schritt',
    editTitle:
      'Im Folgenden finden Sie die grundlegenden Details zu Ihrer Organisation.',
    sectionTitle: 'Benutzerdaten',
    sectionTitleEmployer: 'Unternehmensprofil',
    companyName: 'Unternehmen',
    consultantEditTitle:
      'Im Folgenden finden Sie die grundlegenden Details zu Ihrer Organisation.',
    consultantSectionTitle: 'Benutzerdaten',
    citiesOperation: 'Cities with operations',
    numberEmployees: 'Employees amount',
    ongoingProjects: 'Number of ongoing projects',
    totalProjects: 'Number of total projects',
    city: {
      Riyadh: 'Riyadh',
      Makkah: 'Makkah',
      Madinah: 'Madinah',
      Tabuk: 'Tabuk',
      AlJawf: 'Al-Jawf',
      Hail: 'Hail',
      AlQassim: 'Al Qassim',
      NorthernBordersProvince: 'Northern Borders Province',
      EasternProvince: 'Eastern Province',
      Najran: 'Najran',
      Jazan: 'Jazan',
      Aseer: 'Aseer',
      AlBahah: 'AlBahah'
    }
  },
  Questionnaire: {
    alternativeQuestions: 'Alternative Fragen',
    answerLabel: 'In dieser Situation fühle ich mich …',
    consultantSurveyTitle:
      'Durch Eingabe der Zahl über die Tastatur 1/2/3/... und Enter-Taste können Sie die Antworten direkt einreichen.',
    questionnaires: 'Assessments',
    title: 'Assessments titel',
    type: {
      label: 'Layout',
      default: 'Vertikal',
      slider: 'Slider',
      sliderNavigation: '_Slider with navigation'
    },
    newQuestionnaire: 'Neues Assessment',
    editQuestionnaire: 'Assessment bearbeiten',
    slug: 'slug',
    surveyTitle:
      'Durch Eingabe der Zahl über die Tastatur 1/2/3/... und Enter-Taste können Sie die Antworten direkt einreichen.',
    formComplete: 'Sie haben diesen Assessment-Abschnitt bereits vervollständigt. Bitte fahren Sie mit dem nächsten Schritt fort.',
    finishForms: 'Sie habem das Assessment erfolgreich fertig durchgearbeitet. Alle Eingaben wurden gespeichert.',
    finishAndSubmit: 'Auswertung abschließen',
    nextForm: 'zum nächsten Abschnitt',
    noAnswer: 'Kann ich nicht beantworten',
    question: 'F:',
    questionText: 'F:{number} {question}',
    retakeSurvey: 'Assessment wiederholen',
    retakeSurveySubmission: 'neues Stellenprofil anlegen',
    emptyAnswer: 'Das Assessment muss ausgefüllt werden'
  },
  ActionInitiative: {
    bookAppointment: '',
    combinedMissing: 'Ergebnis zu dieser Kompetenz liegt nur als Selbsteinschätzung vor.',
    buyerCombinedDesc: '',
    consultantDesc: '',
    sectionTitle: 'Ergebnis',
    seeMore: 'Mehr anzeigen',
    totalFunctionScore: 'Kompetenzlevel',
    total: 'Gesamt ',
    score: 'Bewertung',
    showAllFunctions: 'Kompetenzfelder',
    actionInitiative: 'Kompetenzfeld:  ',
    actionInitiativeTitle: 'Kompentenz Titel',
    actionInitiativeSubtitle: 'Sehen Sie sich Ihre Kompetenzen im Detail an und entscheiden Sie welche für Sie und Ihre Organisation essentiell sind durch Klick ("Markieren"). Die Kompetenzen, die für Sie höchste Priorität haben können Sie hervorheben ("Flagge").',
    Select: {
      sectionTitle: 'Report',
      exportActionInitiatives: 'Report exportieren',
      checkReport: 'Report anzeigen',
      actionInitiativeSubtitle: 'Folgende Kompetenzen müssen entwickelt werden. Sollten Sie zu einer Kompetenz Weiterbildungsmaßnahmen zur Verbesserung vornehmen, so setzen Sie den Status in "in Bearbeitung". Sollten alle Maßnahmen eine Kompetenz zu entwickeln ausgeschöpft haben, setzen Sie diese auf "Abgeschlossen".',
      score: 'Level',
      status: 'Status',
      flag: 'Flagge',
      initiativeState: 'Status auswählen'
    }
  },
  Questions: {
    questions: 'Fragen',
    editQuestion: 'Frage bearbeiten',
    newQuestion: 'Frage hinzufügen',
    tag: 'Tag:',
    questionCreated: 'Frage wurde erstellt',
    questionUpdated: 'Frage wurde aktualisiert',
    questionDeleted: 'Frage wurde gelöscht',
    position: 'Position'
  },
  Initiatives: {
    initiatives: 'Kompetenzen',
    newInitiative: 'Kompetenz hinzufügen',
    editInitiative: 'Kompetenz bearbeiten',
    initiativeTitle: 'Titel:',
    initiativeDescription: 'Beschreibung',
    initiativeTags: 'Tag Liste',
    initiativeCreated: 'Kompetenz wurde erstellt',
    initiativeUpdated: 'Kompetenz wurde aktualisiert',
    initiativeDeleted: 'Kompetenz wurde gelöscht'
  },
  Answers: {
    answers: 'Antworten',
    answer: 'Antwort',
    newAnswer: 'Neue Antwort hinzufügen',
    editAnswer: 'Antwort bearbeiten',
    weight: 'Gewicht',
    answerCreated: 'Antwort wurde erstellt',
    answerUpdated: 'Antwort wurde aktualisiert',
    answerDeleted: 'Antwort wurde gelöscht'
  },
  Scoreboard: {
    toDate: 'Alle',
    thisWeek: 'Woche',
    thisMonth: 'Monat',
    selectDeveloper: 'Mitarbeiter auswählen',
    barchartTitle: 'Kompetenzen in Entwicklung',
    barchartSubtitle: 'alle Mitarbeiter die Kompetenzen entwickeln',
    doughnutchartTitle: 'Mitarbeiter Status',
    doughnutchartSubtitle: 'Kompetenzen in Entwicklung',
    statsTitle: 'Populärste Kompetenzen in Entwicklung',
    actionTitle: 'Kompentenzentwicklung erfassen',
    selectFunction: 'Gruppierung auswählen',
    initiativesStatus: 'Kompetenz Status',
    initiativesEmpty: 'Keine Ergebnisse',
    selectUser: 'Mitarbeiter auswählen',
    allDevelopers: 'Alle Benutzer',
    takeSurvey: 'Vervollständige das Assessement'
  },
  Dashboard: {
    totalScore: 'Gesamt pro Gruppierung',
    topPerformance: 'Top 3 Mitarbeiter',
    totalRespondents: 'Aktivität',
    totalRespondentsRange: 'Ergebnisverteilung über Kompetenzen',
    totalRespondentsFunction: 'Gesamtzahl der Befragten nach Bewertungsbereich je Gruppe',
    performanceTrend: 'Gruppierter Leistungstrend',
    rangeDate: 'Datumsbereich',
    scoreRange: 'Levels ',
    selectFunction: 'Gruppierung',
    allScoreRange: 'Alle Auswertungen',
    allFunctions: 'Alle Gruppen',
    apply: 'Anwenden',
    listQuestionnaires: 'Assessments'
  },
  DemoModals: {
    adminDashboardExample: {
      title: 'Kompetenzentwicklung im Unternehmen veranschaulichen',
      text: '<ul><li>Eine Erhebung von Kompetenzen kann in regelmäßigen Abständen und über den Einsatz verschiedener Kompetenzmodelle erfolgen.</li><li>Das Administrations-Dashboard ermöglicht umfassende Gruppierungen, Auswertungen, sowie Visualisierung zeitlicher Entwicklung einzelner Kompetenzen.</li></ul>'
    },
    redirectDialog: {
      title: 'Weiterleitung in die personalisierte Lernwelt',
      text: '<ul><li>Folgen Sie den persönlichen Empfehlungen und starten Sie in den Lernprozess.</li><li>Wählen Sie im nächsten Schritt zwischen bestehenden Kursangeboten.</li></ul>'
    },
    saveResultsModal: {
      title: 'Ergebnisse in Ihre persönliche Wallet ablegen',
      text: '<ul><li>All Ihre Aktivitäten werden im Hintergrund aggregiert.</li><li>Die Liste veranschaulicht gesammelte Kompetenz-Matchings in einem konformen Format z.B. zur weiteren Verarbeitung durch eine KI.</li><li>Während der Testung erfolgt keine Speicherung über SSI/Wallet aufgrund der vorangehenden SSO-Authentifizierung über Innovationswettbewerb INVITE - Startseite</li></ul>'
    },
    welcomeDialog: {
      title: 'Willkommen zur Testung des Prototyps!',
      text: '<ul><li>Die Journey wird lediglich einen Bruchteil der aktuellen Forschungs- & Entwicklungsarbeiten widerspiegeln und soll ein Gefühl für einen möglichen Einsatz in einer Bildungsplattform geben.</li><li>Alle Inhalte und Daten sowie Mechanismen der Datensicherheit und -speicherung (z.B. imaginäre Wallet) dienen Demonstrationszwecken.</li><li>Ihre Daten werden nach dem 20.12.2022 vollständig & unwiderruflich gelöscht.</li><br><p>Wir wünschen viel Spaß beim durchklicken!</p></ul>'
    }
  },
  Percentages: {
    low: '50%',
    medium: '51-71%',
    advance: '71-90%',
    high: '91-100%'
  },
  pdf: {
    header: {
      title: '_Lorem ipsum title',
      subtitle: '_Your skills report',
      footer: '_Lorem'
    },
    meta: {
      title: 'Report'
    },
    footer: {
      page: 'Seite: {0}'
    },
    field: {
      header: {
        efficiency: 'Effizienz',
        component: 'Gruppe',
        supportive: 'Informationen'
      },
      title: {
        functions: 'Gruppen',
        result: 'Ergebnis:',
        percent: '{n}%'
      }
    },
    general: {
      title: '_1.- General information'
    },
    overall: {
      title: '2.- Ergebnisse der diagnostischen Gesamtbetrachtung',
      function: '{fn} Gruppen: {num}%'
    },
    fieldTitles: {
      fieldLow: '',
      fieldMed: '',
      fieldAdv: '',
      fieldHigh: ''
    },
    report_p: {
      report_1: '_Lorem 1 ipsum dolor sit amet, consectetur adipiscing elit. Donec pellentesque, leo et porttitor aliquam, turpis mi ornare dolor, in sagittis lectus felis vel lorem. Ut euismod quam tristique massa 1 et.',
      report_2: '_Lorem 2 ipsum dolor sit amet, consectetur adipiscing elit. Quisque ut dictum neque, eget dignissim nulla. Nam pharetra est dui, nec consequat nibh sodales eu. Ut consectetur id orci varius tempor. Fusce dolor turpis, dictum vitae pellentesque vehicula, varius eget nisi. Nam nec dignissim lectus, ut sodales ipsum. Nam posuere 2.',
      report_3: '_Lorem 3 ipsum dolor sit amet, consectetur adipiscing elit. Donec molestie rhoncus nunc, quis ultrices mi blandit eu. Etiam a elit a lectus commodo tristique. Vestibulum finibus vehicula consectetur. Aliquam feugiat ipsum quis quam aliquam, nec vulputate lacus euismod. Pellentesque efficitur eros odio, vel rhoncus lorem porttitor sit amet. Nullam fringilla hendrerit dolor a venenatis. Integer pulvinar fermentum lorem, in accumsan ante condimentum et. Mauris suscipit eros non leo vehicula efficitur. Donec eleifend neque lorem. Phasellus sit amet purus sed 3 non',
      report_4: ''
    },
    report_title: {
      subtitle: ''
    }
  },
  footerText: 'Copyright 2022',
  ok: 'OK',
  confirmDelete: 'Möchten Sie dieses Element wirklich löschen?',
  inputRequired: 'Verpflichtend',
  next: 'Weiter',
  passwordLength: 'Das Passwort sollte mindestens aus 6 Zeichen bestehen.',
  search: 'Suche',
  cancel: 'Abbrechen',
  save: 'Speichern',
  yes: 'JA',
  no: 'NEIN',
  extraLow: 'Extra low',
  low: 'Low',
  medium: 'Medium',
  advance: 'Advance',
  high: 'High',
  all: 'Alle',
  completed: 'Abgeschlossen',
  inProgress: 'in Bearbeitung',
  notStarted: 'nicht gestartet',
  error: 'Es ist ein Fehler aufgetreten',
  userProfile: {
    createProfile: 'Speichern',
    createdProfile: '_User profile successfully updated',
    goalDesc: '_Tell us your goals',
    goals: {
      growCurrentJob: '_Grow in my current role',
      switchRole: '_Switch to a different role'
    },
    industry: '_Set your industry',
    mainGoal: '_Set your learning plan',
    openUserProfile: '_Open user profile',
    occupation: '_I am currently a',
    profile: '_Profile',
    skillsDesired: '_I am interested in learning these skills',
    skillsOwned: '_I have these skills',
    targetOccupation: '_I want to become a',
    userProfile: '_Personalize your experience'
  },
  wallet: {
    errorVerification: '_Error verifying credential',
    importVcs: '_Import from wallet',
    issuance: 'Ausstellen eines Bildungsnachweises',
    openUserProfile: '_Open user profile',
    qrCodeIssuance: 'Scannen Sie den QR-Code mit Ihrer Wallet-App, um den Eintrag in Ihrer Wallet abzulegen.',
    validCredential: '_Valid credential',
    verify: '_Verify wallet certificate'
  }
}
