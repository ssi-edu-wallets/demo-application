const mergeTranslations = (file: string, $i18n: any) => {
  const newTranslations: any = require('./' + file)
  if (newTranslations?.default) {
    $i18n.mergeLocaleMessage($i18n.locale, newTranslations.default)
  }
}

const setCustomLocale = (customLocale: string, $i18n: any, saveInLocalStorage: boolean = true) => {
  try {
    const key = 'defaultCustomLocaleName'
    let name: string = customLocale
    if (name) {
      if (saveInLocalStorage) {
        window.localStorage.setItem(key, name)
      } else {
        window.localStorage.removeItem(key)
      }
    } else {
      name = window.localStorage.getItem(key) ? window.localStorage.getItem(key) + '' : ''
    }
    if (name) {
      mergeTranslations(name, $i18n)
    }
  } catch (err) {
    console.error('There is no custom file for the current locale', err)
    mergeTranslations($i18n.locale, $i18n)
  }
}

export default setCustomLocale
