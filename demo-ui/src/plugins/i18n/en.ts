export default {
  Esco: {
    disclaimer: 'This service uses the ESCO classification of the European Commission.'
  },
  User: {
    email: 'Email',
    password: 'Password',
    selector: 'How do you define yourself?',
    errorPassword: 'Passwords must be the same.',
    errorEmail: 'email already registered'
  },
  Navbar: {
    initiatives: 'Initiatives',
    questions: 'Questions',
    scoreboard: 'Scoreboard',
    dashboard: 'Dashboard',
    home: 'Home',
    logout: 'Logout',
    questionnaires: 'Questionnaires',
    fields: 'Fields'
  },
  Home: {
    title: 'Title',
    personality: 'Personality',
    dashboard: 'Dashboard',
    diaganosis_survey: 'Diaganosis Survey',
    analysis: 'Analysis',
    actions: 'Actions',
    read_more: 'Read More'
  },
  Signup: {
    signUp: 'Sign up',
    confirmPassword: 'Confirm password',
    helpSection: {
      sectionOne: {
        title: '',
        desc: ''
      },
      sectionTwo: {
        title: '',
        desc: ''
      }
    },
    legal: 'Legal',
    messageRegister: 'Successfully registered user',
    privacyTerms: {
      start: 'I give my consent to the processing of my data. You can find more information on this in our',
      link: 'privacy policy',
      end: ''
    },
    serviceTerms: {
      start: 'I agree to the',
      link: 'terms and conditions',
      end: ''
    },
    registerNew: 'REGISTER NEW',
    clear: 'CLEAR'
  },
  Login: {
    login: 'Login',
    messageLogin: 'Successfully login'
  },
  Step: {
    consultantReport: 'Reports',
    warning: 'WARNING',
    formIncomplete: 'You have not finished this quiz yet, if you exit all progress will be lost.',
    noContent: 'Content not found',
    developerData: 'Developer Data',
    personalAdvice: '',
    transitionPlans: 'Transition Plans',
    report: 'Report',
    submissionForm: 'Position to be filled'
  },
  Form: {
    edit: 'Edit',
    nextStep: 'next step',
    editTitle:
      'Following are the basic details about your organization. Keep this details up-to-date as you progress through this initiative.',
    sectionTitle: 'Developer Data',
    sectionTitleEmployer: 'Company profile',
    companyName: 'Company name',
    consultantEditTitle:
      'Following are the basic details about your organization. Keep this details up-to-date as you progress through this initiative.',
    consultantSectionTitle: 'Developer Data',
    citiesOperation: 'Cities with operations',
    numberEmployees: 'Employees amount',
    ongoingProjects: 'Number of ongoing projects',
    totalProjects: 'Number of total projects',
    city: {
      Riyadh: 'Riyadh',
      Makkah: 'Makkah',
      Madinah: 'Madinah',
      Tabuk: 'Tabuk',
      AlJawf: 'Al-Jawf',
      Hail: 'Hail',
      AlQassim: 'Al Qassim',
      NorthernBordersProvince: 'Northern Borders Province',
      EasternProvince: 'Eastern Province',
      Najran: 'Najran',
      Jazan: 'Jazan',
      Aseer: 'Aseer',
      AlBahah: 'AlBahah'
    }
  },
  Questionnaire: {
    alternativeQuestions: 'Alternative questions',
    answerLabel: 'In this situation I feel...',
    consultantSurveyTitle:
      'Press keys corresponding to the numbered choices, like “1/2/3/Enter” on your keyboard to answer the following Diaganostic Survey.',
    questionnaires: 'Questionnaires',
    title: 'Questionnaire title',
    type: {
      label: 'Layout',
      default: 'Vertical',
      slider: 'Slider',
      sliderNavigation: 'Slider with navigation'
    },
    newQuestionnaire: 'New Questionnaire',
    editQuestionnaire: 'Edit Questionnaire',
    slug: 'Slug',
    surveyTitle:
      'Press keys corresponding to the numbered choices, like “1/2/3/Enter” on your keyboard to answer the following Diaganostic Survey.',
    formComplete: 'You have already answered this questionnaire, continue with the next one.',
    finishForms: 'You are done with all the quizzes, the answers have been saved.',
    finishAndSubmit: 'Finish and submit',
    nextForm: 'Next Form',
    noAnswer: 'I can not answer',
    question: 'Q:',
    questionText: 'Q:{number} {question}',
    retakeSurvey: 'Retake the survey',
    retakeSurveySubmission: 'create a new job profile',
    emptyAnswer: 'The questionnaire cannot be sent empty'
  },
  ActionInitiative: {
    bookAppointment: '',
    combinedMissing: 'The result of this competence is only available as a self-assessment.',
    buyerCombinedDesc: '',
    consultantDesc: '',
    sectionTitle: 'Transition Plans',
    seeMore: 'See more',
    totalFunctionScore: 'Total Function Score',
    total: 'Total ',
    score: 'Score',
    showAllFunctions: 'show all functions',
    actionInitiative: 'Action Initiative: ',
    actionInitiativeTitle: 'Action initiative title',
    actionInitiativeSubtitle: 'Review the following action initiatives and select the ones that are crucial for your organization. Also, flag the initiatives that you would like to prioritise for your growth.',
    Select: {
      sectionTitle: 'Report',
      exportActionInitiatives: 'Export Action Initiatives',
      checkReport: 'Check report',
      actionInitiativeSubtitle: 'Following are the recommended action initiatives that you need to undertake. As you initiate and complete each initiative, change the status to “In Progress” or “Completed”.',
      score: 'score',
      status: 'status',
      flag: 'Flag',
      initiativeState: 'Select state'
    }
  },
  Questions: {
    questions: 'Questions',
    editQuestion: 'Edit question',
    newQuestion: 'New question',
    tag: 'Tag:',
    questionCreated: 'Question created',
    questionUpdated: 'Question Updated',
    questionDeleted: 'Question deleted',
    position: 'Position'
  },
  Initiatives: {
    initiatives: 'Initiatives',
    newInitiative: 'New Initiative',
    editInitiative: 'Edit Initiative',
    initiativeTitle: 'Title:',
    initiativeDescription: 'Description:',
    initiativeTags: 'Tag List',
    initiativeCreated: 'Initiative created',
    initiativeUpdated: 'Initiative Updated',
    initiativeDeleted: 'Initiative deleted'
  },
  Answers: {
    answers: 'Answers:',
    answer: 'Answer',
    newAnswer: 'New Answer',
    editAnswer: 'Edit Answer',
    weight: 'Weight',
    answerCreated: 'Answer created',
    answerUpdated: 'Answer Updated',
    answerDeleted: 'Answer deleted'
  },
  Scoreboard: {
    toDate: 'To Date',
    thisWeek: 'This Week',
    thisMonth: 'This Month',
    selectDeveloper: 'Select Developer',
    barchartTitle: 'Number of Developers Progressing with Initiatives',
    barchartSubtitle: 'Barchart Subtitle',
    doughnutchartTitle: 'Developers Response',
    doughnutchartSubtitle: 'Doughnut Subtitle',
    statsTitle: 'Initiative with maximum value within each status type',
    actionTitle: 'Track Action Initiatives',
    selectFunction: 'Select Function',
    initiativesStatus: 'Action Initiatives Status',
    initiativesEmpty: 'No Action Initiatives',
    selectUser: 'Select a user',
    allDevelopers: 'All Developers',
    takeSurvey: 'Take assessment as user'
  },
  Dashboard: {
    totalScore: 'Total Score by Function',
    topPerformance: 'Top 3 Performance',
    totalRespondents: 'Total # of Respondents',
    totalRespondentsRange: 'Total number of respondents by score range',
    totalRespondentsFunction: 'Total number of respondents by score range for each function',
    performanceTrend: 'Functional Performance Trend',
    rangeDate: 'Range Date',
    scoreRange: 'Score Range',
    selectFunction: 'Select Function',
    allScoreRange: 'All Score Range',
    allFunctions: 'All Functions',
    apply: 'Apply',
    listQuestionnaires: 'Questionnaires'
  },
  DemoModals: {
    adminDashboardExample: {
      title: 'Illustrate competence development within the company',
      text: '<ul><li>A survey of competences can be conducted at regular intervals and using various competence models. The administration dashboard allows comprehensive grouping, evaluations, and visualization of the temporal development of individual competences.</li></ul>'
    },
    redirectDialog: {
      title: 'Forwarding to the personalized learning world',
      text: '<ul><li>Follow the personal recommendations and start the learning process.</li><li>In the next step, choose between existing courses.</li></ul>'
    },
    saveResultsModal: {
      title: 'Store results in your personal wallet',
      text: '<ul><li>All your activities are aggregated in the background.</li><li>The list illustrates collected competency matchings in a compliant format, e.g. for further processing by an AI.</li><li>During the test, there is no storage via SSI/Wallet due to the previous SSO authentication via the INVITE innovation competition - start page.</li></ul>'
    },
    welcomeDialog: {
      title: 'Welcome to testing the prototype!',
      text: '<ul><li>The journey will only reflect a fraction of the current research and development work and should give a feeling for a possible use in an educational platform.</li><li>All content and data as well as data security and storage mechanisms (e.g. imaginary wallet) are for demonstration purposes.</li><li>Your data will be completely and irrevocably deleted after December 20th, 2022.</li><br><p>We hope you enjoy clicking through!</p></ul>'
    }
  },
  Percentages: {
    low: '50%',
    medium: '51-71%',
    advance: '71-90%',
    high: '91-100%'
  },
  pdf: {
    header: {
      title: 'Lorem ipsum title',
      subtitle: 'Your skills report',
      footer: '| 1443 success 28'
    },
    meta: {
      title: 'Skills'
    },
    footer: {
      page: 'Page: {0}'
    },
    field: {
      header: {
        efficiency: 'Efficiency',
        component: 'Component',
        supportive: 'Supportive initiative'
      },
      title: {
        functions: 'functions',
        result: 'result:',
        percent: '{n}%'
      }
    },
    general: {
      title: '1.- General information'
    },
    overall: {
      title: '2.- Results of the overall diagnostic review',
      function: '{fn} function: {num}%'
    },
    fieldTitles: {
      fieldLow: '',
      fieldMed: '',
      fieldAdv: '',
      fieldHigh: ''
    },
    report_p: {
      report_1: 'Lorem 1 ipsum dolor sit amet, consectetur adipiscing elit. Donec pellentesque, leo et porttitor aliquam, turpis mi ornare dolor, in sagittis lectus felis vel lorem. Ut euismod quam tristique massa 1 et.',
      report_2: 'Lorem 2 ipsum dolor sit amet, consectetur adipiscing elit. Quisque ut dictum neque, eget dignissim nulla. Nam pharetra est dui, nec consequat nibh sodales eu. Ut consectetur id orci varius tempor. Fusce dolor turpis, dictum vitae pellentesque vehicula, varius eget nisi. Nam nec dignissim lectus, ut sodales ipsum. Nam posuere 2.',
      report_3: 'Lorem 3 ipsum dolor sit amet, consectetur adipiscing elit. Donec molestie rhoncus nunc, quis ultrices mi blandit eu. Etiam a elit a lectus commodo tristique. Vestibulum finibus vehicula consectetur. Aliquam feugiat ipsum quis quam aliquam, nec vulputate lacus euismod. Pellentesque efficitur eros odio, vel rhoncus lorem porttitor sit amet. Nullam fringilla hendrerit dolor a venenatis. Integer pulvinar fermentum lorem, in accumsan ante condimentum et. Mauris suscipit eros non leo vehicula efficitur. Donec eleifend neque lorem. Phasellus sit amet purus sed 3 non',
      report_4: ''
    },
    report_title: {
      subtitle: ''
    }
  },
  footerText: 'Copyright 2022',
  ok: 'OK',
  confirmDelete: 'Are you sure you want to delete this item?',
  inputRequired: 'This field is required',
  next: 'Continue',
  passwordLength: 'Password should be at least 6 characters long.',
  search: 'Search',
  cancel: 'Cancel',
  save: 'Save',
  yes: 'YES',
  no: 'NO',
  extraLow: 'Extra low',
  low: 'Low',
  medium: 'Med',
  advance: 'Adv',
  high: 'High',
  all: 'All',
  completed: 'Completed',
  inProgress: 'In progress',
  notStarted: 'Not started',
  error: 'An error has occurred',
  userProfile: {
    createProfile: 'Save',
    createdProfile: 'User profile successfully updated',
    goalDesc: 'Tell us your goals',
    goals: {
      growCurrentJob: 'Grow in my current role',
      switchRole: 'Switch to a different role'
    },
    industry: 'Set your industry',
    mainGoal: 'Set your learning plan',
    openUserProfile: 'Open user profile',
    occupation: 'I am currently a',
    profile: 'Profile',
    skillsDesired: 'I am interested in learning these skills',
    skillsOwned: 'I have these skills',
    targetOccupation: 'I want to become a',
    userProfile: 'Personalize your experience'
  },
  wallet: {
    errorVerification: 'Error verifying credential',
    importVcs: 'Import from wallet',
    issuance: 'Issue wallet certificate',
    openUserProfile: 'Open user profile',
    qrCodeIssuance: 'Scan the Qr code to start the issuance',
    validCredential: 'Valid credential',
    verify: 'Verify wallet certificate'
  }
}
