export default {
  Home: {
    title: 'Educational Predictive Analytics API für personalisierte Learning Experience Plattformen'
  },
  footerText: 'LXP',
  Navbar: {
    home: 'zur Lernplattform wechseln'
  }
}
