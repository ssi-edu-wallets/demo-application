export default {
  ActionInitiative: {
    bookAppointment: 'Termin buchen',
    actionInitiative: 'Future Skill: ',
    buyerCombinedDesc: 'Hinweis: Das Ergebnis setzt sich aus deiner Einschätzung und der Einschätzung deiner Berater:in zusammen',
    consultantDesc: 'Hinweis: Das Ergebnis setzt sich aus der zuvor eingereichten Selbsteinschätzung der Interessenten und deiner Einschätzung zusammen.',
    showAllFunctions: 'Future Skills',
    Select: {
      actionInitiativeSubtitle: ''
    }
  },
  Form: {
    age: {
      label: 'Alter',
      options: {
        option1: 'unter 35',
        option2: '35-50',
        option3: 'über 50'
      }
    },
    consultantEditTitle: 'Bitte erfrage oder schätze folgende Daten über die Interessent:in ein.',
    consultantSectionTitle: 'Interessentendaten',
    education: {
      label: 'Höchster Bildungsabschluss',
      options: {
        option1: 'Kein Schulabschluss',
        option2: 'Grund-/Hauptschulabschluss',
        option3: 'Realschule (Mittlerer Schulabschluss)',
        option4: 'Gymnasium (Abitur/Fachabitur)',
        option5: 'Abgeschlossene Berufsausbildung',
        option6: 'Fachhochschulabschluss',
        option7: 'Hochschule (Bachelor)',
        option8: 'Hochschule (Master/Diplom/Magister)',
        option9: 'Hochschule (Promotion)'
      }
    },
    employment: {
      label: 'Erwerbsstatus',
      options: {
        option1: 'Beschäftigt in Vollzeit (35h und mehr)',
        option2: 'Beschäftigt in Teilzeit',
        option3: 'Arbeitslos, auf der Suche nach Arbeit',
        option4: 'Arbeitslos, nicht auf der Suche nach Arbeit',
        option5: 'In Rente',
        option6: 'Arbeitsunfähig',
        option7: 'In Ausbildung',
        option8: 'Sonstiges'
      }
    },
    gender: {
      label: 'Geschlecht',
      options: {
        d: 'Divers/Keine Angabe',
        f: 'Weiblich',
        m: 'Männlich'
      }
    }
  },
  Navbar: {
    home: ' ',
    scoreboard: 'Start'
  },

  pdf: {
    meta: {
      title: 'Das KIRA-Profil'
    },
    report_p: {
      report_1: 'Nachdem  du  dich  in  die  Situationen  hineinversetzt  hast,  konnten  wir  anhand  der Antworten  dein  individuelles  KIRA-Profil  erstellen.  Im  Folgenden  geben  wir  dir Empfehlungen für deine persönliche Entwicklung in zehn Future Skills.',
      report_2: 'Future  Skills  helfen  dir  in  neuen  Situationen  Handeln  zu  können.  Sie  ergänzen  und unterstützen dadurch deine beruflichen Fähigkeiten. Für viele Jobs ist es wichtig, auch die Future Skills weiterzuentwickeln.',
      report_3: 'Im KIRA-Profil haben wir diese zehn Future Skills untersucht:',
      report_4: 'Im  Folgenden  erhältst  du  eine  Übersicht  bei  welchen  Future  Skills  dein  größtes Entwicklungspotenzial   liegt   und   in   welchen   beruflichen   Schwerpunkten   du   die jeweiligen  Future  Skills  brauchst.  Im  Beratungsgespräch  kannst  du  anhand  dieser Auswertung  gezielt  nach  Fortbildungen  fragen,  in  denen  auch  deine  Future  Skills entwickelt werden. '
    },
    report_title: {
      subtitle: 'für     ...'
    },
    fieldTitles: {
      fieldLow: 'WIR ÜBERLASSEN ES DIR',
      fieldMed: 'WIR RATEN AB',
      fieldAdv: 'WIR EMPFEHLEN',
      fieldHigh: 'WIR RATEN AB'
    }
  },
  Scoreboard: {
    selectDeveloper: 'Interessent:in auswählen'
  },
  Signup: {
    helpSection: {
      sectionOne: {
        title: 'Willkommen bei KIRA!',
        desc: 'Fachkräftemangel ist in aller Munde. Gleichzeitig gibt es eine hohe Zahl an Arbeitssuchenden oder Personen, die in ihrem aktuellen Job nicht zufrieden sind. Ist das alles eine Frage der Passung?<br>' +
          '<br>Für den idealen Match deiner beruflichen Weiterbildung und deinem zukünftigen Arbeitgeber haben wir ein Testverfahren entwickelt, um dein individuelles Potenzial zu messen.'
      },
      sectionTwo: {
        title: 'Wie funktioniert der Test?',
        desc: 'Bei KIRA versetzt zu dich anhand verschiedener Fragestellungen in unterschiedliche Situationen und schätzt deine Gefühlslage mittels einer Skala ein. Fühle dich bitte in jede der Situationen ein und antworte dann spontan. Es gibt keine richtigen oder falschen Antworten, es zählt nur deine persönliche Einschätzung.<br>' +
          '<br>' +
          'Der gesamte Test dauert ca. 15 Minuten. Im Anschluss an den Test kannst du dein persönliches Beratungsgespräch buchen.<br>' +
          '<br>' +
          'Mit deinem persönlichen Ergebnis bist du dann bestens vorbereitet auf deine weitere Bildungsreise!'
      }
    },
    privacyTerms: {
      start: 'Ich erteile meine Einwilligung zur Verarbeitung meiner Daten im Rahmen von KIRA sowie der Weitergabe meiner Daten an WBS-Berater:innen. Die Teilnahme an der Studie ist absolut freiwillig. Sie haben das Recht, Ihre datenschutzrechtliche Einwilligungserklärung jederzeit zu widerrufen. Mehr Informationen dazu finden Sie in unserer',
      link: 'Datenschutzerklärung'
    },
    serviceTerms: {
      start: 'Ich erkläre mich mit den',
      link: 'AGB',
      end: 'einverstanden'
    }
  },
  Step: {
    personalAdvice: 'Deine persönliche Beratung'
  },
  footerText: '<strong>Über WBS TRAINING.</strong> <a class="ct-custom-header-links" href="https://www.wbs-gruppe.de/story-spirit-sinn/" target="_blank">Unternehmen</a> | <a class="ct-custom-header-links" href="https://www.wbstraining.de/unternehmen/impressum/" target="_blank">Impressum</a> | <a class="ct-custom-header-links" href="https://www.wbstraining.de/allgemeine-geschaeftsbedingungen/" target="_blank">AGB</a> | <a class="ct-custom-header-links" href="/de/p/privacy-policy/" target="_blank">Datenschutz</a> | Key Visual © Delmaine Donson – iStock.com',
  Questionnaire: {
    consultantSurveyTitle: 'Bitte erfasse im Folgenden deinen Eindruck und deine Erkenntnisse aus dem Beratungsgespräch mit der Interessent:in.',
    finishAndSubmit: 'Einschätzung absenden',
    formComplete: 'Test abgeschlossen. Ergebnisse unter Tab Report.',
    questionText: '{question}',
    surveyTitle: 'Fühle dich bitte in die Situation ein. Dann antworte bitte spontan, welches Wort dein Gefühl zu dieser Situation am besten beschreibt. Es gibt keine richtigen oder falschen Antworten, es zählt nur deine persönliche Einschätzung.',
    noAnswer: 'Alternativsituation nutzen'
  },
  low: 'Unknown',
  medium: 'Low',
  advance: 'Learn',
  high: 'Good'
}
