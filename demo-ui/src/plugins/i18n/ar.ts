export default {
  Esco: {
    disclaimer: '_This service uses the ESCO classification of the European Commission.'
  },
  User: {
    email: 'البريد الإلكتروني',
    password: 'كلمة المرور',
    selector: 'كيف تقيم مهاراتك؟',
    errorPassword: 'كلمة المرور غير صحيحة',
    errorEmail: 'البريد الإلكتروني مسجل مسبقا'
  },
  Navbar: {
    initiatives: 'المبادرات',
    questions: 'الأسئلة',
    scoreboard: ' لوحة التقدم',
    dashboard: 'لوحة المعلومات',
    home: 'الصفحة الرئيسية',
    logout: 'تسجيل خروج',
    questionnaires: 'استبيان',
    fields: 'الخانات'
  },
  Home: {
    title: 'تم إعداد المراجعة التشخيصية لمساعدتك في تطوير أعمالك',
    personality: 'ﺑﺪء اﻟﻤﺮاﺟﻌﺔ اﻟﺘﺸﺨﻴﺼﻴﺔ',
    dashboard: 'مراحل المراجعة',
    diaganosis_survey: 'استبيان المراجعة التشخيصية',
    analysis: 'مراجعة التحليلات',
    actions: 'الخطة الإنتقالية',
    read_more: 'المزيد'
  },
  Signup: {
    signUp: 'اشتراك',
    confirmPassword: 'تأكيد كلمة المرور',
    helpSection: {
      sectionOne: {
        title: '',
        desc: ''
      },
      sectionTwo: {
        title: '',
        desc: ''
      }
    },
    legal: 'قانون',
    messageRegister: 'تم التسجيل بنجاح',
    privacyTerms: {
      start: 'أوافق على معالجة بياناتي. يمكنك العثور على مزيد من المعلومات في موقعنا',
      link: 'سياسة الخصوصية'
    },
    serviceTerms: {
      start: 'أنا أوافق على',
      link: 'الأحكام والشروط',
      end: ''
    },
    registerNew: 'تسجيل جديد',
    clear: 'تصفية النتائج'
  },
  Login: {
    login: 'تسجيل الدخول',
    messageLogin: 'تم تسجيل الدخول بنجاح'
  },
  Step: {
    consultantReport: '_Reports',
    warning: 'تنبيه',
    formIncomplete: 'لم تنته من هذا الاختبار حتى الآن ، إذا قمت بالخروج ، فسيتم فقدان جميع الإجابات السابقة.',
    noContent: 'المحتوى غير متاح',
    developerData: 'بيانات المطور العقاري',
    personalAdvice: '',
    transitionPlans: 'الخطة الإنتقالية',
    report: 'تقرير',
    submissionForm: '_Position to be filled'
  },
  Form: {
    edit: 'تعديل',
    nextStep: 'التالي',
    editTitle:
      'فيما يلي التفاصيل الأساسية حول مؤسستك، احرص على تحديث هذه المعلومات باستمرار أثناء تقدمك في هذه المبادرة.',
    sectionTitle: 'بيانات المطورالعقاري',
    sectionTitleEmployer: '_Company profile',
    companyName: 'اسم الشركة',
    consultantEditTitle:
      'فيما يلي التفاصيل الأساسية حول مؤسستك، احرص على تحديث هذه المعلومات باستمرار أثناء تقدمك في هذه المبادرة.',
    consultantSectionTitle: 'بيانات المطورالعقاري',
    citiesOperation: 'المدن التي عملت بها ',
    numberEmployees: 'عدد الموظفين',
    ongoingProjects: 'عدد المشاريع الحالية',
    totalProjects: 'عدد المشاريع الكلية',
    city: {
      Riyadh: 'الرياض',
      Makkah: 'مكة',
      Madinah: 'المدينة',
      Tabuk: 'تبوك',
      AlJawf: 'الجوف',
      Hail: 'حائل',
      AlQassim: 'القصيم',
      NorthernBordersProvince: 'الحدود الشمالية',
      EasternProvince: 'منطقة الشرقية',
      Najran: 'نجران',
      Jazan: 'جازان',
      Aseer: 'عسير',
      AlBahah: 'الباحة'
    }
  },
  Questionnaire: {
    alternativeQuestions: 'أسئلة بديلة',
    answerLabel: 'في هذه الحالة أشعر ...',
    consultantSurveyTitle:
      'اضغط على المفاتيح المقابلة للخيارات المرقمة ، مثل "1/2/3 / ادخل" على لوحة المفاتيح للإجابة على استبيان التشخيص التالي.',
    questionnaires: 'الإستبيانات',
    title: 'عنوان الإستبيان',
    type: {
      label: 'المخطط',
      default: 'عمودي',
      slider: 'شريط التمرير',
      sliderNavigation: 'شريط التمرير مع التنقل'
    },
    newQuestionnaire: 'إستبيان جديد',
    editQuestionnaire: 'تعديل الإستبيان',
    slug: 'شريحة',
    surveyTitle:
      'اضغط على المفاتيح المقابلة للخيارات المرقمة ، مثل "1/2/3 / ادخل" على لوحة المفاتيح للإجابة على استبيان التشخيص التالي.',
    formComplete: 'لقد أجبت مسبقًا على هذا الاستبيان ، تابع الاستبيان التالي.',
    finishForms: 'لقد انتهيت من جميع الاسئلة، وتم حفظ الإجابات.',
    finishAndSubmit: 'قم بالإنهاء والإرسال',
    nextForm: 'النموذج التالي',
    noAnswer: 'لم تتم الإجابة',
    question: ' :سؤال',
    questionText: ':سؤال {number} {question}',
    retakeSurvey: 'إعادة الاستبيان',
    retakeSurveySubmission: '_create a new job profile',
    emptyAnswer: 'لا يمكن إرسال الاستبيان فارغا'
  },
  ActionInitiative: {
    bookAppointment: '',
    combinedMissing: 'لا تتوفر نتيجة هذا الاختصاص إلا كتقييم ذاتي.',
    buyerCombinedDesc: '',
    consultantDesc: '',
    sectionTitle: 'خطة انتقالية',
    seeMore: 'المزيد',
    totalFunctionScore: 'مجموع نقاط الوظيفة',
    total: 'المجموع ',
    score: 'درجة',
    showAllFunctions: 'عرض جميع الوظائف',
    actionInitiative: 'الخطة الانتقالية: ',
    actionInitiativeTitle: 'عنوان الخطة الانتقالية',
    actionInitiativeSubtitle: 'قم بمراجعة الخطط الانتقالية التالية وحدد الخطط الحاسمة لمؤسستك.  ، و ضع علامة على الخطط التي ترغب في تحديد أولوياتها لنموك.',
    Select: {
      sectionTitle: 'التقرير',
      exportActionInitiatives: 'عرض الخطة الانتقالية',
      checkReport: 'التقرير',
      actionInitiativeSubtitle: 'فيما يلي الخطط الانتقالية الموصى بها . أثناء بدء كل مهمة وإكمالها، قم بتغيير الحالة إلى "قيد التقدم" أو "مكتمل"',
      score: 'الدرجة',
      status: 'الحالة',
      flag: 'علامة',
      initiativeState: 'حدد حالة الخطة'
    }
  },
  Questions: {
    questions: 'الأسئلة',
    editQuestion: 'تعديل السؤال',
    newQuestion: 'سؤال جديد',
    tag: 'التصنيف',
    questionCreated: 'تم إضافة السؤال',
    questionUpdated: 'تم تحديث السؤال',
    questionDeleted: 'تم حذف السؤال',
    position: 'ترتيب السؤال'
  },
  Initiatives: {
    initiatives: 'الخطط الانتقالية',
    newInitiative: 'خطة انتقالية جديدة',
    editInitiative: 'تعديل الخطة الانتقالية',
    initiativeTitle: 'العنوان:',
    initiativeDescription: 'الوصف:',
    initiativeTags: 'قائمة التصنيفات',
    initiativeCreated: 'تم إنشاء الخطة الانتقالية',
    initiativeUpdated: 'تم تحديث الخطة الانتقالية',
    initiativeDeleted: 'تم حذف الخطة الانتقالية'
  },
  Answers: {
    answers: 'الأجوبة:',
    answer: 'الإجابة',
    newAnswer: 'إجابة جديدة',
    editAnswer: 'تعديل الإجابة',
    weight: 'الدرجة',
    answerCreated: 'تم إنشاء الإجابة',
    answerUpdated: 'تم تحديث الإجابة',
    answerDeleted: 'تم حذف الإجابة'
  },
  Scoreboard: {
    toDate: 'الآن',
    thisWeek: 'الأسبوع الحالي',
    thisMonth: 'الشهر الحالي',
    selectDeveloper: 'حدد المطور',
    barchartTitle: 'تقدم المطورين في الخطط الانتقالية',
    barchartSubtitle: 'العنوان الفرعي',
    doughnutchartTitle: 'إجابات المطورين',
    doughnutchartSubtitle: 'العنوان الفرعي للمخطط البياني',
    statsTitle: 'نسبة التقدم بالخطط حسب الحالات',
    actionTitle: 'تتبع الخطة الانتقالية',
    selectFunction: 'حدد الوظيفة',
    initiativesStatus: 'حالة الخطة الانتقالية',
    initiativesEmpty: 'لا توجد خطط انتقالية',
    selectUser: 'حدد المطور',
    allDevelopers: 'جميع المطورين',
    takeSurvey: 'إجراء المراجعة التشخيصية كـ مطور '
  },
  Dashboard: {
    totalScore: 'مجموع النقاط حسب الوظيفة',
    topPerformance: 'الأعلى في الآداء',
    totalRespondents: 'إجمالي الإجابات ',
    totalRespondentsRange: 'إجمالي عدد الإجابات حسب نطاق الدرجات',
    totalRespondentsFunction: 'إجمالي عدد المستجيبين حسب نطاق النقاط لكل وظيفة',
    performanceTrend: 'اتجاه الأداء الوظيفي',
    rangeDate: 'نطاق التاريخ',
    scoreRange: 'نطاق الدرجات',
    selectFunction: 'حدد الوظيفة',
    allScoreRange: 'نطاق جميع الدرجات',
    allFunctions: 'جميع الوظائف',
    apply: 'تطبيق',
    listQuestionnaires: 'الإستبيانات'
  },
  Percentages: {
    low: '50٪',
    medium: '51-71٪',
    advance: '71-90٪',
    high: '91-100٪'
  },
  pdf: {
    header: {
      title: 'وزارة الشؤون البلدية والقروية والإسكان',
      subtitle: 'الخطة الانتقالية لدعم المطور العقاري',
      footer: '| 1443 شوال 28'
    },
    meta: {
      title: 'الخطة الانتقالية لدعم المطور العقاري'
    },
    footer: {
      page: 'صفحة: {0}'
    },
    field: {
      header: {
        efficiency: 'اﻟﻜﻔﺎءة',
        component: 'اﻟﻨﺸﺎط',
        supportive: 'المبادرة الداعمة'
      },
      title: {
        functions: 'الوظائف',
        result: 'اﻟﻨﺘﻴﺠﺔ:',
        percent: '{n}٪'
      }
    },
    general: {
      title: '1- معلومات عامة'
    },
    overall: {
      title: '2- نتيجة المراجعة التشخيصية الاجمالية',
      function: '{fn} الوظائف: {num}%'
    },
    fieldTitles: {
      fieldLow: '',
      fieldMed: '',
      fieldAdv: '',
      fieldHigh: ''
    },
    report_p: {
      report_1: 'انسجاما مع رؤية المملكة 2030 ، وكجزء من برامج وزارة الشؤون البلدية والقروية والإسكان في الارتقاء بقطاع التطوير العقاري ودعم المطورين العقاريين ، نقدم لكم تقرير الخطة الانتقالية لدعم المطور الخاص بكم .',
      report_2: 'تم تحديد مجموعة من المبادرات ضمن خطة انتقالية مصممة بهدف معالجة نقاط الضعف ومساعدة المطور في رفع مستوى الكفاءة في المحالات المرصودة كضعيفة أو متوسطة المستوى والتي ظهرت من خلال المراجعة التشخيصية التي تمت بمشاركتكم .',
      report_3: 'ينقسم التقرير إلى عدة اجزاء تشمل المعلومات العامة عن كيانكم كما عرفتموه من خلال المراجعة التشخيصية والنتيجة الإجمالية للمراجعة وتفصيلها على حسب مجموعات الوظائف انتقالا إلى استعراض النتائج المفصلة على الوظائف ومستويات الكفاءة لكل نشاط ، كما تم اقتراح مبادرات داعمة لتشكل خطة انتقالية للتفاعل معها و العمل بها للوصول للكفاءة والفعالية الاعلى',
      report_4: ''
    },
    report_title: {
      subtitle: ''
    }
  },
  footerText: 'وزارة الشؤون البلدية والقروية والاسكان | جميع الحقوق محفوظة 2022',
  ok: 'موافق',
  confirmDelete: 'هل انت متأكد؟ سيتم حذف العنصر ',
  inputRequired: 'هذه الخانة مطلوبه',
  next: 'أكمل',
  passwordLength: 'يجب أن تتكون كلمة المرور من 6 أحرف على الأقل.',
  search: 'بحث',
  cancel: 'إلغاء',
  save: 'حفظ',
  yes: 'نعم',
  no: 'لا',
  extraLow: 'منخفض جدًا',
  low: 'منخفض',
  medium: 'متوسط',
  advance: 'متقدم',
  high: 'مرتفع',
  all: 'الجميع',
  completed: 'مكتمل',
  inProgress: 'قيد التقدم',
  notStarted: 'لم تبدأ',
  error: 'حدث خطأ',
  userProfile: {
    createProfile: '_Save',
    createdProfile: '_User profile successfully updated',
    goalDesc: '_Tell us your goals',
    goals: {
      growCurrentJob: '_Grow in my current role',
      switchRole: '_Switch to a different role'
    },
    industry: '_Set your industry',
    mainGoal: '_Set your learning plan',
    openUserProfile: '_Open user profile',
    occupation: '_I am currently a',
    profile: '_Profile',
    skillsDesired: '_I am interested in learning these skills',
    skillsOwned: '_I have these skills',
    targetOccupation: '_I want to become a',
    userProfile: '_Personalize your experience'
  },
  wallet: {
    errorVerification: '_Error verifying credential',
    importVcs: '_Import from wallet',
    issuance: '_Issue wallet certificate',
    openUserProfile: '_Open user profile',
    qrCodeIssuance: '_Scan the Qr code to start the issuance',
    validCredential: '_Valid credential',
    verify: '_Verify wallet certificate'
  }
}
