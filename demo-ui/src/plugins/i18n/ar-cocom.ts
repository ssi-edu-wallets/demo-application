export default {
  Step: {
    developerData: 'بيانات المتدرب',
    transitionPlans: 'الخطة التطويرية',
    report: 'التقرير'
  },
  Form: {
    sectionTitle: 'بيانات المتدرب',
    companyName: 'اسم المتدرب'
  },
  Scoreboard: {
    selectDeveloper: 'حدد المتدرب',
    barchartTitle: 'تقدم المتدربين في الخطط الانتقالية',
    barchartSubtitle: '',
    doughnutchartSubtitle: '',
    doughnutchartTitle: 'إجابات المتدربين',
    statsTitle: 'نسبة التقدم بالخطط',
    selectUser: 'حدد المتدرب',
    allDevelopers: 'جميع المتدربين',
    takeSurvey: 'إجراء المراجعة التشخيصية كـ متدرب '
  },
  pdf: {
    header: {
      title: 'غرفة الرياض',
      subtitle: 'الخطة الانتقالية لدعم المتدرب'
    },
    meta: {
      title: 'الخطة الانتقالية لدعم المتدرب'
    },
    report_p: {
      report_1: 'انسجاما مع رؤية المملكة 2030 ، وكجزء من برامج وزارة الشؤون البلدية والقروية والإسكان في الارتقاء بقطاع التدريب ودعم المتدربين ، نقدم لكم تقرير الخطة الانتقالية لدعم المتدرب الخاص بكم .',
      report_2: 'تم تحديد مجموعة من المبادرات ضمن خطة انتقالية مصممة بهدف معالجة نقاط الضعف ومساعدة المتدرب في رفع مستوى الكفاءة في المحالات المرصودة كضعيفة أو متوسطة المستوى والتي ظهرت من خلال المراجعة التشخيصية التي تمت بمشاركتكم .'
    }
  },
  ActionInitiative: {
    actionInitiative: 'الخطة التطويرية: ',
    actionInitiativeTitle: 'عنوان الخطة التطويرية',
    showAllFunctions: 'عرض جميع المعايير',
    totalFunctionScore: 'مجموع  نقاطك في',
    Select: {
      sectionTitle: 'التقرير',
      exportActionInitiatives: 'عرض الخطة التطويرية',
      actionInitiativeSubtitle: 'فيما يلي الخطط التطويرية الموصى بها . أثناء بدء كل مهمة وإكمالها، قم بتغيير الحالة إلى "قيد التقدم" أو "مكتمل"',
      checkReport: 'التقرير',
      showAllFunctions: 'عرض جميع المعايير'

    }
  },
  Initiatives: {
    initiatives: 'الخطط التطويرية',
    editInitiative: 'تعديل الخطة التطويرية',
    initiativeCreated: 'تم إنشاء الخطة التطويرية',
    initiativeUpdated: 'تم تحديث الخطة التطويرية',
    initiativeDeleted: 'تم حذف الخطة التطويرية'
  },
  footerText: 'غرفة الرياض | جميع الحقوق محفوظة'
}
