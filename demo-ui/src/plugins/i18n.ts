import Vue from 'vue'
import VueI18n from 'vue-i18n'
import axios from 'axios'
import en from './i18n/en'
import ar from './i18n/ar'
import de from './i18n/de'
import setCustomLocale from './i18n/customLocales'

Vue.use(VueI18n)

const messages = {
  en: en,
  ar: ar,
  de: de
}

const i18n = new VueI18n({
  locale: 'en', // set locale
  fallbackLocale: 'en', // set fallback locale
  messages // set locale messages
})

const loadedLanguages = ['en', 'ar', 'de'] // our default language that is preloaded

function setI18nLanguage (lang: any) {
  i18n.locale = lang
  axios.defaults.headers.common['Accept-Language'] = lang
  document.querySelector('html')!.setAttribute('lang', lang)
  setCustomLocale('', i18n)
  return lang
}

export function loadLanguageAsync (lang: any) {
  // If the same language
  if (i18n.locale === lang) {
    return Promise.resolve(setI18nLanguage(lang))
  }

  // If the language was already loaded
  if (loadedLanguages.includes(lang)) {
    return Promise.resolve(setI18nLanguage(lang))
  }

  // If the language hasn't been loaded yet
  return import(`@/plugins/i18n/${lang ?? 'en'}.ts`).then(
    messages => {
      i18n.setLocaleMessage(i18n.locale, messages.default)
      loadedLanguages.push(i18n.locale)
      return setI18nLanguage(i18n.locale)
    }
  )
}

export default i18n
