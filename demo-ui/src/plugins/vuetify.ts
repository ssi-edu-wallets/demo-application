import Vue from 'vue'
import Vuetify from 'vuetify/lib/framework'
import setColorTheme from './themes'

Vue.use(Vuetify)

export default new Vuetify({
  rtl: false,
  theme: {
    options: {
      variations: false,
      customProperties: true
    },
    themes: {
      light: (() => {
        return setColorTheme('').theme
      })(),
      dark: (() => {
        return setColorTheme('').theme
      })()
    }
  }
})
