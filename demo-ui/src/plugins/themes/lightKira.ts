import CustomTheme from './CustomTheme'
import lightBase from './light'

const primary = '#a56743'
const secondary = '#112D4C'
const lightBlue = '#467EAE'

const theme: any = {
  primary,
  secondary,
  anchor: primary,
  adminBackground: '#e9f0f5',
  adminLinkBackground: lightBlue,
  adminListCard: '#071029',
  adminCard: secondary,
  adminBorderSelect: '#071029',
  adminActiveTextColor: '#e9f0f5',
  adminSubtitleText: '#c1805b',
  filterBtn: '#0A0A1D',
  activeTab: primary,
  activeColor: primary,
  background: '#e9f0f5',
  colorTab: secondary,
  cardTextColor: secondary,
  headerGradient: secondary,
  linkBackground: lightBlue,
  titleText: secondary,
  darkgreen: secondary,
  footerBackground: '#c8d8e7',
  footerTextColor: '#000000',
  low: '#EB904E',
  med: '#F2C93A',
  adv: '#52A675',
  high: '#4783C4'
}

for (const propt in lightBase) {
  if (!theme[propt]) {
    theme[propt] = lightBase[propt]
  }
}

const ret: CustomTheme = theme
export default ret
