import dark from './dark'
import light from './light'
import lightEduplex from './lightEduplex'
import lightKira from './lightKira'
import lightCocom from './lightCocom'
import CustomTheme from './CustomTheme'

interface ThemesIndex<T> {
  [name: string]: T
}

const themesIndex: ThemesIndex<CustomTheme> = {
  dark,
  lightEduplex,
  lightKira,
  lightCocom,
  light
}

const setColorTheme = (themeName: string = '', $vuetify: any = null) => {
  const key = 'defaultThemeName'
  let name: string = themeName
  if (name) {
    window.localStorage.setItem(key, name)
  } else {
    name = window.localStorage.getItem(key) + ''
  }
  const isDark: boolean = name.substring(0, 4) === 'dark'
  let theme
  if (isDark) {
    theme = themesIndex[name] || themesIndex.dark
    if ($vuetify) {
      $vuetify.theme.themes.dark = theme
    }
  } else {
    theme = themesIndex[name] || themesIndex.light
    if ($vuetify) {
      $vuetify.theme.themes.light = theme
    }
  }
  if ($vuetify) {
    $vuetify.theme.dark = theme.isDark
  }
  return {
    theme,
    isDark
  }
}

export default setColorTheme
