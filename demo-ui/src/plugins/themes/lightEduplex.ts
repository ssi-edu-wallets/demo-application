import CustomTheme from './CustomTheme'
import lightBase from './light'

const primary = '#24aae0'
const secondary = '#002751'

const theme: any = {
  primary,
  secondary,
  anchor: primary,
  adminBackground: '#e9f0f5',
  adminLinkBackground: primary,
  adminListCard: '#071029',
  adminCard: secondary,
  adminBorderSelect: '#071029',
  adminActiveTextColor: '#e9f0f5',
  adminSubtitleText: '#74c3e1',
  filterBtn: '#0A0A1D',
  activeTab: primary,
  activeColor: primary,
  background: '#e9f0f5',
  colorTab: secondary,
  cardTextColor: secondary,
  headerGradient: secondary,
  linkBackground: primary,
  titleText: secondary,
  darkgreen: secondary,
  footerBackground: secondary,
  footerTextColor: '#FFFFFF',
  'lord-icon-primary': secondary,
  'lord-icon-secondary': primary
}

for (const propt in lightBase) {
  if (!theme[propt]) {
    theme[propt] = lightBase[propt]
  }
}

const ret: CustomTheme = theme
export default ret
