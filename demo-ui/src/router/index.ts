import Vue from 'vue'
import VueRouter from 'vue-router'

import i18n, { loadLanguageAsync } from '@/plugins/i18n'
import auth from '@/middleware/auth'
import { defaultLocale, locales } from '@/plugins/i18n/i18n'
import { SUB_PATH } from '@/services/constants'
import Login from '@/views/Login.vue'
import SignUpForm from '@/views/SignupForm.vue'
import PageNotFound from '@/views/PageNotFound.vue'
import authAdmin from '@/middleware/authAdmin'
import authConsultant from '../middleware/authConsultant'
import store from '@/store'
Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      path: '/',
      redirect: `/${defaultLocale}`
    },
    {
      path: '/:locale',
      component: () => import('../views/AppWrapper.vue'),
      beforeEnter (to, from, next) {
        const lang = to.params.locale
        if (locales.some((e) => e.code === lang)) {
          if (i18n.locale !== lang) {
            i18n.locale = lang
          }
          return next()
        }
        return next(defaultLocale)
      },
      children: [
        {
          path: '',
          name: 'home',
          component: () => import('../views/Home.vue')
        },
        {
          path: 'issuance',
          component: () => import('../views/Issuance.vue')
        },
        {
          path: 'verify/success',
          component: () => import('../views/Verify.vue'),
          meta: {
            middleware: auth
          }
        },
        {
          path: 'account',
          name: 'account',
          component: () => import('../views/UnderConstruction.vue'),
          meta: {
            middleware: auth
          }
        },
        {
          path: 'about',
          name: 'about',
          component: () => import('../views/UnderConstruction.vue')
        },
        {
          path: 'edit',
          name: 'edit',
          component: () => import('../views/UnderConstruction.vue'),
          meta: {
            middleware: auth
          },
          children: [
            {
              path: ':slug',
              component: () => import('../views/UnderConstruction.vue'),
            }
          ]
        },
        {
          path: 'signup',
          name: 'signup',
          component: SignUpForm
        },
        {
          path: 'login',
          name: 'login',
          component: Login,
          beforeEnter (to, from, next) {
            if (process.env.NODE_ENV === 'production') {
              window.location.href = `/${i18n.locale}/login`
              return next(false)
            }

            return next()
          }
        },
        {
          path: 'questions',
          name: 'questions',
          component: () => import('../views/UnderConstruction.vue'),
          meta: {
            middleware: authAdmin
          }
        },
        {
          path: 'answers/:id',
          name: 'answers',
          component: () => import('../views/UnderConstruction.vue'),
          meta: {
            middleware: authAdmin
          }
        },
        {
          path: 'initiatives',
          name: 'initiatives',
          component: () => import('../views/UnderConstruction.vue'),
          meta: {
            middleware: authAdmin
          }
        },
        {
          path: 'questionnaire',
          name: 'questionnaire',
          component: () => import('../views/UnderConstruction.vue'),
          meta: {
            middleware: authAdmin
          }
        },
        {
          path: 'scoreboard',
          name: 'scoreboard',
          component: () => import('../views/UnderConstruction.vue'),
          meta: {
            middleware: authAdmin,
            name: 'scoreboard'
          },
          children: [
            {
              path: ':slug',
              component: () => import('../views/UnderConstruction.vue'),
              meta: {
                name: 'scoreboard'
              }
            }
          ]
        },
        {
          path: 'consultant-list',
          name: 'consultant-list',
          component: () => import('../views/UnderConstruction.vue'),
          meta: {
            middleware: authConsultant
          }
        },
        {
          path: 'dashboard',
          name: 'dashboard',
          component: () => import('../views/UnderConstruction.vue'),
          meta: {
            middleware: authAdmin
          }
        },
        {
          path: '404',
          name: 'notFound',
          component: PageNotFound
        }
      ]
    },
    {
      path: '*',
      component: PageNotFound
    }
  ],
  base: '/' + SUB_PATH + '/',
  mode: 'history'
})

// Creates a `nextMiddleware()` function which not only
// runs the default `next()` callback but also triggers
// the subsequent Middleware function.
function nextFactory (context: any, middleware: any, index: any) {
  const subsequentMiddleware = middleware[index]
  // If no subsequent Middleware exists,
  // the default `next()` callback is returned.
  if (!subsequentMiddleware) return context.next

  return (...parameters: any) => {
    // Run the default Vue Router `next()` callback first.
    context.next(...parameters)
    // Then run the subsequent Middleware with a new
    // `nextMiddleware()` callback.
    const nextMiddleware = nextFactory(context, middleware, index + 1)
    subsequentMiddleware({ ...context, next: nextMiddleware })
  }
}

router.beforeEach(async (to: any, from: any, next: any) => {
  const lang = to.params.locale
  const isValidLocale: boolean = locales.some((locale) => {
    return locale.code === lang
  })
  if (!isValidLocale) {
    let path: string = '/'
    if (to.name) {
      path = to.fullPath
    }
    return next(defaultLocale + path)
  }
  store.dispatch('config/registerLocaleData', to.params.locale).then(r => {})
  store.dispatch('config/registerToPath', to.name).then(r => {})
  await loadLanguageAsync(lang)

  if (to.meta.middleware) {
    const middleware = Array.isArray(to.meta.middleware)
      ? to.meta.middleware
      : [to.meta.middleware]

    const context = {
      from,
      next,
      router,
      to
    }
    const nextMiddleware = nextFactory(context, middleware, 1)

    return middleware[0]({ ...context, next: nextMiddleware })
  }

  next()
})

export default router
