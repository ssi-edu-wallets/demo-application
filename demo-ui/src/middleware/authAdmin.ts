import store from '@/store'
import { GROUP_ADMIN, GROUP_SELLER } from '@/services/constants'
import { User } from '../store/modules/user/types'

export default async function authAdmin ({ next, router }: any) {
  let user: User = store.getters['user/getUser']
  if (!user || user.token.access_token === '') {
    await store.dispatch('user/loadUserToken')
    user = store.getters['user/getUser']
  }

  if (!user || user.token.access_token === '') {
    router.replace({ name: 'login', params: { locale: store.getters['config/getLocale'] } }).catch(() => {})
  } else if (user.group_id !== GROUP_ADMIN && user.group_id !== GROUP_SELLER) {
    if (store.getters['config/getToPath'] !== 'home') {
      router.replace({ name: 'home', params: { locale: store.getters['config/getLocale'] } }).catch(() => {})
    }
  } else if (user.group_id === GROUP_SELLER) {
    if (store.getters['config/getToPath'] === 'initiatives' || store.getters['config/getToPath'] === 'questions' || store.getters['config/getToPath'] === 'answers') {
      router.replace({ name: 'home', params: { locale: store.getters['config/getLocale'] } }).catch(() => {})
    }
  }

  return next()
}
