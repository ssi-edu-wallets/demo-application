import store from '@/store'
import { GROUP_ADMIN, GROUP_SELLER } from '@/services/constants'
import { User } from '../store/modules/user/types'

export default async function auth ({ next, router }: any) {
  let user: User = store.getters['user/getUser']
  if (!user || user.token.access_token === '') {
    await store.dispatch('user/loadUserToken')
    user = store.getters['user/getUser']
  }

  if (user.group_id === GROUP_ADMIN || user.group_id === GROUP_SELLER) {
    return router.replace({ name: 'scoreboard', params: { locale: store.getters['config/getLocale'] } }).catch(() => true)
  }

  if (user && user.token.access_token) {
    return next()
  } else {
    if (router.currentRoute.name !== 'login') {
      router.replace({ name: 'login', params: { locale: store.getters['config/getLocale'] } }).catch(() => true)
    }
  }
}
