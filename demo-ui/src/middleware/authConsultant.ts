import store from '../store'
import { User } from '../store/modules/user/types'
import { GRANT_LEVEL_SUPERVISOR, GROUP_TRAINER } from '../services/constants'

export default async function authConsultant ({ next, router }: any) {
  let user: User = store.getters['user/getUser']
  if (!user || user.token.access_token === '') {
    await store.dispatch('user/loadUserToken')
    user = store.getters['user/getUser']
  }

  if (!user || user.token.access_token === '') {
    router.replace({ name: 'login', params: { locale: store.getters['config/getLocale'] } })
  }
  const isConsultant = user.group_id === GROUP_TRAINER && user.grant_level === GRANT_LEVEL_SUPERVISOR
  if (!isConsultant) {
    router.replace({ name: 'home', params: { locale: store.getters['config/getLocale'] } })
  }

  return next()
}
