const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  devServer: {
    proxy: {
      '^/devProxyBackend': {
        target: process.env.VUE_APP_TARGET_PROXY,
        changeOrigin: true,
        pathRewrite: {
          '^/devProxyBackend': '/'
        }
      }
    }
  },
  publicPath: process.env.NODE_ENV === 'production'
    ? 'https://d3tllfpwq6x5mq.cloudfront.net/fe/' + process.env.DATE_IMG_TAG + '/_d/'
    : '/',
  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.pug$/,
          loader: 'pug-plain-loader'
        }
      ]
    }
  },
  transpileDependencies: [
    'vuetify'
  ]
})
