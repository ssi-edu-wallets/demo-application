# Public Relations/ Networking

We introduced parts of our research to the public at the LearnTec Congress in June 2023 in Karlsruhe for the first time (speaker slot). Our future goal is to spread the technology approach throughout Europe and to establish close links with government agencies - a process we have already begun.
