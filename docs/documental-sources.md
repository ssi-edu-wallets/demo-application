# Documental sources

Cohen, G. and Steele, O. (2023) _Verifiable credentials JSON schema specification_, _W3C_. Available at: [https://www.w3.org/TR/vc-json-schema/](https://www.w3.org/TR/vc-json-schema/) (Accessed: 09 August 2023)

Electronic Identification (2022) _Electronic identification_, _Electronic IDentification - IDentity Verification Solutions_. Available at: [https://www.electronicid.eu/en/blog/post/eidas-2-0-what-can-companies-expect-from-it/en ](https://www.electronicid.eu/en/blog/post/eidas-2-0-what-can-companies-expect-from-it/en)(Accessed: 02 August 2023).

ESCO _About Esco_, _ESCO_. Edited by the European Commission. Available at: [https://esco.ec.europa.eu/en/about-esco](https://esco.ec.europa.eu/en/about-esco) (Accessed: 08 August 2023).

Europass Learning Model _Upcoming launch of the European Learning Model V3: Europass_, _Upcoming launch of the European Learning Model v3 | Europass_. Edited by the European Commission. Available at:[ https://europa.eu/europass/tr/news/upcoming-launch-european-learning-model-v3 ](https://europa.eu/europass/tr/news/upcoming-launch-european-learning-model-v3)(Accessed: 08 August 2023).

European Commission(10/06/2021) , _EBSI Architecture, explained._ Available at: [https://www.google.com/url?sa=t\&rct=j\&q=\&esrc=s\&source=web\&cd=\&cad=rja\&uact=8\&ved=2ahUKEwiWtJqk1OKAAxXZUqQEHf6cBVIQFnoECB4QAQ\&url=https%3A%2F%2Fec.europa.eu%2Fdigital-building-blocks%2Fwikis%2Fdownload%2Fattachments%2F447687044%2F%2528210610%2529%2528EBSI\_Architecture\_Explained%2529%2528v1.02%2529.pdf%3Fapi%3Dv2\&usg=AOvVaw2MiBXC8nid8v-sO-TU2Jyn\&opi=89978449](https://www.google.com/url?sa=t\&rct=j\&q=\&esrc=s\&source=web\&cd=\&cad=rja\&uact=8\&ved=2ahUKEwiWtJqk1OKAAxXZUqQEHf6cBVIQFnoECB4QAQ\&url=https%3A%2F%2Fec.europa.eu%2Fdigital-building-blocks%2Fwikis%2Fdownload%2Fattachments%2F447687044%2F%2528210610%2529%2528EBSI\_Architecture\_Explained%2529%2528v1.02%2529.pdf%3Fapi%3Dv2\&usg=AOvVaw2MiBXC8nid8v-sO-TU2Jyn\&opi=89978449)

European Commission (2020) ‘Publishing of Qualification and Learning Opportunity Data Documentation’.

European commission (2022) _Official Legal Text_, _General Data Protection Regulation (GDPR)_. Available at: [https://gdpr-info.eu/](https://gdpr-info.eu/) (Accessed: 12 August 2023).

European Commission, _Europass Digital Credentials Infrastructure (EDCI),_ Available at: [https://ec.europa.eu/futurium/en/system/files/ged/edci\_presentation.pdf](https://ec.europa.eu/futurium/en/system/files/ged/edci\_presentation.pdf).

European Commission _EBSI verifiable credentials,_ EBSI Available at: [https://ec.europa.eu/digital-building-blocks/wikis/download/attachments/600343491/Chapter%203%20-%20EBSI%20DIDs.pdf?api=v2](https://ec.europa.eu/digital-building-blocks/wikis/download/attachments/600343491/Chapter%203%20-%20EBSI%20DIDs.pdf?api=v2)

European Commission _European Blockchain Services Infrastructure_, _Home - EBSI -_. Available at: [https://ec.europa.eu/digital-building-blocks/wikis/display/EBSI/Home](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSI/Home) (Accessed: 09 August 2023).

European Commission _Introduction to the european learning model_ , _European Learning Model for Stakeholders | Europass_. Available at: [https://europa.eu/europass/en/node/2128](https://europa.eu/europass/en/node/2128) (Accessed: 09 August 2023).

European Commission _Verifiable diploma schema_, _Verifiable Diploma Schema - EBSI Specifications -_. Available at: [https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Verifiable+Diploma+Schema](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Verifiable+Diploma+Schema) (Accessed: 09 August 2023).

European Commission (no date b) _Introduction to the european learning model (ELM)_, _ELM Browser_. Available at:[ https://europa.eu/europass/elm-browser/index.html#introduction-to-the-european-learning-model-elm](https://europa.eu/europass/elm-browser/index.html#introduction-to-the-european-learning-model-elm) (Accessed: 09 August 2023).

European Commission (no date c) _The European Digital Identity Wallet Architecture and Reference Framework_, _Shaping Europe’s digital future_. Available at: [https://digital-strategy.ec.europa.eu/en/library/european-digital-identity-wallet-architecture-and-reference-framework](https://digital-strategy.ec.europa.eu/en/library/european-digital-identity-wallet-architecture-and-reference-framework) (Accessed: 08 August 2023).

European Commission (no date c) _Verifiable attestation_, _Verifiable Attestation - EBSI Specifications -_. Available at: [https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Verifiable+Attesttation](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Verifiable+Attesttation) (Accessed: 09 August 2023).

European Commission (no date d) _Verifiable attestation for ID_, _Verifiable Attestation for ID - EBSI Specifications -_. Available at:[ https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Verifiable+Attestation+for+ID](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Verifiable+Attestation+for+ID) (Accessed: 12 August 2023).

European Commission (no date d) _What is EBSI_, _What is EBSI - EBSI -_. Available at: [https://ec.europa.eu/digital-building-blocks/wikis/display/EBSI/What+is+EBSI](./) (Accessed: 10 August 2023).

European Learning Model v3 _european-commission-empl/European-Learning-Model_ Edited by European Commission. Available at:[ https://github.com/european-commission-empl/European-Learning-Model](https://github.com/european-commission-empl/European-Learning-Model)

Kristina Yasuda, Dr. Torsten Lodderstedt ,_OpenID Connect for SSI,_ Available at: [https://openid.net/wordpress-content/uploads/2021/09/OIDF\_OIDC4SSI-Update\_Kristina-Yasuda-Torsten-Lodderstedt.pdf](https://openid.net/wordpress-content/uploads/2021/09/OIDF\_OIDC4SSI-Update\_Kristina-Yasuda-Torsten-Lodderstedt.pdf).

O. Terbu, T. Lodderstedt,K. Yasuda, T. Looker (21/04/2023), _OpenID for Verifiable Presentations - draft 18._ Available at: [https://openid.net/specs/openid-4-verifiable-presentations-1\_0.html](https://openid.net/specs/openid-4-verifiable-presentations-1\_0.html).

Oauth _OAuth 2.0_, _OAuth_. Available at: [https://oauth.net/2/ ](https://oauth.net/2/)(Accessed: 12 August 2023).

Pastor Matut, C. and Du Seuil, D. _Understanding the European self-sovereign identity framework (ESSIF)_, _PPT_. Available at: [https://www.slideshare.net/SSIMeetup/understanding-the-european-selfsovereign-identity-framework-essif ](https://www.slideshare.net/SSIMeetup/understanding-the-european-selfsovereign-identity-framework-essif)(Accessed: 08 August 2023).

Sporny , M., Longley , D. and Chadwick , D. _Verifiable credentials data model V1.1_, _W3C_. Available at: [https://www.w3.org/TR/vc-data-model/#abstract](https://www.w3.org/TR/vc-data-model/#abstract) (Accessed: 08 August 2023).

Sporny, M. _et al._ _Decentralized identifiers (DIDs) v1.0_, _W3C_. Available at: [https://www.w3.org/TR/did-core/](https://www.w3.org/TR/did-core/) (Accessed: 08 August 2023).

Swagger _API development for everyone_, _Swagger_. Available at: [https://swagger.io/ ](https://swagger.io/)(Accessed: 12 August 2023).

T. Lodderstedt,K. Yasuda, T. Looker (03/02/2023), _OpenID for Verifiable Credential Issuance._ Available at: [https://openid.net/specs/openid-4-verifiable-credential-issuance-1\_0.html#name-introduction](https://openid.net/specs/openid-4-verifiable-credential-issuance-1\_0.html#name-introduction).

ValidatedId _Validated ID - electronic signature and digital identity providers_, _Validated ID - Electronic Signature and Digital Identity Providers_. Available at: [https://www.validatedid.com/en ](https://www.validatedid.com/en)(Accessed: 08 August 2023).

Walt.id ,_Walt.id Wallet Kit_, _walt.id_. Available at:[ https://github.com/walt-id/waltid-walletkit ](https://github.com/walt-id/waltid-walletkit)(Accessed: 08 August 2023).

Walt.id ,_Walt.id Web wallet_, _walt.id_. Available at:[ https://github.com/walt-id/waltid-web-wallet ](https://github.com/walt-id/waltid-web-wallet)(Accessed: 08 August 2023).

1. European Commission_._ _Different data paradigms along the web generations_, _EBSI_. Available at (Page 3): [https://ec.europa.eu/digital-building-blocks/wikis/download/attachments/597952490/Chapter%200%20-%20Verifiable%20Credentials%20An%20introduction.pdf?version=1\&modificationDate=1676459051355\&api=v2](https://ec.europa.eu/digital-building-blocks/wikis/download/attachments/597952490/Chapter%200%20-%20Verifiable%20Credentials%20An%20introduction.pdf?version=1\&modificationDate=1676459051355\&api=v2) ↑
2. European Commission _The European Digital Identity Wallet Architecture and Reference Framework_, _Shaping Europe’s digital future_. Available at: [https://digital-strategy.ec.europa.eu/en/library/european-digital-identity-wallet-architecture-and-reference-framework](https://digital-strategy.ec.europa.eu/en/library/european-digital-identity-wallet-architecture-and-reference-framework) (Accessed: 08 August 2023). ↑
3. Sporny, M. _et al._ _Decentralized identifiers (DIDs) v1.0_, _W3C_. Available at: [https://www.w3.org/TR/did-core/](https://www.w3.org/TR/did-core/) (Accessed: 08 August 2023). ↑
4. Sporny , M., Longley , D. and Chadwick , D. _Verifiable credentials data model V1.1_, _W3C_. Available at: [https://www.w3.org/TR/vc-data-model/#abstract](https://www.w3.org/TR/vc-data-model/#abstract) (Accessed: 08 August 2023). ↑
5. Pastor Matut, C. and Du Seuil, D. _Understanding the European self-sovereign identity framework (ESSIF)_, _PPT_. Available at: [https://www.slideshare.net/SSIMeetup/understanding-the-european-selfsovereign-identity-framework-essif ](https://www.slideshare.net/SSIMeetup/understanding-the-european-selfsovereign-identity-framework-essif)(Accessed: 08 August 2023). ↑
6. European Commission (2020) ‘Publishing of Qualification and Learning Opportunity Data Documentation’. ↑
7. ESCO _About Esco_, _ESCO_. Edited by European Commission. Available at: [https://esco.ec.europa.eu/en/about-esco](https://esco.ec.europa.eu/en/about-esco) (Accessed: 08 August 2023). ↑
8. European Commission _European Blockchain Services Infrastructure_, _Home - EBSI -_. Available at: [https://ec.europa.eu/digital-building-blocks/wikis/display/EBSI/Home](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSI/Home) (Accessed: 09 August 2023). ↑
9. European commission (2022) _Official Legal Text_, _General Data Protection Regulation (GDPR)_. Available at: [https://gdpr-info.eu/ ](https://gdpr-info.eu/)(Accessed: 12 August 2023). ↑
10. Electronic Identification (2022) _Electronic identification_, _Electronic IDentification - IDentity Verification Solutions_. Available at:[ https://www.electronicid.eu/en/blog/post/eidas-2-0-what-can-companies-expect-from-it/en](https://www.electronicid.eu/en/blog/post/eidas-2-0-what-can-companies-expect-from-it/en) (Accessed: 02 August 2023). ↑
11. Sporny, M. _et al._ _Decentralized identifiers (DIDs) v1.0_, _W3C_. Available at: [https://www.w3.org/TR/did-core/](https://www.w3.org/TR/did-core/) (Accessed: 08 August 2023). ↑
12. Decentralized Identifier parts ↑
13. European Commission _EBSI verifiable credentials,_ EBSI Available at: [https://ec.europa.eu/digital-building-blocks/wikis/download/attachments/600343491/Chapter%203%20-%20EBSI%20DIDs.pdf?api=v2](https://ec.europa.eu/digital-building-blocks/wikis/download/attachments/600343491/Chapter%203%20-%20EBSI%20DIDs.pdf?api=v2) ↑
14. Cohen, G. and Steele, O. (2023) _Verifiable credentials JSON schema specification_, _W3C_. Available at:[ https://www.w3.org/TR/vc-json-schema/](https://www.w3.org/TR/vc-json-schema/) (Accessed: 09 August 2023) ↑
15. European Commission _Verifiable diploma schema_, _Verifiable Diploma Schema - EBSI Specifications -_. Available at:[ https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Verifiable+Diploma+Schema](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Verifiable+Diploma+Schema) (Accessed: 09 August 2023). ↑
16. European Commission (no date d) _What is EBSI_, _What is EBSI - EBSI -_. Available at: [https://ec.europa.eu/digital-building-blocks/wikis/display/EBSI/What+is+EBSI](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSI/What+is+EBSI) (Accessed: 10 August 2023). ↑
17. European Commission (no date d) _What is EBSI_, _What is EBSI - EBSI -_. Available at: [https://ec.europa.eu/digital-building-blocks/wikis/display/EBSI/What+is+EBSI](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSI/What+is+EBSI) (Accessed: 10 August 2023). ↑
18. European Commission (no date b) _Introduction to the european learning model (ELM)_, _ELM Browser_. Available at:[ https://europa.eu/europass/elm-browser/index.html#introduction-to-the-european-learning-model-elm](https://europa.eu/europass/elm-browser/index.html#introduction-to-the-european-learning-model-elm) (Accessed: 09 August 2023). ↑
19. European Learning Model v3 _european-commission-empl/European-Learning-Model_ Edited by European Commission. Available at:[ https://github.com/european-commission-empl/European-Learning-Model](https://github.com/european-commission-empl/European-Learning-Model) ↑
20. European Commission _Introduction to the european learning model_ , _European Learning Model for Stakeholders | Europass_. Available at:[ https://europa.eu/europass/en/node/2128 ](https://europa.eu/europass/en/node/2128)(Accessed: 09 August 2023). ↑
21. European Commission, _Europass Digital Credentials Infrastructure (EDCI),_ Available at: [https://ec.europa.eu/futurium/en/system/files/ged/edci\_presentation.pdf](https://ec.europa.eu/futurium/en/system/files/ged/edci\_presentation.pdf). ↑
22. Europass Learning Model _Upcoming launch of the European Learning Model V3: Europass_, _Upcoming launch of the European Learning Model v3 | Europass_. Edited by the European Commission. Available at: [https://europa.eu/europass/tr/news/upcoming-launch-european-learning-model-v3](https://europa.eu/europass/tr/news/upcoming-launch-european-learning-model-v3) (Accessed: 08 August 2023). ↑
23. European Commission (2020) ‘Publishing of Qualification and Learning Opportunity Data Documentation’. ↑
24. ESCO _About Esco_, _ESCO_. Edited by the European Commission. Available at: [https://esco.ec.europa.eu/en/about-esco](https://esco.ec.europa.eu/en/about-esco) (Accessed: 08 August 2023). ↑
25. ValidatedId _Validated ID - electronic signature and digital identity providers_, _Validated ID - Electronic Signature and Digital Identity Providers_. Available at: [https://www.validatedid.com/en](https://www.validatedid.com/en) (Accessed: 08 August 2023). ↑
26. Walt.id, _Walt.id Wallet Kit_, _walt.id_. Available at:[ https://github.com/walt-id/waltid-walletkit](https://github.com/walt-id/waltid-walletkit) (Accessed: 08 August 2023). ↑
27. Kristina Yasuda, Dr. Torsten Lodderstedt ,_OpenID Connect for SSI,_ Available at: [https://openid.net/wordpress-content/uploads/2021/09/OIDF\_OIDC4SSI-Update\_Kristina-Yasuda-Torsten-Lodderstedt.pdf](https://openid.net/wordpress-content/uploads/2021/09/OIDF\_OIDC4SSI-Update\_Kristina-Yasuda-Torsten-Lodderstedt.pdf) ↑
28. Issuers trust model EBSI, Edited by the European Commission. Available at: [https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Issuers+trust+model+-+Accreditation+of+Issuers](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Issuers+trust+model+-+Accreditation+of+Issuers) (Accessed: 08 August 2023) ↑
29. European Commission (no date c) _The European Digital Identity Wallet Architecture and Reference Framework_, _Shaping Europe’s digital future_. Available at: [https://digital-strategy.ec.europa.eu/en/library/european-digital-identity-wallet-architecture-and-reference-framework](https://digital-strategy.ec.europa.eu/en/library/european-digital-identity-wallet-architecture-and-reference-framework) (Accessed: 08 August 2023). ↑
30. European Commission(10/06/2021) , _EBSI Architecture, explained._ Available at: [https://www.google.com/url?sa=t\&rct=j\&q=\&esrc=s\&source=web\&cd=\&cad=rja\&uact=8\&ved=2ahUKEwiWtJqk1OKAAxXZUqQEHf6cBVIQFnoECB4QAQ\&url=https%3A%2F%2Fec.europa.eu%2Fdigital-building-blocks%2Fwikis%2Fdownload%2Fattachments%2F447687044%2F%2528210610%2529%2528EBSI\_Architecture\_Explained%2529%2528v1.02%2529.pdf%3Fapi%3Dv2\&usg=AOvVaw2MiBXC8nid8v-sO-TU2Jyn\&opi=89978449](https://www.google.com/url?sa=t\&rct=j\&q=\&esrc=s\&source=web\&cd=\&cad=rja\&uact=8\&ved=2ahUKEwiWtJqk1OKAAxXZUqQEHf6cBVIQFnoECB4QAQ\&url=https%3A%2F%2Fec.europa.eu%2Fdigital-building-blocks%2Fwikis%2Fdownload%2Fattachments%2F447687044%2F%2528210610%2529%2528EBSI\_Architecture\_Explained%2529%2528v1.02%2529.pdf%3Fapi%3Dv2\&usg=AOvVaw2MiBXC8nid8v-sO-TU2Jyn\&opi=89978449) ↑
31. T. Lodderstedt,K. Yasuda, T. Looker (03/02/2023), _OpenID for Verifiable Credential Issuance._ Available at: [https://openid.net/specs/openid-4-verifiable-credential-issuance-1\_0.html#name-introduction](https://openid.net/specs/openid-4-verifiable-credential-issuance-1\_0.html#name-introduction) ↑
32. O. Terbu, T. Lodderstedt,K. Yasuda, T. Looker (21/04/2023), _OpenID for Verifiable Presentations - draft 18._ Available at:[ https://openid.net/specs/openid-4-verifiable-presentations-1\_0.html](https://openid.net/specs/openid-4-verifiable-presentations-1\_0.html) ↑
33. European Commission (no date d) _Verifiable attestation for ID_, _Verifiable Attestation for ID - EBSI Specifications -_. Available at:[ https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Verifiable+Attestation+for+ID](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Verifiable+Attestation+for+ID) (Accessed: 12 August 2023). ↑
34. European Commission (no date c) _Verifiable attestation_, _Verifiable Attestation - EBSI Specifications -_. Available at: [https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Verifiable+Attesttation](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Verifiable+Attesttation) (Accessed: 09 August 2023). ↑
35. Walt.id ,_Walt.id Web wallet_, _walt.id_. Available at: [https://github.com/walt-id/waltid-web-wallet](https://github.com/walt-id/waltid-web-wallet) (Accessed: 08 August 2023). ↑
36. Oauth _OAuth 2.0_, _OAuth_. Available at:[ https://oauth.net/2/](https://oauth.net/2/) (Accessed: 12 August 2023). ↑
37. Swagger _API development for everyone_, _Swagger_. Available at:[ https://swagger.io/](https://swagger.io/) (Accessed: 12 August 2023). ↑
38. European Commission (2020) ‘Publishing of Qualification and Learning Opportunity Data Documentation’. ↑
