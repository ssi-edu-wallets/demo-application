# Course of the work packages

## Work package 1 - _\<Project start>_ <a href="#_heading-h.2s8eyo1" id="_heading-h.2s8eyo1"></a>

* **Start of the project.**
* **Assessment of the project** to begin planning & define the tasks to be carried out.
* **Development of a website to inform about the project.**
* **Creation of a blog** to summarize the creation process/objectives and the phases of the project.

### Project assessment <a href="#_heading-h.2biict30d1jv" id="_heading-h.2biict30d1jv"></a>

In this first working package it has been done the **project assessment** where the details and specification of the project were set in order to **plan the tasks** that will be done and the **timeline** during the development of it. Also some implementation **decisions were made,** in this implementation we choose to use of **ESSIF/EBSI infrastructure** and the verifiable credentials standards **instead of the EDCI** infrastructure because EDCI is intended **only for educational purposes** that means that the system is not flexible and interoperable with other type of verifiable credentials. Instead the ESSIF/EBSI make use of decentralized blockchain services which provide a **high availability,security and interoperability**, also within this infrastructure a user with a compliant wallet can handle multiple types of verifiable credentials that can be presented to any third party or verifier. We choose the VCs and EBSI because we believe that the real purpose of the EUDI wallets is to **unify** all credentials in a single place and not force users to use different wallet applications to handle their verifiable credentials, also the verifiable credentials of EDCI that use ELM v3 are now focused on the verifiable credentials data model to be implemented on the ESSIF/EBSI infrastructure to perform the same operations that the europass credentials of EDCI.

### Development of website and blog <a href="#_heading-h.wst7ta86hwx3" id="_heading-h.wst7ta86hwx3"></a>

During this working package it was also developed a **web site** for the purpose of informing everyone about the project and their features. It was also developed a **blog site** where the development process was summarized and in which it explained the objectives and the different phases that involved the development of the project.

This work package was developed without any deviations, the major achievements are the decomposition of the project into defined tasks.

## Work package 2 - _\<Requirements analysis, concept & design>_ <a href="#_heading-h.3rdcrjn" id="_heading-h.3rdcrjn"></a>

* **Research about the Self-Sovereign Identity (SSI) paradigm in the context of web technologies.** How it works, Why to use SSI paradigm, advantages over the issuance of verifiable credentials.
* **Research about European Digital Identity wallets (EUDI) programme** and their current status & use cases for them. Future of the conformant wallets with the EU standards, review the scope of the project to unify different daily tasks in a single app following defined standards from the EU, focus on the issuance of verifiable diplomas to replace current titles.
* **Research about the European Self-Sovereign Identity Framework (ESSIF).** How ESSIF works, how to be conformant with the framework, which advantages ESSIF has, review the interoperability with other systems that follow this framework, privacy concerns.
* **Research about European Blockchain Service Infrastructure (EBSI) ecosystem,** How EBSI is implemented, which standards EBSI follow, how blockchain or Distributed Ledger Technologies (DLT) works out, how to be compliant with EBSI ecosystem, current status of EBSI, how EBSI is integrated to be use in a SSI wallet, why to use the EBSI ecosystem.
* **Research about the Decentralized Identifiers (DIDs) & Verifiable Credentials (VCs)** Standards that they follow from the W3C, How DIDs are generated, which different methods we have to create a DID, how DIDs are store and where, why the DIDs are essential in SSI, how DIDs can be resolve, which information the DID contains, which types of VCs exist, which standards the VCs follow, how VCs are generated, how VCs are secure & how VCs are signed to be tamper-proof, where VCs are store, how the VCs are shared.
* **Research about the current wallets** that are compliant with the ESSIF/EBSI ecosystem to implement a solution that leverages on them.
* **Research about the verifiable credential schemas** that exist or are defined by an authority, which types are defined in EBSI ecosystem, which fields are mandatory, how to create own schemas, how to use schemas to verify that the verifiable credentials are compliant with the chosen schema.
* **Research about pre-build wallet** **solutions** that follow ESSIF/EBSI ecosystem to make a test pilot and check if its possible to leverage on them.
* **Research the implementation of the SSI wallet** that allows verifiable credentials issuance, planning & design the architecture of the implementation.

In this work package, the **analysis** of the necessary requirements to carry out the **design** and **implementation** of the system has been carried out.

### SSI <a href="#_heading-h.63x9yqyeakcp" id="_heading-h.63x9yqyeakcp"></a>

In this research has been carried out on the **self-sovereign identity paradigm** in the context of Web 3.0, in this process it has been investigated how this new paradigm works, why to use it and its advantages.

In this new paradigm, **the user becomes the owner of their data**, so that there are no third parties that store and manage user data. This paradigm also focuses on a more **decentralized** web, improving the availability of services and making it more **secure** by using blockchain to store information about transactions in a way that cannot be tampered with once they have entered the blockchain and streamline the processes of issuance and verification of verifiable credentials.

To make use of this new paradigm, the system is made up of **3 or 4** fundamental parts, the **issuer** of verifiable credentials, which is in charge of issuing a cryptographically secure digital credential that demonstrates certain qualities or knowledge of the user. The **SSI digital wallet** which is the application that **an end user uses** in order to **manage, receive, share and store verifiable credentials**. The **verifier** which is in charge of receiving the verifiable credentials of the user's wallets to **verify** that they have not been manipulated and that they really belong to the person who has shared that credential. And finally within any decentralized blockchain system the **blockchain ledger** in which certain transaction **records** and other data are **stored** so that a verifier can actually verify the authenticity of a verifiable credential through the use of asymmetric cryptography.

### EUDI wallets <a href="#_heading-h.qexaavp8ot2n" id="_heading-h.qexaavp8ot2n"></a>

Research has been carried out on the European Union program for the creation of digital identity (**EUDI**) **wallets** under the European Digital Identity Framework to enable the **cross-border recognition** of government electronic identification (‘eIDs’) to access public services, and to establish a Union market for trust services recognised across borders with the same legal status as the traditional equivalent paper-based processes. In which a user can make these wallets so the verifiable credentials can be stored, such as a unique European identity through the use of electronic IDentification, Authentication and trust Services (eIDAS2), these wallets are developed by third parties under the **supervision and standards of the European Union defined by the** [**Architecture and Reference Framework (ARF)**](#user-content-fn-1)[^1]. To provide people with c**ontrol over their online identity and data** as well as to enable access to public, private and cross-border digital services.

The objectives of these are:

* To be able to store different types of verifiable credentials which prove something about the user. These verifiable credentials may be stored and shared with third parties to exchange information quickly and securely.
* Secure and trusted identification to access online services
* Mobility and digital driving license
* Health
* **Educational credentials and professional qualifications**
* Digital Finance
* Digital Travel Credential

### ESSIF <a href="#_heading-h.6v8ebsqe1ejo" id="_heading-h.6v8ebsqe1ejo"></a>

The **European Self-Sovereign Identity Framework** was developed by the European Union in the context of the SSI paradigm in order to create an implementation framework to make use of EBSI services to solve problems like:

* Data acquisition and maintenance
* Data processing
* Data silos
* Lack of data control
* Privacy issuer
* Lack of universality
* Lack of interoperability
* Limitations of eIDAS
* Lack of certifications

The use of ESSIF tries to facilitate cross-border interaction with SSI, make national SSI projects interoperable, integrate existing building blocks such eIDAS, stimulate the transformation of public services, build an identity layer within the new European Blockchain Services Infrastructure (EBSI) and stimulate the development and standardization on global level.

This framework is intended to make use of the SSI paradigm in order to avoid that the user’s data is managed by third parties and providing the users the management of all of her/his data and make use of the standards of the **verifiable credentials and decentralized identifiers** within the EBSI services in order to store relevant data on the EBSI ledger and perform operations like the **generation of DIDs, issuance of verifiable credentials or verification of verifiable presentations**. The use of this framework allows us to provide an SSI solution under European standards and rules.

### How ESSIF works <a href="#_heading-h.u0yknsg5z9qs" id="_heading-h.u0yknsg5z9qs"></a>

In the ESSIF framework it is defined by some actors: the issuer, the holder or user, the verifier and the EBSI ledger. From the issuer’s wallet first it generates a DID and a private and public key par, then the issuer stores the public key and registers the DID on the EBSI ledger, then it can issue a verifiable credential to the users. The holder within its own wallet application generates a private key, a public key and a DID in which the public key is encoded, once the holder make a credential request (CR) to the issuer, the issuer issue a verifiable credential to the user and sign it with its private key. Once the user creates a presentation request (PR) to share a verifiable credential with a verifier, the user creates a verifiable presentation that acts as a wrapper of VCs, this VP is signed with the private key that the user holds in its wallet. Once the verifier receive the VP from the holder then it will check the sign of it with the public key of the user that is encoded on the DID of the user, then check if the hash of the content is equals to verify the authenticity of the VP, once it its verified then the verifier will verify the sign of each VC that the VP contains, to perform this, the verifier check the EBSI ledger in order to get the DID and public key of the issuers that they’re registered on the blockchain ledger, it also will check the validity of each VC in order to verify if the VC was revoked or not and finally it will also check that the hash of the content are the same. Once the VP with the VCs inside passes these verification processes then the VP is valid and authentic and then the verifier can use the data of the VCs.

### EBSI <a href="#_heading-h.9vzrd2dvjyvc" id="_heading-h.9vzrd2dvjyvc"></a>

EBSI is an initiative of the European commission to create an ecosystem in order to make use of the SSI paradigm under the european regulations and decentralized ledger technologies, SSI EduWallets is compatible with this infrastructure but it is not used yet, the use of EBSI will enhance all the functionalities related to SSI thats why this project aims to make use of it in a near future.

EBSI is builded over the ESSIF, VCDM and DID standards and the ecosystem is made up of several elements:

* A Trusted Accreditation Organisations (TAO) which is a trusted entity which is responsible for allowing certain entities to issue verifiable credentials of a certain type.
* Trusted verifiable credential issuers (TI) which will be able to issue a certain type of verifiable credential to users, derive from the granting of permissions granted by a TAO.
* A trusted issuer registry (TIR) in which trusted issuers are registered to verify that the issuance of verifiable credentials is only made or valid if the issuer is within this registry this register is inside the EBSI ledger.
* A Trusted Schema Registry (TSR) where verifiable credential schemas are stored for interoperability by having a common registry for everyone within the ecosystem, this register is inside the EBSI ledger.
* Trusted Accreditation Organisation Registry (TAOR) registry that contains information about organizations that can accredit another party (Issuer) to issue certain types of VCs.
* A network of nodes which currently serves as a decentralized database using blockchain technologies, in which the DIDs of the issuers and their public keys are stored.
* Verifiers who will have access to the ledger to obtain the data of the issuers such as their DIDs,public keys, and access to the registry of trusted issuers and schemas in order to verify if a credential is valid or not.

#### [**How DLT works**](#user-content-fn-2)[^2]

Blockchain can be used to store and manage a variety of different types of information,such as financial transactions, contracts, or other records. However, unlike a traditional ledger or filing cabinet, a blockchain is distributed across a network of computers, which makes it highly secure and resistant to tampering or modification.

A blockchain is a linked list of blocks of information that are cryptographically linked together, forming a chain. the process follow the next steps:

1. Each block contains the hash of the prior block in the chain,keeping the integrity of the set of data in the blockchain.
2. Each block can contain transactions, data and a reference to the previous blocks (creating the chain)
3. Each block has a hash that is derived from the block’s content.
4. Each new block contains the hash of the previous block. Transactions are recorded chronologically and cannot be changed once added to the chain.
5. For blocks to be added to the blockchain, it must be achieved through consensus.

When transactions are added to a block, the blocks are validated by the network. Every node maintains an identical copy of the blockchain.

The currents status of EBSI is now in pilot testing phase, that why the SSI EduWallets implementation can't be used within EBSI right now, The advantages of using the EBSI ecosystem are the following:

* Interoperability between verifiable credentials since their scheme is in a common registry, anyone can know the implementation of a certain type of verifiable credential.
* Greater availability and redundancy of data, when using a distributed system.
* Greater security by using a blockchain system to store the records of each transaction made, and using the standards defined by ESSIF creating a trust framework.
* Use of self-sovereign identity, so that it is the users who own their data and not third parties .
* Management of which entities can issue certain types of verifiable credentials.
* Revocation of verifiable credentials to a certain user.

Through the implementation of SSI EduWallets that make use of the API wallet kit, it is possible to configure and perform onboarding in the EBSI infrastructure. To be compatible with EBSI, it is needed to use the ESSIF framework, and to join the ecosystem, the applicant must meet certain requirements that a TAO defines.

### DID <a href="#_heading-h.m68zgk18ew35" id="_heading-h.m68zgk18ew35"></a>

The SSI EduWallets make use of the Decentralized Identifiers in order to provide an unique identifier for anything, A DID is just a long string that does not provide any meaningful information about a natural or legal entity they are a new type of identifier that enables verifiable, decentralized digital identity. The design enables the controller of a DID to prove control over it without requiring permission from any other party. DIDs are URIs that associate a DID subject with a DID document allowing trustable interactions associated with that subject.

Each DID document can express cryptographic material, verification methods, or services, which provide a set of mechanisms enabling a DID controller to prove control of the DID. DIDs are used to ensure the authenticity of issuers and holders in machine verifiable documents known as Verifiable Credentials (VCs). DIDs establish a (distributed) public key infrastructure (DPKI) and allow parties to find each other, authenticate and encrypt and verifiably sign data.

The DIDs are generated by applications such as the wallets of the users or issuers, through the wallet kit API you can make a call to it to generate a DID in that we can choose with which method and cryptography model to generate it.

A variety of “DID methods'', which are different implementations of the DID specification, exist. Considering that DID methods differ in terms of how they are created, registered and resolved, different methods come with different advantages and disadvantages. In this implementation we will be able to generate DIDs through three different methods:

1. **DID:ebsi**: DIDs that are often anchored on Registries, such as EBSI
2. **DID:web:** DIDs that are often anchored to the Domain Name Service
3. **DID:key:** DIDs that do not require Registries because their distribution is based on peer-to-peer interactions.

In the SSI EduWallets implementation it makes use of the **DID:key** method because no registries are being used.

Normally the DID are generated by the users' wallets so that they are the ones in charge of storing them. In the case of an issuer, the same thing can happen, even though within EBSI infrastructure the issuer can store and register its DID in the EBSI ledger.

The information contained in DIDs is given by the DID document information which normally contains the cryptographic form necessary to authenticate the DID controller, metadata, DID controller, context or other attributes.

To resolve a DID, it is necessary that this DID be shared with a system that integrates functions for its resolution. Based on the DID generation method, it will be resolved one way or another. With these data, the function in charge of its resolution performs the resolution and the DID document is obtained, which contains all the information of the DID document, such as public cryptographic keys associated with the DID

### VC <a href="#_heading-h.oqt98puz0za2" id="_heading-h.oqt98puz0za2"></a>

The SSI EduWallets make use of the Verifiable credentials data model in order to create digital credentials that accredit something to someone. Through the use of the wallet kit API, a data model can be generated manually from a JSON document that will be imported with a name given in it to later allow the creation of verifiable credentials of a certain type defined with the data that is supplied to it. provide by filling in the fields that have been defined in the data model or template earlier.

Within the scope of EBSI there are different types of verifiable credentials depending on who they are for. In the case of a normal user who uses EBSI services, in this infrastructure two types of verifiable credentials are defined: verifiable credential to obtain an identity and verifiable educational diploma type credential.

To exchange verifiable credentials between the issuer and the user's wallet, the [OIDC4VC protocol](#user-content-fn-3)[^3] is used, which is an authentication and authorization protocol using tokens that allows a secure data exchange between the issuer and receiver, the same occurs in the case of presenting a verifiable presentation, this time using the [OIDC4VP protocol ](#user-content-fn-4)[^4]which is a variation of the previous one to perform the reverse flow from user wallet to platform or verifier.

Once verifiable credentials are issued to a user's wallet and accepted by the user, these are stored in the user's wallet in the JSON-LD format or as JWT.

Once the credential issuance process is completed before sending it to the recipient user's wallet, the issuer calculates a hash of the credential's content and using its private key encrypts the verifiable credential that is sent to the recipient.

Once the verifiable credential is received in the user's wallet, the user can make a verifiable submission to share this verifiable credential with a third party or verifier. Before sending the verifiable presentation to the verifier, the user calculates the hash of the content of the verifiable presentation which contains the verifiable credential to be shared and then using his private key encrypts the verifiable presentation.

Once the verifier receives the verifiable presentation, it decodes its public key through the DID of the user and verifies that it can really decrypt the verifiable presentation, then it calculates the hash of the content and checks that it also matches, once the user verification is done, the next thing is to check the verifiable credential, in this case if EBSI is being used the verifier will search the EBSI ledger for the DID and public key of the issuer, if no decentralized system based on blockchain is used then the verifier obtains the public key by decoding the DID of the issuer , once the public key is obtained, the previous process is repeated and if both the presentation and the verifiable credential are valid, then the authenticity of the shared verifiable credential data will be confirmed.

### VC schemas <a href="#_heading-h.9piixryoqgxv" id="_heading-h.9piixryoqgxv"></a>

Currently within the EBSI infrastructure there are numerous verifiable credential schemes associated with verifiable credentials of a certain type, even though for real use cases of a user we only find two schemes, the [verifiable credential scheme for identification ](#user-content-fn-5)[^5]and the credential scheme verifiable educational diploma.

Logically, outside of the EBSI ecosystem, we do not find any defined scheme that is stored in the same place as it is in the case of EBSI, but this does not mean that there are no defined schemes, there are, the only problem is that they are not all in the same common registry which can cause problems when verifying and trusting these schemes.

In SSI EduWallets, when a verifiable credential is created, it has to meet certain requirements. These are that the verifiable credential is defined within a [verifiable attestation credential](#user-content-fn-6)[^6], which defines some mandatory fields which are the following: context, id, type, issuer, issuanceDate, issued, validFrom, credentialSubject, credentialSchema.

Once a verifiable credential of a certain type is created, it is subject to the definition of its schema in which certain fields will be defined as mandatory or optional, and once a verifiable credential of a certain type is issued to a user at the moment After presenting it to a verifier, the verifier will carry out a search to obtain the scheme that defines that credential and will go through a validation process in which it will be verified that the definition of the verifiable credential conforms to the defined scheme. This is done through security policies. verification, which the verifier configures.

To create schemes for a certain type of verifiable credential, it is only necessary to create a definition of the fields that the verifiable credential will have and define which of these fields are mandatory or not. In this implementation, to define the scheme of the verifiable credential created, the model used in the already existing EBSI schemes has been followed.

### ESSIF/EBSI compliant wallets <a href="#_heading-h.avcw2r3xly53" id="_heading-h.avcw2r3xly53"></a>

During the course of this work package it was performed a research in order to find wallets compliant with ESSIF/EBSI infrastructure, we manage to found some wallets like the [web wallet of “walt.id”](#user-content-fn-7)[^7] and “ValidatedID” which are compliant with EBSI and both were tested with the SSI EduWallets implementation allowing the issuance flow, in which these wallets could communicate with the implementation in order to receive and perform the exchange process of verifiable credentials between the issuer and the user’s wallet.

The achievements on this work package was the knowledge how this new paradigm of Web 3.0 fits in the current context to the status of the EUDI wallets program, how this implementation works and the architecture behind it.

The major problems of this work package was to research about a lot of different approaches and new concepts and unify them to understand how SSI works and the architecture that composes it. This work package was developed without any deviations from the initial planning.

## Work package 3 - _\<Infrastructure & setup>_ <a href="#_heading-h.3rdcrjn" id="_heading-h.3rdcrjn"></a>

* **Setup of the SSI EduWallets architecture** Definition of the architecture that the project will follow, how the project will be implemented, which approach we follow (microservices or other).
* **Testing some use cases** for our proof of concept, what will be needed to perform verifiable credentials issuance, how the users are signed up in the platforms through an SSI wallet, how the platform can verify the verifiable credentials that the users send to / share with the platform.
* **Issue Verifiable Credentials** How it’ll be issued, which steps are needed to issue a verifiable credential to a user, which fields are essential within verifiable credentials.
* **Verify Verifiable Credentials** How the verifiable credentials that the users share with the platform are verified, which steps are needed to verify a VC, what happens after the verification process finishes.
* **Setup a basic web interface** to create a test pilot of a real user claiming a verifiable credential to the platform.
* **Implementation of the endpoints** for the registration/login using a SSI wallet, issuance request, issuance of a verifiable credential, verifiable credential request, verification of verifiable credentials.
* **Setup of the hardware,** which hardware is needed and how it’ll hold the API, how many resources are needed, which budget is needed, how it’ll be configured.

In this work package, the construction and start-up of the project architecture on the real infrastructure has been carried out.

### Setup of the SSI EduWallets architecture <a href="#_heading-h.ek9h9o4ytsgf" id="_heading-h.ek9h9o4ytsgf"></a>

Since the project is made up of different software components, the most appropriate architecture for this is microservices in which the different APIs will be isolated from each other, but in turn will be interconnected through configuration files.

### Testing some wallets use cases <a href="#_heading-h.bj7w2doz63k0" id="_heading-h.bj7w2doz63k0"></a>

During the course of this work package, proof of concept has been conducted to see if it made sense to implement an implementation based on educational verifiable credentials in the context of SSI.

Once a verifiable educational credential design has been decided, tests have been carried out to reproduce the issuance and verification flow that a user would carry out on a platform that uses this system. And it has been verified that both flows have been possible through the Open API and the API wallet kit.

### Issuance process of verifiable credentials <a href="#_heading-h.ewmuik1srycp" id="_heading-h.ewmuik1srycp"></a>

The appropriate configuration has been made in the wallet kit API to allow the issuance of verifiable credentials to users of compatible wallets. Verifiable credentials will be issued by the wallet kit API through the issuance API to user wallets.

To issue a verifiable educational credential, the user of the platform must complete a course so that he or she can issue the credential with the data of the platform, the user, and the achievements obtained at the end of the course.

The required fields of the educational verifiable credential are the following: context, id, type, issuer, issuanceDate, issued, validFrom, credentialSubject, credentialSchema

### Verification process of verifiable credentials <a href="#_heading-h.wyzizbi1frd3" id="_heading-h.wyzizbi1frd3"></a>

The appropriate configuration has been made in the wallet kit API to allow the verification of verifiable submissions from the users' wallets to the verifiers.

Once a user interacts with the verification UI components it sends a request to the verification API to connect to the user's waller through the [OIDC4VP](https://openid.net/specs/openid-4-verifiable-presentations-1\_0.html) protocol then the users can share the verifiable credentials they have in their wallets by choosing the verifiable credentials from the wallet, then a connection between the user's wallet and the verifier is created thus exchanging a verifiable presentation containing the verifiable credentials the user has chosen to share.

Through the verification API, the user's VP is received and it is processed and forwarded to the API wallet kit to be verified, then the API wallet kit performs the verification and returns the result of this together with the credentials to the verification API verifiable.

Once the verification process finishes each platform (verifier) decides how to process the information to be used within the platform.

### Basic web interface to perform the issuance process <a href="#_heading-h.nxiscz67wvz1" id="_heading-h.nxiscz67wvz1"></a>

A basic user interface component has been designed and created as a test for the issuance of a verifiable credential. This component consists of a button which makes a request to the API for the issuance of verifiable credentials, which is in charge of starting the issuance flow from this graphic component.

### Implementation of Open API endpoints <a href="#_heading-h.qddebibzqskb" id="_heading-h.qddebibzqskb"></a>

Adding open API endpoints to the issuance and verification UI components to enable interaction between UI components and user wallets to perform verifiable credential issuance and presentation verification flows verifiable.

### Hardware setup <a href="#_heading-h.tz0meyi6eu9x" id="_heading-h.tz0meyi6eu9x"></a>

Selection of the server software that will execute the Open API and the API wallet kit, how many resources will be dedicated to it, what budget will be allocated, how it will be configured and what architecture will be followed.

For the deployment and start-up of the project, it has been chosen to use a microservices architecture through the use of kubernetes and Helm chart, in the kubernetes configuration file the resources that will be used to start up the entire stack that makes up the project will be established. project, the software on which the different software components will be executed will be on containers through nginx servers

The achievements on this work package was to set up an API that allows us to implement an SSI Kit to perform the task of creating DIDs,VC types, issue the VC to the users and verify the VC that the users send to the verifier.

The major problems was to read and understand all the documentation about how to implement the workflows with the SSI wallets for issue a signed credential and verify it, also we are trying to get access to the EBSI ecosystem to implement a whole solution based on the Europe Union infrastructure to be fully compliant with European Union standards and ensure interoperability. Our request to the EU/EBSI team is under review.

This work package was developed with some deviations to become compliant with the EBSI ecosystem EBSI.

## Work package 4 - _\<Prototype development>_ <a href="#_heading-h.lnxbz9" id="_heading-h.lnxbz9"></a>

* **Implementation of writing the verifiable credentials** to the user‘s wallet, once the verifiable credential is created: how the exchange between platform (issuer) and the user (holder) is performed, how the user stores its verifiable credential in his/her wallet.
* **Implementation of the reading a verifiable credential** from a user‘s wallet, How the exchange process between user (holder) and the platform (issuer) is created, how the issuer API verify the verifiable credentials that the user share with the platforms, how the data of the verifiable credentials are manage inside the platforms
* **Documentation of the code** that we‘re implementing, how the wallet kit needs to be implemented in our ecosystem (PoC), which API calls are defined in the application, instructions on how to integrate the project in ecosystems.
* **Implementation of open API** to serve as an intermediary to communicate the user request with the platform and the issuer wallet.
* **Implementation of authentication system** that allows users to signup and sign in on the platforms, integration of an IDP kit to allow users to perform those actions using an SSI wallet.
* **Build and link the whole SSI ecosystem** build the open API and integrate the IDP API and SSI wallet API.
* **Implementation of the EBSI onboarding** integrating the project with the EBSI ecosystem.
* **Implementation of UI components** to perform the actions of sign up / sign in, verifiable credentials issuance, and verification of verifiable credentials.

In this work package, the entire implementation of the system has been developed once the different development options derived from the research process previously carried out have been evaluated.

### Store verifiable credentials on the user’s wallet <a href="#_heading-h.2xuggt57cwi1" id="_heading-h.2xuggt57cwi1"></a>

Once a verifiable credential is created through the API wallet kit, it is to be shared from the platform with the user's wallet, some processes need to be carried out to allow its exchange. In this process, the [OIDC4VC](https://openid.net/specs/openid-4-verifiable-credential-issuance-1\_0.html#name-introduction) protocol is used for the issuance of verifiable credentials and the [OIDC4VP](https://openid.net/specs/openid-4-verifiable-presentations-1\_0.html) protocol for the presentation of verifiable presentations, this protocol is an authentication and authorization protocol based on the [OAuth 2.0 protocol](#user-content-fn-8)[^8], it is used to exchange information between an issuer and a receiver through the use of tokens that are used to verify the identity of the user and thus be able to access the protected resources.

Once communication has been successfully initiated between the issuer of the verifiable credential and the user's wallet then the wallet is able to read and interpret the verifiable credential and decide whether or not to store the verifiable credential in its wallet.

### Reading verifiable credentials (Verifiable presentation) <a href="#_heading-h.h97lz2p1j88a" id="_heading-h.h97lz2p1j88a"></a>

Once a user through his wallet wants or needs to share a verifiable credential with a third party or verifier, this from the wallet itself creates a verifiable presentation which acts as a wrapper on the verifiable credentials that he decides to share, to carry out the process To exchange credentials from the user's wallet to the verifier, it is necessary to use the [OIDC4VP](https://openid.net/specs/openid-4-verifiable-presentations-1\_0.html) protocol, which is an authentication and authorization protocol based on the OAuth 2.0 protocol, it is used to exchange information between a sender and a receiver through the use of tokens that are used to verify identity and therefore have access to protected resources.

Once the verifiable credential is received by the verification platform, it makes a request with the necessary data to the verification API, which will process the received data and forward the request against the API wallet kit, which will be in charge of verifying the verification. verifiable presentation by checking both the user and issuer signatures of the VCs in addition to checking the hashes of the verifiable presentation content. Once the verifiable presentation is validated, the API wallet kit returns the response with the validation result and the content of the verifiable credentials to the verification API and this is in charge of providing the verification UI component on whether the result of the validation is correct or not. Once the content of the validated verifiable credentials has been obtained, each platform can carry out the pertinent processing on that data.

### Implementation of a Open API <a href="#_heading-h.dalpufwq5k6v" id="_heading-h.dalpufwq5k6v"></a>

An Open API was developed in order to serve as an **intermediary** between the Wallet Kit API, which is the core in charge of performing all the tasks related to SSI, such as the creation of DIDs, issuance of verifiable credentials, creation of verifiable presentations and their validation.

This API is divided into **two functionalities**, both of which are responsible for communicating directly with user wallets through a graphical interface on the platform that integrates the implementation.

The first functionality is **the issuance of verifiable credentials** to the users of the platforms, this is launched through the issuance UI on the platform which initiates the issuance flow. From this UI, a request is made to the issuance API, which processes the request and makes another request to the Wallet kit API. Once it reaches the Wallet Kit API, it processes the request and returns the result to the issuance API, which will later return this same result to the user's wallet already with the verifiable credential.

The other functionality is the verification of verifiable credentials, through the verification user interface the user sends a request from the frontend of the platform to the verification API which processes the request and forwards it to the API wallet kit that is It is responsible for making a connection between the user's wallet and the verification API. This makes a redirection to the user's wallet asking for certain types of verifiable credentials, once the user chooses the verifiable credential or credentials that he wants to share with the platform, the user's wallet creates a verifiable presentation with the verifiable credentials or credentials that he has chosen. and it is sent to the verification API which will process the request and forward it to the API wallet kit to be verified.

Once verified, the API wallet kit will return the verification result to the verification API, which will manage the presentation data and display the verification result on the platform's verification UI.

### Documentation of the Open API <a href="#_heading-h.6l7onbabte9s" id="_heading-h.6l7onbabte9s"></a>

Once the development and implementation of the Open API has been completed, a document has been created that documents the Open API endpoints. This document is in **JSON format** that can be opened by web applications such as "[**swagger.io**](#user-content-fn-9)[^9]" to graphically display the documentation of the endpoints implemented to perform verifiable credential issuance and verification flows.

### Implementation of authentication system <a href="#_heading-h.fd6ezhauovzb" id="_heading-h.fd6ezhauovzb"></a>

Through the implementation of the verification API, it is possible for a user from their wallet to create verifiable presentations and send them against the platform that implements SSI EduWallets, in such a way that a certain type of credential can be configured to be presented in order to to be validated and allow access to the platform.

### Building the SSI EduWallets <a href="#_heading-h.2025nneb4xni" id="_heading-h.2025nneb4xni"></a>

Once the system components have been correctly developed, the next step is for them to communicate with each other to unite the system and allow the correct use of the implementation. This is done by configuring the configuration files of the wallet kit API to define how it will connect to the open API, which endpoints are available for the open API and the port configuration of the containers where the different APIs such as the wallet kit are executed. API and the Open API.

### Implementation of EBSI onboarding <a href="#_heading-h.1gih9qit7dyq" id="_heading-h.1gih9qit7dyq"></a>

Through the integration of the API wallet kit it is possible and ready to be integrated with the EBSI ecosystem. But because EBSI is still in the testing phase, it is not available in production at the moment, so the system has been implemented outside of any decentralized blockchain service so that the credentials are verified directly with the public keys encoded in the user and issuer DIDs

### Implementation of UI components for issuance and verification <a href="#_heading-h.pmjbi6rtb6gj" id="_heading-h.pmjbi6rtb6gj"></a>

To carry out both the issuance and verification flows of verifiable credentials on a platform that implements this system, graphical components are needed, which can be integrated into the platforms, so that each one of them will be in charge of carrying out an operation.

The issuance component is in charge of graphically displaying in the platform UI the steps to follow to issue verifiable credentials to users, and the verification component is in charge of graphically displaying in the platform UI the steps to follow to perform the presentation of verifiable credentials.

This work package was developed with some deviations while trying to integrate and obtain access to the pilot testing within the EBSI, the major achievement was to perform the issuance and verification through an open API between the user and the issuer.

The major problems are related to the steps needed to exchange a verifiable credential with the wallet of the user.

## Work package _5 - \<Project management & documentation>_ <a href="#_heading-h.vvibs3gi6rup" id="_heading-h.vvibs3gi6rup"></a>

* **Documentation and final report of the project.** Creation of a general documentation about the project and their features.
* **User documentation.** Creation of a user manual to introduce this target audience to the project, this documentation is an abstraction of the technical architecture of the project.
* **Developer documentation.** Creation of a developer manual to introduce this target audience to the implementation and integration of the project to convey details to be enabled to reuse the SSI approach.
* **Summarize report** creation of a short document to summarize the whole project.
* **QMS integration.** How to reuse the Qualification Metadata Schemata in context with wallets.
* **ESCO integration**. How to reuse the European Skills, Competency & Occupations Taxonomy in context with wallets.
* **Public relation report.** Creation of a public relation report.
* **Marketing.** Creation of a marketing report.

In this work package, the documentation of the implementation and investigation of the system, marketing and public relations has been carried out. There has also been research on the integration of QMS and ESCO within the educational verifiable credentials.

### Final report of the project <a href="#_heading-h.dkbxbiwdqv" id="_heading-h.dkbxbiwdqv"></a>

In this final report of the project, a general summary of the research, development and implementation process of the SSI EduWallets project is made.

### User documentation <a href="#_heading-h.vcu6yjljiyui" id="_heading-h.vcu6yjljiyui"></a>

A dedicated documentation has been created for users of platforms that integrate SSI EduWallets in order to show how the implementation works so that a user can interact with the system in order to issue and verify verifiable credentials.

### Developer documentation <a href="#_heading-h.6h7wsjglkc53" id="_heading-h.6h7wsjglkc53"></a>

A documentation dedicated to developers has been created in order to document all the technical aspects of the project and put any developer in context of the system architecture in order to allow the development, implementation and deployment of the on any platform.

### Summarize report <a href="#_heading-h.85yht4ec4t9r" id="_heading-h.85yht4ec4t9r"></a>

A brief abstract report on what SSI EduWallets is, how it works, who it is for and a list of project results has been created in order to briefly report what the project is about.

### QMS integration <a href="#_heading-h.xtg0gmo4ihqw" id="_heading-h.xtg0gmo4ihqw"></a>

During the implementation of this system, it has been investigated how to integrate the necessary fields in the structure of the verifiable credential in JSON-LD format that makes up the structure of the verifiable credential in ELMv3. Since ELM and its respective components, such as QMS, have been developed solely for the purpose of being used in the EDCI infrastructure on europass, to make use of this system an integration of QMS over the verifiable credentials has been attempted, but in this proof of concept **a full QMS integration has not been done** as this data model has been pre-developed to the verifiable credentials data model and EBSI infrastructure.

### ESCO integration <a href="#_heading-h.ukzkmjzd7ypv" id="_heading-h.ukzkmjzd7ypv"></a>

Research has been carried out on **European Skills, Competences, Qualifications and Occupations** and how to integrate this classification system within verifiable educational credentials in order to obtain important metadata about skills, qualifications and occupations that a user achieve through the completion of a learning opportunity in a multilingual format in order to be recognized at a European level and therefore both have a common and interoperable set of learning outcomes.

This work package was developed without any deviation.

## Work package _6 - \<Quality management, IT compliance & data protection (guidelines)>_ <a href="#_heading-h.pkjd31hhqovm" id="_heading-h.pkjd31hhqovm"></a>

* **EBSI assessment.** Onboard to the EBSI ecosystem into the project and validate the data on the European Blockchain Services Infrastructure, compliance to their rules, regulations and standards.
* **Check the compliance of SSI & GDPR.** Review the compliance of the SSI paradigm and the GDPR to fulfill the current data protection laws in the scope of the European Union.
* **Evaluation of the European Learning Model (ELM)** Matching different data formats to be ELM and wallet kit compliant, compare differences between LOM (IEEE1484.12.1) with[ Learning Opportunities Metadata Schema (LOMS)](#user-content-fn-10)[^10], importance of EQR/QDR, compliance & structure of EU diplomas.
* **Evaluation of license models** to distribute the project under certain restrictions & requirements.

In this work package a review of IT compliance and data laws protection was done in order to guarantee a legal and correct implementation of the SSI EduWallets project within e-learning platforms.

### EBSI assessment <a href="#_heading-h.qnz8pzu2tc9w" id="_heading-h.qnz8pzu2tc9w"></a>

An evaluation of EBSI has been made on which requirements are necessary to be able to be onboarded on the EBSI ecosystem as issuers of verifiable credentials and how to perform the verification of verifiable credentials using the EBSI ledger. It has also been investigated how to proceed as a **trusted issuer** to obtain permission to **issue verifiable educational credentials**, in this case an admission process must be carried out in which the issuer makes a request to a Trusted Accreditation Organisation (TAO) which is in charge of decide whether to accept the new issuer as trusted issuer of a certain type of verifiable credentials. If the TAO accepts the request then the issuer is registered on the Trusted Issuer Registry (TIR) and then the issuer only needs to generate a DID, a private and public key and store the DID and the public key on the EBSI ledger.

Through the implementation of SSI EduWallets is possible to be onboarded on the EBSI infrastructure but for now, it is not possible yet due that EBSI is in a test phase and not production ready.

### ELM <a href="#_heading-h.g6ik7n8g6kdg" id="_heading-h.g6ik7n8g6kdg"></a>

During the course of the project, an attempt has been made to implement the European Learning Model (ELM) data model to have a data structure under a common framework, in order to obtain detailed skills documentation and complete interoperability between systems that use this system that the European commission has developed, however it has not been possible to perform a complete integration of this data model, since for this proof of concept it does not make sense to create such an extensively detailed verifiable credential with the necessary fields and classes that ELM proposes, so a simpler variant has been developed but making use of some of the classes and fields that this model uses.

### Compliance with GDPR <a href="#_heading-h.3v0kfpvc07zv" id="_heading-h.3v0kfpvc07zv"></a>

Due that this project is intended to work with the user’s data we had to research about the compliance with the actual data laws in the european scope, we found out that with the new paradigm of SSI the user's data protection is much higher than before and then accomplished the General Data Protection Regulation (GDPR).

### License models <a href="#_heading-h.4pi43jr9nr8o" id="_heading-h.4pi43jr9nr8o"></a>

The project was evaluated in order to set a license model under certain restrictions and requirements of the project. The criteria considered in order to choose the licenses were encouraging collaboration, reusability, and future development, as well as providing transparency trust, and accountability as users can inspect the code. Keeping the commercial feasibility of the outcome of the project was also an important topic considered. The main developments are licensed under the MIT license. Some other developments are extensions on top of existing projects with already defined licenses like Apache License v2, in this case the original license was kept. General documentation is licensed under Creative Commons.

This work package was developed without any deviation. The major achievement was the assessment of the architecture & technical details of the EBSI ecosystem & its impact on GDPR compliance, as well as, identifying relevant ELM data models and their schemata in terms of compatibility.

## Work package _7 - \<Documentation and formalities at the end of the project>_ <a href="#_heading-h.k0uwptxun681" id="_heading-h.k0uwptxun681"></a>

* **Reports submission** of the project when it's finally finished.
* **Submission of all reports**
* **Bill/invoices submission**

In this last work package is performed the submission of the documentation reports and the bill/invoices submission.

[^1]: European Commission (no date c) The European Digital Identity Wallet Architecture and Reference Framework, Shaping Europe’s digital future. Available at:[ https://digital-strategy.ec.europa.eu/en/library/european-digital-identity-wallet-architecture-and-reference-framework](https://digital-strategy.ec.europa.eu/en/library/european-digital-identity-wallet-architecture-and-reference-framework) (Accessed: 08 August 2023).

[^2]: European Commission(10/06/2021) , EBSI Architecture, explained. Available at: [https://www.google.com/url?sa=t\&rct=j\&q=\&esrc=s\&source=web\&cd=\&cad=rja\&uact=8\&ved=2ahUKEwiWtJqk1OKAAxXZUqQEHf6cBVIQFnoECB4QAQ\&url=https%3A%2F%2Fec.europa.eu%2Fdigital-building-blocks%2Fwikis%2Fdownload%2Fattachments%2F447687044%2F%2528210610%2529%2528EBSI\_Architecture\_Explained%2529%2528v1.02%2529.pdf%3Fapi](https://www.google.com/url?sa=t\&rct=j\&q=\&esrc=s\&source=web\&cd=\&cad=rja\&uact=8\&ved=2ahUKEwiWtJqk1OKAAxXZUqQEHf6cBVIQFnoECB4QAQ\&url=https%3A%2F%2Fec.europa.eu%2Fdigital-building-blocks%2Fwikis%2Fdownload%2Fattachments%2F447687044%2F%2528210610%2529%2528EBSI\_Architecture\_Explained%2529%2528v1.02%2529.pdf%3Fapi)%3Dv2\&usg=AOvVaw2MiBXC8nid8v-sO-TU2Jyn\&opi=89978449

[^3]: T. Lodderstedt,K. Yasuda, T. Looker (03/02/2023), OpenID for Verifiable Credential Issuance. Available at: [https://openid.net/specs/openid-4-verifiable-credential-issuance-1\_0.html#name-introduction](https://openid.net/specs/openid-4-verifiable-credential-issuance-1\_0.html#name-introduction)

[^4]: O. Terbu, T. Lodderstedt,K. Yasuda, T. Looker (21/04/2023), OpenID for Verifiable Presentations - draft 18. Available at: [https://openid.net/specs/openid-4-verifiable-presentations-1\_0.html](https://openid.net/specs/openid-4-verifiable-presentations-1\_0.html)

[^5]: European Commission (no date d) Verifiable attestation for ID, Verifiable Attestation for ID - EBSI Specifications -. Available at: [https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Verifiable+Attestation+for+ID](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Verifiable+Attestation+for+ID) (Accessed: 12 August 2023).

[^6]: European Commission (no date c) Verifiable attestation, Verifiable Attestation - EBSI Specifications -. Available at: [https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Verifiable+Attesttation](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/Verifiable+Attesttation) (Accessed: 09 August 2023).

[^7]: Walt.id ,Walt.id Web wallet, walt.id. Available at: [https://github.com/walt-id/waltid-web-wallet](https://github.com/walt-id/waltid-web-wallet) (Accessed: 08 August 2023).

[^8]: Oauth OAuth 2.0, OAuth. Available at: [https://oauth.net/2/](https://oauth.net/2/) (Accessed: 12 August 2023).

[^9]: Swagger API development for everyone, Swagger. Available at: [https://swagger.io/](https://swagger.io/) (Accessed: 12 August 2023).

[^10]: European Commission (2020) ‘Publishing of Qualification and Learning Opportunity Data Documentation’.
