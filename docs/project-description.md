# Project description

## Goals and benefits of the project

The main goal of this project is the implementation of a “proof-of-concept” capable of being integrated in educational platforms so third parties can make profit of the new Web 3.0/SSI paradigm and leverage the features of user privacy and the issuance of educational verifiable credentials to the users. Verifiable credentials are a representation of an accreditation of knowledge achieved under a qualification framework like the[ Qualification Metadata Schemata (QMS)](#user-content-fn-1)[^1]. This schemata are used to classify the learning outcomes in a detailed specification and also make use of the [European Skills, Competences, Qualifications and Occupations (ESCO) ](#user-content-fn-2)[^2]taxonomy.

Once a user finishes a course or assessment he/she can share verifiable presentations (VPs) for verification and validation of learning achievements via wallet and a platform. The main benefits of the implementation are the capabilities of a platform to issue a digital, cryptographically secure, semantically rich credential that specifies qualifications, skills and occupations under a common framework and, furthermore, to enhance interoperability between independent systems. Certificates which are issued as educational verifiable credentials are easy to verify and difficult to manipulate.

The SSI EduWallets implementation seeks to eliminate the problems of the user data handling, the forgery of traditional credentials, and the non-interoperability between systems. It streamlines the process of the issuance of a secure educational credential and the verification of the credentials saving time and costs.

## Target group

The SSI EduWallets project is intended for platform operators (SMEs, corporates, governmental, non-governmental) or platform providers (LMS reseller, Learning App provider) which want to extend their platforms with the new paradigm of SSI, eliminating data privacy & security issues related to the management of users’ personal/private data.

Once integrated, they allow their users to perform secure identification using EUDI/ESSIF compliant wallets via verifiable presentations (VPs) of learning achievements or issuance of educational verifiable credentials to streamline accreditations & certificates (and leverage the use of DIDs and VCs).

## Content overview

SI EduWallets provide a series of software components that create a stack needed to implement the new paradigm of SSI and the use of educational verifiable credentials within learning platforms.

Through the components of **EUDI wallets**, **issuer API**, **verifier API,** **wallet kit API**, **issuance and verifier user interface** the implementation contains following user journeys:

* Users can use EUDI wallet applications within the platforms that implement SSI EduWallets to exchange data. Those wallets store and manage the users’ data instead of third parties, **enhancing the users’ privacy**.
* Platforms that implement SSI EduWallets can **issue digital, cryptographically secure education diplomas** called "educational verifiable credentials" that represent an accreditation of the achievements that a user receives once he/she completes a course or assessment. These verifiable credentials will be stored and managed inside the user's wallets.
* The educational verifiable credentials issued consist of the data of the user (holder), the data of the platform (issuer), the data of the course or assessment and the issuer's signature. The information about the course or assessment follows the Qualification Metadata Schemata and ESCO in order to provide **useful information about the skills, qualifications and experiences** that the user achieves through a learning experience in order to **obtain new learning opportunities and occupations**.
* Platforms implementing SSI EduWallets can receive verifiable presentations from users' wallets to **verify and validate shared verifiable credential data** and use within platforms to **streamline some internal processes**.
* The verifiable credentials are **interoperable between different systems** because they are defined under a common data schema. Independent/external systems can recognize and then parse the structure and constraints of the specific type of verifiable credentials.
* The management processes of the interoperability, issuance and verification of the credentials are **more simple and faster improving the user experience, saving costs and time** because the intermediary parties usually in charge of performing those tasks are removed.
* Verifiable credentials are **secure because they use asymmetric cryptography technologies and digital signatures** to make them tamper-proof, easy to verify and difficult to forge.
* The data of the issuers like DIDs, public keys and transactions are **redundant, highly available and secure** if the system uses the blockchain infrastructure of [European Blockchain Services Infrastructure (EBSI)](#user-content-fn-3)[^3] in order to verify a verifiable credential.

[^1]: European Commission (2020) ‘Publishing of Qualification and Learning Opportunity Data Documentation. Available at: [https://europa.eu/europass/system/files/2020-07/Documentation\_publishing%20of%20Q%20and%20LO%20Data\_v2.0.pdf](https://europa.eu/europass/system/files/2020-07/Documentation\_publishing%20of%20Q%20and%20LO%20Data\_v2.0.pdf)

[^2]: ESCO About Esco, ESCO. Edited by European Commission. Available at: [https://esco.ec.europa.eu/en/about-esco](https://esco.ec.europa.eu/en/about-esco) (Accessed: 08 August 2023).

[^3]: European Commission European Blockchain Services Infrastructure, Home - EBSI -. Available at: [https://ec.europa.eu/digital-building-blocks/wikis/display/EBSI/Home ](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSI/Home)(Accessed: 09 August 2023).
