# Demo Application

Back-end and front-end for the issuer and verifier demo application.

This source code it is supposed to be a demo that can serve as implementation example.

It should be implemented as part of a 3rd party application (LMS, learning platform, etc) in order to take advantage of SSI Edu Wallets.

## License
The source code for the site is licensed under the MIT license, which you can find in the LICENSE file.
