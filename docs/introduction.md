# Introduction

The Self-Sovereign Identity (SSI) EduWallets project aims to revolutionize the way we manage our personal information and privacy in the digital age of the Web 3.0. By harnessing the power of decentralized and distributed technologies such as blockchain, verifiable credentials (VCs) and self-sovereign identity, this project seeks to give users complete control over their data, freeing it from the silos of third-party storage and therefore build a more transparent and trustworthy digital ecosystem.

<figure><img src=".gitbook/assets/Web_generations-edited.png" alt=""><figcaption></figcaption></figure>

[_Different data paradigms along the web generations._](#user-content-fn-1)[^1]

The SSI EduWallets  project focuses on the implementation of a user management system for online platforms. Users are allowed to interact with the platforms that implement this system and perform the issuance of educational verifiable credentials once they complete a course or assessment through the use of [European digital identity (EUDI)](#user-content-fn-2)[^2] wallets. The presentation of verifiable credentials that are stored in the user’s wallet help to enhance the platform’s “learning experiences” by verifying learning histories and using this data to provide a better overall experience on the platform.

This approach, based on SSI, [W3C Decentralized identifiers (DIDs)](#user-content-fn-3)[^3],[ W3C Verifiable Credentials Data Model (VCs)](#user-content-fn-4)[^4] and EUDI/ [European Self-Sovereign Identity Framework (ESSIF)](#user-content-fn-5)[^5] standards, ensures interoperability and security between different systems within the European Union.

Once a user receives a verifiable credential it will be stored in the user’s wallet for future verification. By digitally signing each verifiable credential and ensuring standardization of formats, the project seeks to end the problems of interoperability, forgery and inconsistency that plague traditional educational certificate systems.&#x20;





[^1]: European Commission The European Digital Identity Wallet Architecture and Reference Framework, Shaping Europe’s digital future. Available at: [https://digital-strategy.ec.europa.eu/en/library/european-digital-identity-wallet-architecture-and-reference-framework](introduction.md) (Accessed: 08 August 2023).

[^2]: European Commission The European Digital Identity Wallet Architecture and Reference Framework, Shaping Europe’s digital future. Available at: [https://digital-strategy.ec.europa.eu/en/library/european-digital-identity-wallet-architecture-and-reference-framework](https://digital-strategy.ec.europa.eu/en/library/european-digital-identity-wallet-architecture-and-reference-framework) (Accessed: 08 August 2023

[^3]: Sporny, M. et al. Decentralized identifiers (DIDs) v1.0, W3C. Available at: [https://www.w3.org/TR/did-core/](https://www.w3.org/TR/did-core/) (Accessed: 08 August 2023).

[^4]: Sporny , M., Longley , D. and Chadwick , D. Verifiable credentials data model V1.1, W3C. Available at: [https://www.w3.org/TR/vc-data-model/#abstract](https://www.w3.org/TR/vc-data-model/#abstract) (Accessed: 08 August 2023).

[^5]: Pastor Matut, C. and Du Seuil, D. Understanding the European self-sovereign identity framework (ESSIF), PPT. Available at: [https://www.slideshare.net/SSIMeetup/understanding-the-european-selfsovereign-identity-framework-essif ](https://www.slideshare.net/SSIMeetup/understanding-the-european-selfsovereign-identity-framework-essif)(Accessed: 08 August 2023).
