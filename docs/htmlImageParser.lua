
function convertHtmlImageToMarkdownImage (elem)
    if elem.tag == "RawInline" and elem.format == "html" and elem.text:match '%<img' then
        local imgHtml = elem.text
        local alt = imgHtml:match('alt="(.-)"') or ""
        local src = imgHtml:match('src="(.-)"') or ""
        local title = imgHtml:match('title="(.-)"') or ""

        return pandoc.Image({}, src, title, alt)
    end
end

return {
    {RawInline = convertHtmlImageToMarkdownImage}
}
