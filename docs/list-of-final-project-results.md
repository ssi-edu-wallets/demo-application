# List of final project results



|                                                      |                      |                                                                                                                                             |
| ---------------------------------------------------- | -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- |
| _Interim Report_                                     | _CC BY-SA 4.0_       | _Interim report of the project in which the main functionalities and implementation are described._                                         |
| _Final Report_                                       | _CC BY-SA 4.0_       | _Final report of the project in which all the components, architecture and functionalities of the project are described._                   |
| _Developer Documentation_                            | _CC BY-SA 4.0_       | _Technical documentation for developers._                                                                                                   |
| _User Manual_                                        | _CC BY-SA 4.0_       | _Non-technical documentation for end users._                                                                                                |
| _One Pager_                                          | _CC BY-SA 4.0_       | _Project summary._                                                                                                                          |
| _External Communication_                             | _see Final Report_   | _Stakeholder communications._                                                                                                               |
| _SW project result part\_1: Demo Application_        | _MIT_                | [_https://gitlab.com/ssi-edu-wallets/demo-application_](https://gitlab.com/ssi-edu-wallets/demo-application)                                |
| _SW project result part\_2: Wallet Proof of Concept_ | _Apache License 2.0_ | [_https://gitlab.com/ssi-edu-wallets/wallet-proof-of-concept_](https://gitlab.com/ssi-edu-wallets/wallet-proof-of-concept)                  |
| _Verifiable Credentials, QMS & ELM_                  | _CC BY-SA 4.0_       | _Documentation about Verifiable Credentials in context of the European Learning Model (ELM) and the Qualification Metadata Schemata (QMS)._ |
| _ESCO API_                                           | _CC BY-SA 4.0_       | _Documentation about API integration of ESCO taxonomy provided by the European Commission._                                                 |
