# Application of the project results in practice

The project results are intended to be used in the context of Learning Management Systems (LMS) or educational online platforms or environments (i.e. Learning Ecosystems). Documentations as well as public repositories provide a general introduction on how to set up a wallet architecture based on the concepts of Self-Sovereign Identity (SSI). Our research also contains schemata layouts to be compliant to

* the European Learning Model (Europass / ELM) via Verifiable Credentials as well as
* the European Self Sovereign Identity Framework (ESSIF) and European Blockchain Service Infrastructure (EBSI)
