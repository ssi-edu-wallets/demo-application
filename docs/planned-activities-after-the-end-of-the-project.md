# Planned activities after the end of the project

* We plan to reach out to commercial partners & customers which are interested in integrating the EduWallet approach with us.
* Together with existing clients (e.g. Universities) we want to establish “best-practice” use cases to promote the EduWallet approach to the public.
* We will work on our main landing page ([https://join.courseticket.com/wallet](https://join.courseticket.com/wallet)) and make more details & main code repositories available.&#x20;
* We plan to advertise the landing page ([https://join.courseticket.com/wallet](https://join.courseticket.com/wallet)) via SEM (Google Ads).
