# Research and challenges

## The problem of today’s user data management

Nowadays the society is more concerned about the user data privacy, this is reflected in Europe for example with the [General Data Protection Regulation (GDPR)](#user-content-fn-1)[^1]. Currently there exist enterprises that are handling our data to perform operations like bespoke Identity providers (IDPs). As customers we have to fully trust that these companies do not use our data negligently or illegally for other purposes but in the end we have no proof where the data is going.

The new paradigm of Web 3.0 and SSI tries to get rid of those challenges. Now it is the user who has his/her personal data stored in a wallet application and it is the user who chooses which data he/she wants to share and with whom.

## The problem with current credentials

Nowadays the credentials pass through complex and slow processes that involves more than one entity to perform actions like the issuance of a educational diploma or the verification of the presentation of a document that accredit something and even can't ensure that the credentials are not forged. They are easy to manipulate or fake  and authenticity is very difficult to prove.

Another common problem is the poor interoperability that those traditional diplomas have, because they don't usually follow any standard to convey the relevant information in the same way. &#x20;

A simple example: If a user studied in Spain and achieved a degree certificate. He wants to apply for a master degree at a University in Germany. The student and the University administration need to perform some slow processes to validate the degree certificate and then accept the application for the master degree.

This problem is solved with SSI EduWallets: Users can receive an educational diploma in the shape of a digital diploma that follows a common standard schemata. This digital diploma is called "verifiable credential", delivered in the  format of JSON-LD or JWT, and build upon the information about the student, the platform that issues the verifiable credential, the course or assessment information, and the signature (DID) of the issuer of the verifiable credential (which makes the verifiable credential secure thought the use of asymmetric cryptography).

## Why to use SSI EUDI Wallets on learning platforms?

This new paradigm about people's identity on the Web will solve many problems:

1. **Privacy:** SSI wallets allow users to share only the necessary information to verify a credential without revealing unnecessary personal information. This protects the user's privacy and prevents third parties from collecting and using their data without their consent. Over time, the European Union has been implementing regulations regarding what can or cannot be done with user data like the GDPR. With this new paradigm we will face less GDPR breaches in the future.
2. **Interoperability:** SSI wallets support interoperability between different credential issuers and verifiers. This means that users can store all their credentials in one place and share them with different parties without having to create a new identity for each party.
3. **Decentralized and secure:** SSI wallets usually use decentralized and secure technologies, such as blockchain, to store and manage identity information. This means that the user has complete control over his/her personal data and can choose to share it only with trusted parties and without the need for intermediaries or centralized databases.
4. **Trust and security:** SSI wallets use digital signatures and cryptographic techniques to ensure the authenticity and integrity of the credentials. This provides a high level of trust and security for the credential issuer and the verifier.
5. **Portable and convenient:** SSI wallets provide a portable and convenient way to manage and use credentials. Users can access their credentials from any device with an internet connection making it easy to share them as soon as needed.

[^1]: European commission (2022) Official Legal Text, General Data Protection Regulation (GDPR). Available at: [https://gdpr-info.eu/](https://gdpr-info.eu/) (Accessed: 12 August 2023).
