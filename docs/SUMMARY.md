# Table of contents

* [SSI/VC Edu Wallet Demo Application](README.md)
* [Introduction](introduction.md)
* [Project description](project-description.md)
* [Research and challenges](research-and-challenges.md)
* [System architecture](system-architecture.md)
* [Project workflow](project-workflow.md)
* [Course of the work packages](course-of-the-work-packages.md)
* [List of final project results](list-of-final-project-results.md)
* [Application of the project results in practice](application-of-the-project-results-in-practice.md)
* [Public Relations/ Networking](public-relations-networking.md)
* [Own project website](own-project-website.md)
* [Planned activities after the end of the project](planned-activities-after-the-end-of-the-project.md)
* [Suggestions for further developments by third parties](suggestions-for-further-developments-by-third-parties.md)
* [Documental sources](documental-sources.md)
