#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

#apt-get update -yqq
#apt-get install php5.6-dev php-pear -yqq

#pecl channel-update pecl.php.net
#pecl install xdebug-2.5.5
#docker-php-ext-enable xdebug

/etc/init.d/memcached start
