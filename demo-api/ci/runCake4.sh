#!/bin/bash
export $(cat nginx-dev.env | xargs)
if [ $(date +%u) -eq $1 ];
then
  echo "run test coverage";
  # save processing time, run coverage only once per week
  /usr/bin/php app_rest/vendor/phpunit/phpunit/phpunit --configuration app_rest/phpunit.xml.dist --coverage-html coverageCi --coverage-text --colors=never;
else
  echo "DONT run test coverage";
  /usr/bin/php app_rest/vendor/phpunit/phpunit/phpunit --configuration app_rest/phpunit.xml.dist;
fi
