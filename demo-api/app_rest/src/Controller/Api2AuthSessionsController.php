<?php
declare(strict_types=1);

namespace App\Controller;

use App\Lib\Helpers\CookieHelper;
use App\Lib\Oauth\OauthClient;
use App\Model\Table\OauthAccessTokensTable;
use App\Model\Table\UsersTable;
use Cake\Controller\ComponentRegistry;
use Cake\Event\EventManagerInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use OAuth2\ResponseType\AccessToken;

/**
 * @property UsersTable $Users
 * @property OauthAccessTokensTable $OauthAccessTokens
 */
class Api2AuthSessionsController extends Api2Controller
{
    /**
     * @var OauthClient
     */
    public $OauthClient;

    public function __construct(?ServerRequest $request = null, ?Response $response = null, ?string $name = null, ?EventManagerInterface $eventManager = null, ?ComponentRegistry $components = null)
    {
        $this->OauthClient = new OauthClient();
        parent::__construct($request, $response, $name, $eventManager, $components);
    }

    public function initialize(): void
    {
        $this->Users = UsersTable::load();
        $this->OauthAccessTokens = OauthAccessTokensTable::load();
        parent::initialize();
    }

    public function isPublicController(): bool
    {
        return true;
    }

    protected function getMandatoryParams(): array
    {
        return [];
    }

    private function _getConfig(): array
    {
        $config = env('OAUTH_CLIENT_CONFIG', '{}');
        if (substr($config, 0, 1) === "'") {
            $config = substr($config, 1, strlen($config) - 2);
        }
        $res = json_decode($config, true);
        if ($res['auth2_url'] ?? '') {
            $res['authorize_url'] = $res['auth2_url'] . '/auth';
            $res['token_url'] = $res['auth2_url'] . '/token';
        }
        return $res;
    }

    protected function getList()
    {
        $config = $this->_getConfig();
        $query = $this->request->getQueryParams();
        if (isset($query['requestLogin'])) {
            $params = [
                'response_type' => 'code',
                'client_id' => $config['client_id'],
                'redirect_uri' => $config['redirect_uri'],
                'scope' => $config['scope'] ?? 'openid email profile offline_access',
                'state' => $this->_storeStateLocally(),
            ];
            $this->redirect($config['authorize_url'] . '?' . http_build_query($params));
        } else {
            if (!isset($query['code']) || !isset($query['state'])) {
                throw new BadRequestException('code and state params are mandatory '.json_encode($query));
            }
            if ($query['state'] !== $this->_retrieveStateStoredLocally()) {
                throw new BadRequestException('invalid state stored');
            }
            $res = $this->OauthClient->requestAccessToken($query['code'], $config);
            $jwt = $res['id_token'];
            $encodedPayload = explode('.', $jwt)[1];
            $payload = json_decode(base64_decode($encodedPayload), true);

            $uid = $this->Users->registerBySub($payload);
            $res = $this->_storeLocalAccessToken($uid, 5185);
            $cookie = (new CookieHelper())->writeApi2Remember($res['access_token'], $res['expires_in']);
            $this->response = $this->response->withCookie($cookie);
            $this->redirect('/assess/de/edit/main');
        }
    }

    private function _storeLocalAccessToken($uid, $clientId)
    {
        $_accessToken = new AccessToken($this->OauthAccessTokens);
        return $_accessToken->createAccessToken($clientId, $uid);
    }

    private function _storeStateLocally(): string
    {
        $state = 'xkrjgkdlukhkhvusuye3kcr';
        // TODO this should be stored and checked https://auth0.com/docs/secure/attack-protection/state-parameters
        return $state;
    }

    private function _retrieveStateStoredLocally(): string
    {
        $state = 'xkrjgkdlukhkhvusuye3kcr';
        // TODO this should be stored and checked https://auth0.com/docs/secure/attack-protection/state-parameters
        return $state;
    }
}
