<?php

namespace App\Model\Entity;

use App\Lib\Consts\UserGroups;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Http\Exception\InternalErrorException;
use Cake\ORM\Entity;

/**
 * @property string firstname
 * @property string lastname
 * @property string email
 * @property mixed group_id
 * @property mixed $password
 * @property mixed $sub_sso_id
 */
class User extends Entity
{
    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
    }

    protected $_accessible = [
        '*' => false,
        'id' => false,

        'password' => true,
        'email' => true,
        'firstname' => true,
        'lastname' => true,
    ];

    protected $_hidden = [
        'deleted',
        'password',
        'access_level',
    ];

    protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher)->hash($password);
        }
    }

    public function isTrainer(): bool
    {
        return $this->group_id == UserGroups::TRAINER;
    }
}
