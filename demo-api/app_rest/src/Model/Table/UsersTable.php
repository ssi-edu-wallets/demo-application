<?php

namespace App\Model\Table;

use App\Lib\Consts\CacheGrp;
use App\Model\Entity\User;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Cache\Cache;
use Cake\Http\Client;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use Cake\Http\Exception\UnauthorizedException;
use Listing\Model\Table\NotebooksTable;

class UsersTable extends AppTable
{
    public function __construct(array $config = [])
    {
        $this->_table = env('USERS_TABLE', 'users');
        parent::__construct($config);
    }

    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');
    }

    public static function load(): UsersTable
    {
        /** @var UsersTable $table */
        $table = parent::load();
        return $table;
    }

    public function getDependentUserIDs($uID): array
    {
        return []; // $this->AdminUsers->getDependentUserIDs($uID);
    }

    private function _getFirst($uid): User
    {
        return $this->findById($uid)
            ->cache('_getFirst' . $uid, CacheGrp::EXTRALONG)
            ->firstOrFail();
    }

    public function getUserGroup($uid): ?int
    {
        $u = $this->_getFirst($uid);
        return $u->group_id ?? null;
    }

    public function checkLogin(array $data)
    {
        $email = $data['username'] ?? '';
        if (!$email) {
            throw new BadRequestException('Username is required');
        }
        $pass = $data['password'] ?? '';
        if (!$pass) {
            throw new BadRequestException('Password is required');
        }
        /** @var User $usr */
        $usr = $this->find()
            ->where(['email' => $email])
            ->first();
        if (!$usr) {
            throw new UnauthorizedException('User not found ' . $email);
        }
        if (!(new DefaultPasswordHasher)->check($pass, $usr->password)) {
            throw new UnauthorizedException('Invalid password');
        }
        return $usr;
    }

    public function getUserWithNotebooks($id): User
    {
        /** @var User $user */
        $user = $this->find()
            ->where(['id' => $id])
            ->contain('Notebooks')
            ->first();
        return $user;
    }

    public function getUserAccessLevel($uid): ?int
    {
        $u = $this->_getFirst($uid);
        return $u->access_level ?? null;
    }

    public function clearCacheUser($id)
    {
        Cache::delete('_getFirst' . $id, CacheGrp::EXTRALONG);
    }

    public function getUserParent($uid): ?int
    {
        $u = $this->_getFirst($uid);
        return $u->parent_id ?? null;
    }

    public function registerBySub(array $payload): int
    {
        /** @var User $user */
        $user = $this->find()->where(['email' => $payload['email']])->first();
        if ($user) {
            return $user->id;
        }
        $defaultUserTags = [40, 41, 42];
        $defaultNotebook = 1;
        $usr = [
            'sub_sso_id' => $payload['sub'],
            'email' => $payload['email'],
            'firstname' => $payload['given_name'],
            'lastname' => $payload['family_name'],
            'tags' => $defaultUserTags,
        ];
        $domain = 'https://' . $_SERVER['HTTP_HOST'];
        $client = new Client();
        $newUser = $client->post($domain . '/api/v2/public/users', $usr)->getJson();
        $token = $newUser['data']['token'] ?? '';
        if (!$token) {
            throw new InternalErrorException('Create new user request failed ' . json_encode($newUser));
        }
        NotebooksTable::load()->copyAsOwn($defaultNotebook, $newUser['data']['id']);
        return $newUser['data']['id'];
    }
}
