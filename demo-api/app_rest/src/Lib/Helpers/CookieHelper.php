<?php

namespace App\Lib\Helpers;

use Cake\Core\Configure;
use Cake\Http\Cookie\Cookie;
use Cake\Http\ServerRequest;
use Cake\I18n\FrozenTime;
use DateTimeZone;

class CookieHelper
{
    const REMEMBER_NAME_API2 = 'rememberapi2';
    const ENCRIPT_METHOD = 'AES-256-CBC';

    private function _getConfig(): array
    {
        return explode(':', env('COOKIE_ENCRYPT_CONFIG', '::'));
    }

    private function _getCookieName(): string
    {
        return $this->_getConfig()[0];
    }

    private function _getEncryptKey(): string
    {
        return $this->_getConfig()[1];
    }

    private function _getEncryptIv(): string
    {
        return $this->_getConfig()[2];
    }

    public function writeApi2Remember($accessToken, $expires = null)
    {
        $encryptedToken = openssl_encrypt(
            $accessToken,
            self::ENCRIPT_METHOD,
            $this->_getEncryptKey(),
            null,
            $this->_getEncryptIv()
        );
        if (!$expires) {
            $expires = Configure::read('Platform.User.rememberExpires');
        }
        $expirationTime = new FrozenTime("+ $expires seconds", new DateTimeZone('GMT'));
        $key = $this->_getCookieName() . '[' . self::REMEMBER_NAME_API2 . ']';
        return new Cookie($key, $encryptedToken, $expirationTime);
    }

    public function readApi2Remember(ServerRequest $request)
    {
        $token = $request->getCookie($this->_getCookieName() . '.' . self::REMEMBER_NAME_API2);
        return openssl_decrypt(
            $token,
            self::ENCRIPT_METHOD,
            $this->_getEncryptKey(),
            null,
            $this->_getEncryptIv()
        );
    }

    public function popApi2Remember(ServerRequest $request)
    {
        $token = $this->readApi2Remember($request);
        $this->writeApi2Remember(time(), 1);
        return $token;
    }
}
