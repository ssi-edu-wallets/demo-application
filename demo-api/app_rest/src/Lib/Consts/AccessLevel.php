<?php

namespace App\Lib\Consts;

class AccessLevel
{
    const FULL_ACCESS = 1;
    const SME = 2;
    const BABY_MONITOR = 3;
    const SME_STRING = 'sme';
}
