<?php
declare(strict_types=1);

namespace App\Lib\Oauth;

use App\Lib\Consts\AccessLevel;
use App\Lib\Consts\UserGroups;
use App\Lib\Exception\SilentException;
use App\Lib\FullBaseUrl;
use App\Model\Entity\Seller;
use App\Model\Entity\Trainer;
use App\Model\Table\UsersTable;
use Cake\Controller\Controller;
use Cake\Http\Client;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\InternalErrorException;
use Cake\Log\LogTrait;

class OAuthServer
{
    use LogTrait;

    /** @var Client */
    private $_httpSocket;
    private $_uid;
    private $_oauthSetup;

    public function __construct(array $config = [])
    {
        $this->_oauthSetup = new OauthSetup();
        foreach ($config as $key => $value) {
            $this->{'_' . $key} = $value;
        }
    }

    public function setupOauth(Controller $controller)
    {
        $this->_oauthSetup->setupOauth($controller);
    }

    public function getAccessTokenParams($uid, $clientId = null): array
    {
        return $this->_oauthSetup->getAccessTokenParams($uid, $clientId);
    }

    public function authorizeUserData(Controller $controller)
    {
        $userID = $controller->getRequest()->getParam('user_id') ?? false;
        if ($userID !== false && !$this->isUserAllowed($userID)) {
            if (method_exists($controller, 'authorizeUserData')) {
                $controller->authorizeUserData();
            } else {
                $extra = $userID . ' -> ' . json_encode($this->_getDependentUserIDs());
                $this->log('ForbiddenException OAuthServer: ' . $extra, 'error');
                throw new ForbiddenException('Resource not allowed with this token');
            }
        }
    }

    public function isUserAllowed($userID): bool
    {
        $uID = $this->getUserID();
        if ($uID == $userID || $this->isManagerUser()) {
            return true;
        }
        return in_array($userID, $this->_getDependentUserIDs());
    }

    private function _getDependentUserIDs(): array
    {
        return $this->_oauthSetup->getUserModel()->getDependentUserIDs($this->getUserID()) + [$this->getUserID()];
    }

    public function getUserGroup(): ?int
    {
        if (!$this->getUserID()) {
            throw new InternalErrorException('Empty User ID, used? verifyAuthorization()');
        }
        return $this->_oauthSetup->getUserModel()->getUserGroup($this->getUserID());
    }

    public function isManagerUser(): bool
    {
        return in_array($this->getUserGroup(), [UserGroups::ADMIN, UserGroups::MODERATOR]);
    }

    public function isAdminUser(): bool
    {
        return $this->getUserGroup() == UserGroups::ADMIN;
    }

    public function verifyAuthorization()
    {
        $isAuthorized = $this->_oauthSetup->getServer()
            ->verifyResourceRequest($this->_oauthSetup->getRequest(), $this->_oauthSetup->getResponse());
        if (!$isAuthorized) {
            $err = 'Verify authorization error: ' .
                $this->_oauthSetup->getResponse()->getParameter('error_description');
            $code = $this->_oauthSetup->getResponse()->getStatusCode();
            if (($_SERVER['REQUEST_URI'] ?? '') === '/api/v2/me') {
                throw new SilentException($err, $code);
            } else {
                throw new InternalErrorException($err, $code);
            }
        }
        $token = $this->_oauthSetup->getServer()->getAccessTokenData($this->_oauthSetup->getRequest());
        $this->_uid = ($token['user_id'] ?? '') ? $token['user_id'] : $token['client_id'];
        $_SERVER = array_merge(['AUTH_TOKEN_UID' => $this->_uid], $_SERVER);
        return $this->_uid;
    }

    public function getUserID()
    {
        return $this->_uid;
    }

    public function isSellerUser(): bool
    {
        return $this->getUserGroup() == UserGroups::SELLER;
    }

    public function isTrainerUser(): bool
    {
        return $this->getUserGroup() == UserGroups::TRAINER;
    }

    public function isSmeUser(): bool
    {
        return $this->isSellerUser() && $this->getAccessLevel() == AccessLevel::SME;
    }

    public function getAccessLevel(): ?int
    {
        return $this->_getUserModel()->getUserAccessLevel($this->getUserID());

    }

    private function _getUserModel(): UsersTable
    {
        return UsersTable::load();
    }

    public function getUserIdsBySme(string $token, $smeId)
    {
        $host = FullBaseUrl::host();
        $config = ['headers' => ['Authorization' => $token]];
        $endpoint = $host . "/api/v3/sme/$smeId/buyersManagement?ids_only=1";
        $response = $this->_getHttpSocket($config)->get($endpoint);
        if (!$response->isSuccess()) {
            throw new \Exception('Error: Cannot get sme users: '
                . $response->getReasonPhrase());
        }
        return json_decode($response->getStringBody(), true)['data'];
    }

    public function getTrainerParent()
    {
        if ($this->getUserGroup() == UserGroups::TRAINER) {
            return $this->_getUserModel()->getUserParent($this->getUserID());
        }
        return false;
    }

    private function _getHttpSocket($config = []): Client
    {
        if (!$this->_httpSocket) {
            /** @var Client _httpSocket */
            $this->_httpSocket = new Client($config);
        }
        return $this->_httpSocket;
    }
}
