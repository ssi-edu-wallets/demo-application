<?php

namespace App\Lib\Oauth;

use App\Model\Table\OauthAccessTokensTable;
use App\Model\Table\UsersTable;
use Cake\Controller\Controller;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use Cake\I18n\FrozenTime;
use OAuth2\Autoloader;
use OAuth2\Controller\AuthorizeControllerInterface;
use OAuth2\GrantType\AuthorizationCode;
use OAuth2\GrantType\UserCredentials;
use OAuth2\Request;
use OAuth2\Response;
use OAuth2\Server;

class OauthSetup
{
    /** @var Server */
    private $server;
    /** @var Request */
    public $request;
    /** @var Response */
    public $response;
    private $_storage;
    private $_serverConfig = ['enforce_state' => true, 'allow_implicit' => true];

    public function __construct(array $config = [])
    {
        $this->_storage = OauthAccessTokensTable::load();
    }

    public function setupOauth(Controller $controller)
    {
        Autoloader::register();
        // create array of supported grant types
        $grantTypes = [
            'authorization_code' => new AuthorizationCode($this->_storage),
            'user_credentials' => new UserCredentials($this->_storage),// password
        ];

        unset($_GET['access_token']);
        // add the server to the silex "container" so we can use it
        $this->request = Request::createFromGlobals();

        $authorization = $controller->getRequest()->getEnv('HTTP_AUTHORIZATION');
        if ($authorization) {
            $this->request->headers['AUTHORIZATION'] = explode(',', $authorization)[0];
        }
        $this->response = new Response();
        $allowOrigin = $controller->getResponse()->getHeader('Access-Control-Allow-Origin');
        if (isset($allowOrigin[0])) {
            $this->response->setHttpHeader('Access-Control-Allow-Origin', $allowOrigin[0]);
        }

        // instantiate the oauth server
        $this->server = new Server($this->_storage, $this->_serverConfig, $grantTypes);
    }

    public function getServer(): Server
    {
        return $this->server;
    }
    public function getRequest(): Request
    {
        return $this->request;
    }

    public function getResponse(): Response
    {
        return $this->response;
    }

    public function getUserModel(): UsersTable
    {
        /** @var UsersTable $table */
        $table = $this->_storage->Users->getTarget();
        return $table;
    }

    public function getAccessTokenParams($uid, $clientId = null): array
    {
        //Used for login
        $time = new FrozenTime();
        $authToken = $this->_getAuthorizationTokenWithoutResponse($uid, $clientId);
        $response = $this->_getAccessTokenForDashboard($authToken, $uid, $clientId);
        $response = self::_convert302to200($response);
        $params = $response->getParameters();
        $expiresIn = $this->response->getParameter('expires_in');
        $params['expires_at'] = $time->addSeconds($expiresIn ?? 0);
        return $params;
    }

    private static function _convert302to200(\OAuth2\Response $response)
    {
        if ($response->getStatusCode() === 302) {
            $headers = $response->getHttpHeaders();
            $location = ($headers['Location'] ?? '');
            if (substr($location, 0, 2) == '/#') {
                $params = [];
                parse_str(substr($location, 2), $params);
                return new \OAuth2\Response($params, 200);
            }
        }
        return $response;
    }

    private function _getAuthorizationTokenWithoutResponse($uid, $clientId = null)
    {
        $response = $this->_getAuthorizationToken($uid, $clientId);
        if ($response->getStatusCode() != 302 || !$response->getHttpHeader('Location')) {
            throw new BadRequestException(
                ($response->getParameters()['error'] ?? 'Error with access token'),
                $response->getStatusCode()
            );
        }
        parse_str(parse_url($response->getHttpHeader('Location'))['query'], $params);
        return $params['code'];
    }

    private function _getAccessTokenForDashboard($authToken, $uid, $clientId)
    {
        $responseType = AuthorizeControllerInterface::RESPONSE_TYPE_ACCESS_TOKEN;
        $this->request->query['access_token'] = $authToken;
        $this->request->query['user_id'] = $uid;
        return $this->_getAuthorizedAccessToken($clientId, $responseType, true, $uid);
    }

    private function _getAuthorizedAccessToken($clientId, $responseType, $isAuthorized, $uid = null)
    {
        $this->request->query['client_id'] = $clientId;
        $this->request->query['redirect_uri'] = '/';
        $this->request->query['response_type'] = $responseType;
        $this->request->query['state'] = $responseType;
        $this->server->setConfig('use_jwt_access_tokens', false);
        $this->response = $this->server->handleAuthorizeRequest(
            $this->request, $this->response, $isAuthorized, $uid
        );
        return $this->response;
    }

    private function _getAuthorizationToken($uid, $clientId): \OAuth2\ResponseInterface
    {
        $type = AuthorizeControllerInterface::RESPONSE_TYPE_AUTHORIZATION_CODE;
        if (!$this->request) {
            throw new InternalErrorException('setupOauth() is required');
        }
        $this->request->query['client_id'] = $clientId;
        $this->request->query['redirect_uri'] = '/';
        $this->request->query['response_type'] = $type;
        $this->request->query['state'] = $type;
        $this->server->setConfig('use_jwt_access_tokens', false);
        $this->response = $this->server
            ->handleAuthorizeRequest($this->request, $this->response, true, $uid);
        return $this->response;
    }

}
