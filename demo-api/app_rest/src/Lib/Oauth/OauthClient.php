<?php
declare(strict_types=1);

namespace App\Lib\Oauth;

use App\Lib\Exception\DetailedException;
use Cake\Http\Client;
use Cake\Http\Exception\UnauthorizedException;

class OauthClient
{
    public function requestAccessToken(string $code, array $config): array
    {
        $tokenUrl = $config['token_url'];
        $params = [
            'grant_type' => 'authorization_code',
            'code' => $code,
            'client_id' => $config['client_id'],
            'client_secret' => $config['secret'],
            'redirect_uri' => $config['redirect_uri'],
        ];
        $res = $this->_httpPost($tokenUrl, $params);
        if ($res->isOk()) {
            return $res->getJson();
        } else {
            $res = $res->getJson();
            if ($res['error'] == 'invalid_grant') {
                throw new UnauthorizedException($res['error_description']);
            } else {
                throw new DetailedException('Auth error: ' . json_encode($res));
            }
        }
    }

    private function _httpPost($tokenUrl, $params): Client\Response
    {
        $client = new Client();
        return $client->post($tokenUrl, $params);
    }
}
