<?php
declare(strict_types=1);

namespace Wallet\Test\TestCase\Lib\VCs;

use App\Lib\Exception\DetailedException;
use Cake\Http\ServerRequest;
use PHPUnit\Framework\TestCase;
use Wallet\Lib\VerifierApiClient;

class VerifierApiClientTest extends TestCase
{
    public function testGetAccessToken()
    {
        $api = new VerifierApiClient(new ServerRequest());
        $url = 'https://verifier.walt.id/success/?access_token=LwS0ySajT42oCNEM4vh87Q';
        $expected = 'LwS0ySajT42oCNEM4vh87Q';
        $this->assertEquals($expected, $api->getAccessToken($url));
    }
    public function testGetAccessToken_exception()
    {
        $api = new VerifierApiClient(new ServerRequest());
        $url = 'https://verifier.walt.id/success/?hello=world';
        try {
            $api->getAccessToken($url);
        } catch (DetailedException $e) {
            $this->assertEquals('Redirection without access_token', $e->getMessage());
        }
    }
}
