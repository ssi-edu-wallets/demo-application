<?php
declare(strict_types=1);

namespace Wallet\Test\TestCase\Lib\VCs;

use Wallet\Lib\VCs\VcUserLearningOutcomes;
use PHPUnit\Framework\TestCase;

class VcUserLearningOutcomesTest extends TestCase
{
    public function testToArray()
    {
        $domain = 'https://test.example.com';
        $bookings = [
            'data' => [
                (int) 0 => [
                    'id' => (int) 170,
                    'reference' => '4-1686843527308',
                    'course_id' => null,
                    'user_id' => (int) 3,
                    'original_buyer_id' => (int) 3,
                    'u_group' => null,
                    'completed' => null,
                    'created' => '2023-06-15T17:38:47+02:00',
                    'subscription' => null,
                    'ticket_ref' => '4-1686843527308-00012-170',
                    'ticket_name' => 'Usr Test',
                    'progress' => (int) 0,
                    'service_id' => (int) 2580,
                    'service' => [
                        'id' => (int) 2580,
                        'title' => 'Product test',
                        'event_pic_url' => 'https://cdn.imgix.net/e/img/2580-product-test-event_pic.png?v=2&auto=compress&w=1920&h=606&fit=crop',
                        'shape' => (int) 3,
                        'type' => (int) 25,
                        'tag_list' => [
                            [
                                'id' => 42,
                                'tag' => 'IT Projektmanager IT Projektmanagerin',
                                'display_name' => 'IT-Projektmanager/IT-Projektmanagerin',
                                'is_module' => true,
                                'is_learning_path' => false,
                            ]
                        ],
                        'language_id' => 'de',
                        '_links' => [
                            'self' => [
                                'href' => '/api/v2/services/2580?private=ba4a47847a'
                            ],
                            'canonical' => [
                                'href' => $domain . '/de/e/product-test-2580/ba4a47847a',
                                'name' => 'product-test-2580',
                                'private' => 'ba4a47847a'
                            ]
                        ]
                    ],
                    'status' => [
                        'id' => (int) 1,
                        'group' => 'enrolled',
                        'description' => 'Ihre Buchung war erfolgreich'
                    ],
                    'course_title' => null,
                    'started' => null
                ],
                (int) 1 => [
                    'id' => (int) 169,
                    'reference' => '4-1686843468337',
                    'course_id' => null,
                    'user_id' => (int) 3,
                    'original_buyer_id' => (int) 3,
                    'u_group' => null,
                    'completed' => null,
                    'created' => '2023-06-15T17:37:48+02:00',
                    'subscription' => null,
                    'ticket_ref' => '4-1686843468337-00011-169',
                    'ticket_name' => 'Usr Test',
                    'progress' => (int) 0,
                    'service_id' => (int) 2580,
                    'service' => [
                        'id' => (int) 2580,
                        'title' => 'Product test',
                        'event_pic_url' => 'https://cdn.imgix.net/e/img/2580-product-test-event_pic.png?v=2&auto=compress&w=1920&h=606&fit=crop',
                        'shape' => (int) 3,
                        'type' => (int) 25,
                        'language_id' => 'de',
                        '_links' => [
                            'self' => [
                                'href' => $domain . '/api/v2/services/2580?private=ba4a47847a'
                            ],
                            'canonical' => [
                                'href' => $domain . '/de/e/product-test-2580/ba4a47847a',
                                'name' => 'product-test-2580',
                                'private' => 'ba4a47847a'
                            ]
                        ]
                    ],
                    'status' => [
                        'id' => (int) 1,
                        'group' => 'enrolled',
                        'description' => 'Ihre Buchung war erfolgreich'
                    ],
                    'course_title' => null,
                    'started' => null
                ],
                (int) 2 => [
                    'id' => (int) 168,
                    'reference' => '4-1686839525170',
                    'course_id' => null,
                    'user_id' => (int) 3,
                    'original_buyer_id' => (int) 3,
                    'u_group' => null,
                    'completed' => null,
                    'created' => '2023-06-15T16:32:05+02:00',
                    'subscription' => null,
                    'ticket_ref' => '4-1686839525170-00010-168',
                    'ticket_name' => 'Usr Test',
                    'progress' => (int) 0,
                    'service_id' => (int) 2580,
                    'service' => [
                        'id' => (int) 2580,
                        'title' => 'Product test',
                        'event_pic_url' => 'https://cdn.imgix.net/e/img/2580-product-test-event_pic.png?v=2&auto=compress&w=1920&h=606&fit=crop',
                        'shape' => (int) 3,
                        'type' => (int) 25,
                        'tag_list' => [
                            [
                                'id' => 42,
                                'tag' => 'IT Projektmanager IT Projektmanagerin',
                                'display_name' => 'IT-Projektmanager/IT-Projektmanagerin',
                                'is_module' => true,
                                'is_learning_path' => false,
                                'esco_uri' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525'
                            ]
                        ],
                        'language_id' => 'de',
                        '_links' => [
                            'self' => [
                                'href' => $domain . '/api/v2/services/2580?private=ba4a47847a'
                            ],
                            'canonical' => [
                                'href' => $domain . '/de/e/product-test-2580/ba4a47847a',
                                'name' => 'product-test-2580',
                                'private' => 'ba4a47847a'
                            ]
                        ]
                    ],
                    'status' => [
                        'id' => (int) 1,
                        'group' => 'enrolled',
                        'description' => 'Ihre Buchung war erfolgreich'
                    ],
                    'course_title' => null,
                    'started' => null
                ],
            ]
        ];
        $expectedVc = [
            'credentials' => [
                [
                    'credentialData' => [
                        'credentialSubject' => [
                            'id' => '',
                            'title' => 'User learning outcomes',
                            'performed' => [
                                [
                                    'title' => 'Product test',
                                    'specifiedBy' => [
                                        'teaches' => [
                                            [
                                                'learningOutcome' => [
                                                    'name' => 'IT-Projektmanager/IT-Projektmanagerin'
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                [
                                    'title' => 'Product test'
                                ],
                                [
                                    'title' => 'Product test',
                                    'specifiedBy' => [
                                        'teaches' => [
                                            [
                                                'learningOutcome' => [
                                                    'name' => 'IT-Projektmanager/IT-Projektmanagerin',
                                                    'relatedESCOSkill' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525'
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'type' => 'UserLearningOutcomes'
                ]
            ]
        ];
        $learningOutcomesClass = new VcUserLearningOutcomes();
        $vc = $learningOutcomesClass->setBookings($bookings['data'])->toArray();
        $this->assertEquals($expectedVc, $vc);
    }
}
