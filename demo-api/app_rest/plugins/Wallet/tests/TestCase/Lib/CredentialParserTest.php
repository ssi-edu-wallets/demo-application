<?php
declare(strict_types=1);

namespace Wallet\Test\TestCase\Lib\VCs;

use App\Lib\FullBaseUrl;
use Cake\TestSuite\TestCase;
use Profile\Model\Entity\UserProfile;
use Profile\Model\Table\UserProfilesTable;
use Profile\Test\Fixture\ProfilesSkillsFixture;
use Profile\Test\Fixture\UserProfilesFixture;
use Wallet\Lib\CredentialParser;

class CredentialParserTest extends TestCase
{
    protected $fixtures = [
        ProfilesSkillsFixture::LOAD, UserProfilesFixture::LOAD,
    ];

    public function testRedirectUrl()
    {
        $parser = new CredentialParser($this->_getCredential());

        $userId = UserProfilesFixture::USER_ID;
        $expected = $this->_getCredential()['vps'];
        $this->assertEquals($expected, $parser->store($userId));

        /** @var UserProfile $profile */
        $profile = UserProfilesTable::load()->findUserProfileByUserId($userId)->firstOrFail();
        $goal = 'Occupation goal 1' . "\n\n" . 'I have participated in KI-Kompass 4 KMU, Product test, Scrum im Überblick (powered by GPT), Gruppen Test.';
        $this->assertEquals($goal, $profile->goal_description);
        $skills = [
            [
                'id' => 3,
                'skill' => 'Spacial vision',
                'esco_uri' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525',
                'status' => 'want'
            ],
            [
                'id' => 4,
                'skill' => 'Logic',
                'esco_uri' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525',
                'status' => 'have'
            ],
            [
                'id' => 5,
                'skill' => 'IT-Projektmanager/IT-Projektmanagerin',
                'esco_uri' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525',
                'status' => 'have'
            ],
        ];
        $this->assertEquals($skills, json_decode(json_encode($profile->getProfilesSkills()), true));
    }

    private function _getCredential()
    {
        $domain = 'test.example.com';
        return [
            'auth_token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkaWQ6a2V5Ono2TWtyVmF0RTFvN1RhQTVVdFZrR2h5QjRRVVlkOGRUOVpSWE40cnJXWm85OGdabiJ9.FV9CZ43jEzrjqFW1Rj5YeU7Iw74CBQl1Cria04Pp7P8',
            'isValid' => true,
            'state' => 'LwS0ySajT42oCNEM4vh87Q',
            'subject' => 'did:key:z6MkrVatE1o7TaA5UtVkGhyB4QUYd8dT9ZRXN4rrWZo98gZn',
            'vps' => [
                (int) 0 => [
                    'vcs' => [
                        [
                            'type' => [
                                (int) 0 => 'VerifiableCredential',
                                (int) 1 => 'VerifiableAttestation',
                                (int) 2 => 'VerifiableUserLearningOutcomes'
                            ],
                            '@context' => [
                                (int) 0 => 'https://www.w3.org/2018/credentials/v1'
                            ],
                            'id' => 'urn:uuid:507f62fa-633a-430a-8360-4c7204abf72c',
                            'issuer' => 'did:key:z6Mkms43Q3k9i19mMMyfu7kj3MAHFwKNfXiofDiWD7b7d8Rf',
                            'issuanceDate' => '2023-07-04T00:39:48Z',
                            'issued' => '2023-07-04T00:39:48Z',
                            'validFrom' => '2023-07-04T00:39:48Z',
                            'credentialSchema' => [
                                'id' => 'https://'.$domain.'/edu/api/v1/wallet/credentialSchemas/3',
                                'type' => 'JsonSchemaValidator2018'
                            ],
                            'credentialSubject' => [
                                'id' => 'did:key:z6MkkUbpY7jszx81MnE4Rx7fhdVU3WqD7HmkJgYUKrXDKU8a',
                                'title' => 'User learning outcomes',
                                'performed' => [
                                    [
                                        'title' => 'KI-Kompass 4 KMU',
                                        'startedAtTime' => '2023-07-03T15:50:30+02:00',
                                        'endedAtTime' => '2023-07-03T16:39:47+02:00'
                                    ],
                                    [
                                        'title' => 'Product test'
                                    ],
                                    [
                                        'title' => 'Scrum im Überblick (powered by GPT)',
                                        'startedAtTime' => '2023-05-23T09:39:24+02:00',
                                        'specifiedBy' => [
                                            'teaches' => [
                                                [
                                                    'learningOutcome' => [
                                                        'name' => 'IT-Projektmanager/IT-Projektmanagerin',
                                                        'relatedESCOSkill' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525'
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ],
                                    [
                                        'title' => 'Gruppen Test',
                                        'specifiedBy' => [
                                            'teaches' => [
                                                [
                                                    'learningOutcome' => [
                                                        'name' => 'IT-Projektmanager/IT-Projektmanagerin',
                                                        'relatedESCOSkill' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525'
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ],
                                ]
                            ]
                        ]
                    ],
                    'verification_result' => [
                        'policyResults' => [
                            'SignaturePolicy' => [
                                'isSuccess' => true
                            ],
                            'ChallengePolicy' => [
                                'isSuccess' => true
                            ],
                            'PresentationDefinitionPolicy' => [
                                'isSuccess' => true
                            ]
                        ],
                        'result' => true,
                        'valid' => true
                    ],
                    'vp' => [
                        'type' => [
                            (int) 0 => 'VerifiablePresentation'
                        ],
                        '@context' => [
                            (int) 0 => 'https://www.w3.org/2018/credentials/v1'
                        ],
                        'id' => 'urn:uuid:91e5f485-25ed-4b8a-aec9-293a59b46c9e',
                        'holder' => 'did:key:z6MkrVatE1o7TaA5UtVkGhyB4QUYd8dT9ZRXN4rrWZo98gZn',
                        'verifiableCredential' => [
                            (int) 0 => 'eyJraWQiOiJkaWQ6a2V5Ono2TWtod3BrOE1Ea01VUnhNZExZanJBcGZZRmI2dXpEa2JoNk03bWRpYUNydlNLdSN6Nk1raHdwazhNRGtNVVJ4TWRMWWpyQXBmWUZiNnV6RGtiaDZNN21kaWFDcnZTS3UiLCJ0eXAiOiJKV1QiLCJhbGciOiJFZERTQSJ9.eyJpc3MiOiJkaWQ6a2V5Ono2TWtod3BrOE1Ea01VUnhNZExZanJBcGZZRmI2dXpEa2JoNk03bWRpYUNydlNLdSIsInN1YiI6ImRpZDprZXk6ejZNa3JWYXRFMW83VGFBNVV0VmtHaHlCNFFVWWQ4ZFQ5WlJYTjRycldabzk4Z1puIiwibmJmIjoxNjg4NTY5MzYwLCJpYXQiOjE2ODg1NjkzNjAsInZjIjp7InR5cGUiOlsiVmVyaWZpYWJsZUNyZWRlbnRpYWwiLCJWZXJpZmlhYmxlQXR0ZXN0YXRpb24iLCJWZXJpZmlhYmxlSWQiXSwiQGNvbnRleHQiOlsiaHR0cHM6Ly93d3cudzMub3JnLzIwMTgvY3JlZGVudGlhbHMvdjEiXSwiaWQiOiJ1cm46dXVpZDowZTI5OTZlMC1kNTRiLTRiZDMtYjQ4NS05NTMwZTczNjI0NWYiLCJpc3N1ZXIiOiJkaWQ6a2V5Ono2TWtod3BrOE1Ea01VUnhNZExZanJBcGZZRmI2dXpEa2JoNk03bWRpYUNydlNLdSIsImlzc3VhbmNlRGF0ZSI6IjIwMjMtMDctMDVUMTU6MDI6NDBaIiwiaXNzdWVkIjoiMjAyMy0wNy0wNVQxNTowMjo0MFoiLCJ2YWxpZEZyb20iOiIyMDIzLTA3LTA1VDE1OjAyOjQwWiIsImNyZWRlbnRpYWxTY2hlbWEiOnsiaWQiOiJodHRwczovL3Jhdy5naXRodWJ1c2VyY29udGVudC5jb20vd2FsdC1pZC93YWx0aWQtc3Npa2l0LXZjbGliL21hc3Rlci9zcmMvdGVzdC9yZXNvdXJjZXMvc2NoZW1hcy9WZXJpZmlhYmxlSWQuanNvbiIsInR5cGUiOiJGdWxsSnNvblNjaGVtYVZhbGlkYXRvcjIwMjEifSwiY3JlZGVudGlhbFN1YmplY3QiOnsiaWQiOiJkaWQ6a2V5Ono2TWtyVmF0RTFvN1RhQTVVdFZrR2h5QjRRVVlkOGRUOVpSWE40cnJXWm85OGdabiIsImN1cnJlbnRBZGRyZXNzIjpbIjEgQm91bGV2YXJkIGRlIGxhIExpYmVydMOpLCA1OTgwMCBMaWxsZSJdLCJkYXRlT2ZCaXJ0aCI6IjE5OTMtMDQtMDgiLCJmYW1pbHlOYW1lIjoiRE9FIiwiZmlyc3ROYW1lIjoiSmFuZSIsImdlbmRlciI6IkZFTUFMRSIsIm5hbWVBbmRGYW1pbHlOYW1lQXRCaXJ0aCI6IkphbmUgRE9FIiwicGVyc29uYWxJZGVudGlmaWVyIjoiMDkwNDAwODA4NEgiLCJwbGFjZU9mQmlydGgiOiJMSUxMRSwgRlJBTkNFIn0sImV2aWRlbmNlIjpbeyJkb2N1bWVudFByZXNlbmNlIjpbIlBoeXNpY2FsIl0sImV2aWRlbmNlRG9jdW1lbnQiOlsiUGFzc3BvcnQiXSwic3ViamVjdFByZXNlbmNlIjoiUGh5c2ljYWwiLCJ0eXBlIjpbIkRvY3VtZW50VmVyaWZpY2F0aW9uIl0sInZlcmlmaWVyIjoiZGlkOmVic2k6MkE5Qlo5U1VlNkJhdGFjU3B2czFWNUNkakh2THBRN2JFc2kySmI2TGRIS25ReGFOIn1dfSwianRpIjoidXJuOnV1aWQ6MGUyOTk2ZTAtZDU0Yi00YmQzLWI0ODUtOTUzMGU3MzYyNDVmIn0.UBSeMHtD9omENkw28gmsir4tFWuYN9X2ZFnew-uI89FquEPuvrIAmxFtd2t45pDGt7yZagCZw749MArCnMTtAg~'
                        ]
                    ]
                ]
            ]
        ];
    }
}
