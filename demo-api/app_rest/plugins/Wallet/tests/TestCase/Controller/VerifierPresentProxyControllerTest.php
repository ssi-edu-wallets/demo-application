<?php
declare(strict_types=1);

namespace Wallet\Test\TestCase\Controller;

use Wallet\WalletPlugin;
use App\Test\TestCase\Controller\Api2CommonErrorsTest;

class VerifierPresentProxyControllerTest extends Api2CommonErrorsTest
{
    protected $fixtures = [
    ];

    protected function _getEndpoint(): string
    {
        return WalletPlugin::ROUTE_PREFIX . '/verifier/present/';
    }

    public function testGetList()
    {
        $this->get($this->_getEndpoint() . '?walletId=walt.id&vcType=VerifiableId');
        $this->assertRedirectContains('https://wallet.walt.id/api/siop/initiatePresentation');
    }
}
