<?php
declare(strict_types=1);

namespace Wallet\Test\TestCase\Controller;

use App\Test\Fixture\OauthAccessTokensFixture;
use Wallet\WalletPlugin;
use App\Test\TestCase\Controller\Api2CommonErrorsTest;
use App\Test\Fixture\UsersFixture;

class WalletCredentialSchemaControllerTest extends Api2CommonErrorsTest
{
    protected $fixtures = [
        UsersFixture::LOAD, OauthAccessTokensFixture::LOAD
    ];
    protected function _getEndpoint(): string
    {
        return WalletPlugin::ROUTE_PREFIX . '/credentialSchemas/';
    }

    public function testGetUserLearningOutcomesSchema()
    {
        $this->get($this->_getEndpoint().'3');

        $bodyDecoded = $this->assertJsonResponseOK();
        $expected = [
            '$schema' => 'https://json-schema.org/draft/2020-12/schema',
            'title' => 'User learning outcomes verifiable accreditation',
            'description' => 'Schema of a user learning outcomes verifiable accreditation',
            'type' => 'object',
            'allOf' => [
                (int) 0 => [
                    '$ref' => 'https://api-pilot.ebsi.eu/trusted-schemas-registry/v2/schemas/0xeb6d8131264327f3cbc5ddba9c69cb9afd34732b3b787e4b3e3507a25d3079e9'
                ],
                (int) 1 => [
                    'properties' => [
                        'credentialSubject' => [
                            'description' => 'Defines additional properties on credentialSubject to describe the body of the verifiable credential',
                            'type' => 'object',
                            'properties' => [
                                'id' => [
                                    'description' => 'Defines the did of the credential subject',
                                    'type' => 'string'
                                ],
                                'title' => [
                                    'description' => 'Title of the credential subject',
                                    'type' => 'string'
                                ],
                                'performed' => [
                                    'description' => 'Defines the learning activity that a person participated in or attended',
                                    'type' => 'array',
                                    'items' => [
                                        '$ref' => '#/$defs/performed'
                                    ]
                                ]
                            ],
                            'required' => [
                                (int) 0 => 'id',
                                (int) 1 => 'title',
                                (int) 2 => 'performed'
                            ]
                        ]
                    ]
                ]
            ],
            '$defs' => [
                'performed' => [
                    'description' => 'Defines the learning activity that a person participated in or attended',
                    'type' => 'object',
                    'properties' => [
                        'title' => [
                            'description' => 'Defines a title of the learning achievement',
                            'type' => 'string'
                        ],
                        'startedAtTime' => [
                            'description' => 'The date the learner started the activity',
                            'type' => 'DateTime'
                        ],
                        'endedAtTime' => [
                            'description' => 'The date the learner ended the activity',
                            'type' => 'DateTime'
                        ],
                        'specifiedBy' => [
                            'definition' => 'The specification of this learning activity',
                            'type' => 'object',
                            'properties' => [
                                'teaches' => [
                                    'definition' => 'The expected learning outcomes this learning activity specification can lead or contribute to',
                                    'type' => 'array',
                                    'items' => [
                                        '$ref' => '#/$defs/teaches'
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'required' => [
                        (int) 0 => 'title'
                    ]
                ],
                'teaches' => [
                    'definition' => 'The expected learning outcomes this learning activity specification can lead or contribute to',
                    'type' => 'object',
                    'properties' => [
                        'learningOutcome' => [
                            'description' => 'The learning outcome of the learning specification',
                            'type' => 'object',
                            'properties' => [
                                'name' => [
                                    'description' => 'A legible, descriptive name for the learning outcome',
                                    'type' => 'string'
                                ],
                                'relatedESCOSkill' => [
                                    'description' => 'A URI to the related ESCO Skill',
                                    'type' => 'string'
                                ]
                            ],
                            'required' => [
                                (int) 0 => 'name'
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $this->assertEquals($expected, $bodyDecoded['data']);
    }
    public function testGetSchema_shouldThrowWithInvalidId()
    {
        $this->get($this->_getEndpoint().'7');
        $bodyDecoded = $this->assertException('Bad Request', 400, 'Invalid schema id');

        $this->assertEquals('Invalid schema id', $bodyDecoded['message']);
    }
}
