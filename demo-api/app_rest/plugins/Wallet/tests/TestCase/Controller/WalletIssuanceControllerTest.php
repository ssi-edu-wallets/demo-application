<?php
declare(strict_types=1);

namespace Wallet\Test\TestCase\Controller;

use Assessment\Test\Fixture\InitiativesFixture;
use Assessment\Test\Fixture\InitiativesTagsFixture;
use Assessment\Test\Fixture\InitiativesUsersFixture;
use Assessment\Test\Fixture\QuestionnairesFixture;
use Assessment\Test\Fixture\SubmissionsFixture;
use Wallet\Controller\WalletIssuanceController;
use Wallet\WalletPlugin;
use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use App\Test\TestCase\Controller\Api2CommonErrorsTest;

class WalletIssuanceControllerTest extends Api2CommonErrorsTest
{
    protected $fixtures = [
        UsersFixture::LOAD, OauthAccessTokensFixture::LOAD,
        SubmissionsFixture::LOAD,
        InitiativesUsersFixture::LOAD,
        InitiativesFixture::LOAD,
        InitiativesTagsFixture::LOAD,
        QuestionnairesFixture::LOAD,
    ];

    protected function _getEndpoint(): string
    {
        return WalletPlugin::ROUTE_PREFIX . '/users/' . UsersFixture::BUYER_ID . '/issuance/';
    }

    public function testGetList()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $this->get($this->_getEndpoint());

        $bodyDecoded = $this->assertJsonResponseOK();
        $expected = [
            [
                'id' => WalletIssuanceController::ASSESSMENT_SCORES,
                'name' => 'Onboarding questions',
                'type' => 'Europass',
                'description' => 'Europass Onboarding questions from 3/1/22, 11:00 AM',
            ],
            [
                'id' => WalletIssuanceController::GREAT,
                'name' => 'Great tutor',
                'type' => 'ProofOfResidence',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
            ],
            [
                'id' => WalletIssuanceController::HISTORY,
                'name' => 'User learning outcome',
                'type' => 'UserLearningOutcome',
                'description' => 'Verifiable credential of an user learning outcome',
            ]
        ];
        $this->assertEquals($expected, $bodyDecoded['data']);
    }

    public function testAddNew_VcEuropassWithoutSessionId()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'isPreAuthorized' => true,
            'vc_id' => WalletIssuanceController::ASSESSMENT_SCORES,
            'xDevice'=> false
        ];
        $this->post($this->_getEndpoint(), $data);
        $body = $this->assertJsonResponseOK();
        $this->assertEquals(['redirect_uri'], array_keys($body['data']));
        $starts = 'https://wallet.walt.id/api/siop/initiateIssuance/?issuer=https%3A%2F%2Fissuer.walt.id%2Fissuer-api%2Fdefault%2Foidc%2F&credential_type=Europass&pre-authorized_code=';
        $this->assertStringStartsWith($starts, $body['data']['redirect_uri']);
    }
    public function testAddNew_VcProofOfResidenceWithSessionId()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'session_id' => '23k4-234-jk234-234234',
            'vc_id' => WalletIssuanceController::GREAT,
        ];

        $this->post($this->_getEndpoint(), $data);
        $body = $this->assertJsonResponseOK();
        $this->assertStringContainsString('credential_type=ProofOfResidence', $body['data']['redirect_uri']);
    }

    public function testAddNew_VcEuropassWithSessionId()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'session_id' => '23k4-234-jk234-234234',
            'vc_id' => WalletIssuanceController::ASSESSMENT_SCORES
        ];

        $this->post($this->_getEndpoint(), $data);

        $body = $this->assertJsonResponseOK();
        $this->assertEquals(['redirect_uri'], array_keys($body['data']));
        $starts = 'openid-initiate-issuance://?issuer=https%3A%2F%2Fissuer.walt.id%2Fissuer-api%2Fdefault%2Foidc%2F&credential_type=Europass&user_pin_required=false&op_state=';
        $this->assertStringStartsWith($starts, $body['data']['redirect_uri']);
    }

    public function testAddNew_shouldThrowWithInvalidId()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'session_id' => '23k4-234-jk234-234234',
            'vc_id' => 4,
        ];

        $this->post($this->_getEndpoint(), $data);

        $this->assertException('Bad Request', 400, 'Invalid vc_id');
    }

    public function testAddNew_shouldThrowBadRequest()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'session_id' => '23k4-234-jk234-234234'
        ];
        $this->post($this->_getEndpoint(), $data);
        $this->assertException('Bad Request', 400, 'Required param vc_id missing');
    }

    public function testAddNew_VcEuropassWithXdeviceFlow()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);
        $data = [
            'isPreAuthorized' => true,
            'vc_id' => WalletIssuanceController::ASSESSMENT_SCORES,
            'xDevice'=> true
        ];

        $this->post($this->_getEndpoint(), $data);

        $body = $this->assertJsonResponseOK();
        $this->assertEquals(['redirect_uri'], array_keys($body['data']));
        $starts = 'openid-initiate-issuance://?issuer=https%3A%2F%2Fissuer.walt.id%2Fissuer-api%2Fdefault%2Foidc%2F&credential_type=Europass&pre-authorized_code=';
        $this->assertStringStartsWith($starts, $body['data']['redirect_uri']);
    }
}
