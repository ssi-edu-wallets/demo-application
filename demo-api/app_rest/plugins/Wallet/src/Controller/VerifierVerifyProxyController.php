<?php
declare(strict_types=1);

namespace Wallet\Controller;

use App\Controller\Api2Controller;
use Wallet\Lib\VerifierApiClient;

class VerifierVerifyProxyController extends Api2Controller
{
    public function isPublicController(): bool
    {
        return true;
    }

    protected function getMandatoryParams(): array
    {
        return [];
    }

    public function addNew($data)
    {
        $api = new VerifierApiClient($this->request);
        $redirect = $api->postVerify($data);
        $this->redirect($redirect);
    }
}
