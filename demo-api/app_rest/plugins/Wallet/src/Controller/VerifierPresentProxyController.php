<?php
declare(strict_types=1);

namespace Wallet\Controller;

use App\Controller\Api2Controller;
use Wallet\Lib\VerifierApiClient;

class VerifierPresentProxyController extends Api2Controller
{
    public function isPublicController(): bool
    {
        return true;
    }

    protected function getMandatoryParams(): array
    {
        return [];
    }

    public function getList()
    {
        $api = new VerifierApiClient($this->request);
        $this->redirect($api->getPresent());
    }
}
