<?php
declare(strict_types=1);

namespace Wallet\Controller;

use App\Controller\Api2Controller;
use Wallet\Lib\CredentialParser;
use Wallet\Lib\VerifierApiClient;

class VerifierStoreController extends Api2Controller
{
    protected function getMandatoryParams(): array
    {
        return [];
    }

    public function addNew($data)
    {
        $api = new VerifierApiClient($this->request);
        $credential = $api->getCredential($data);
        $parser = new CredentialParser($credential);
        $this->return = [
            'vps' => $parser->store($this->OAuthServer->getUserID())
        ];
    }
}
