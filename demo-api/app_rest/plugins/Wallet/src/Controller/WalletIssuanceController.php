<?php
declare(strict_types=1);

namespace Wallet\Controller;

use App\Controller\Api2Controller;
use App\Lib\Exception\DetailedException;
use App\Lib\FullBaseUrl;
use Assessment\Model\Table\SubmissionsTable;
use Cake\Core\Configure;
use Cake\Http\Client;
use Cake\Http\Client\Response;
use Cake\Http\Exception\BadRequestException;
use Wallet\Lib\VCs\VcEuropass;
use Wallet\Lib\VCs\VcProofOfResidence;
use Wallet\Lib\VCs\VcUserLearningOutcomes;


/**
 * @property SubmissionsTable $Submissions
 * @property Client $httpClient
 */
class WalletIssuanceController extends Api2Controller
{
    const ASSESSMENT_SCORES = '1';
    const GREAT = '2';
    const HISTORY = '3';
    const OK_PARAM = 'redirect_uri';

    public function initialize(): void
    {
        parent::initialize();
        $this->httpClient = new Client();
        $this->Submissions = SubmissionsTable::load();
    }

    protected function getMandatoryParams(): array
    {
        return ['user_id'];
    }

    public function getList()
    {
        $user_id = $this->request->getParam('user_id');
        // get real type from https://issuer.walt.id/issuer-api/default/credentials/listIssuables?sessionId=XXX

        $submission = $this->Submissions->getLastVcResults($user_id);
        $this->return = [
            [
                'id' => self::ASSESSMENT_SCORES,
                'name' => $submission->questionnaire->title,
                'type' => 'Europass',
                'description' => 'Europass '.$submission->questionnaire->title.' from '.$submission->created->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::SHORT]),
            ],
            [
                'id' => self::GREAT,
                'name' => 'Great tutor',
                'type' => 'ProofOfResidence',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
            ],
            [
                'id' => self::HISTORY,
                'name' => 'User learning outcome',
                'type' => 'UserLearningOutcome',
                'description' => 'Verifiable credential of an user learning outcome',
            ]
        ];
    }

    private function _createCredential($data, $payload): Response
    {
        $endpoint = env('WALLET_ISSUER_ENDPOINT', '');
        if (!$endpoint) {
            throw new DetailedException('env WALLET_ISSUER_ENDPOINT is not defined', 500);
        }
        if ($data['isPreAuthorized'] ?? '') {
            $xDevice = $data['xDevice'] ?? '';
            if ($xDevice === 'false' || !$xDevice) {
                $xDevice = false;
            }
            $wallet = $xDevice ? 'x-device' : 'walt.id';
            $url = $endpoint . '?walletId=' . $wallet . '&isPreAuthorized=true';
        } else {
            if (!isset($data['session_id'])) {
                throw new BadRequestException('Required param session_id or isPreAuthorized missing');
            }
            $url = $endpoint . '?sessionId=' . $data['session_id'];
        }

        $options = ['headers' => ['Content-Type' => 'application/json']];

        $res = $this->httpClient->post($url, json_encode($payload), $options);
        return $res;
    }

    public function addNew($data)
    {
        $this->return = $this->_addNew($data);
    }

    private function _addNew($data): array
    {
        if (!isset($data['vc_id'])) {
            throw new BadRequestException('Required param vc_id missing');
        }
        $payload = $this->_getCredentialBody($data['vc_id']);
        $toRet = $this->_parseResponse($this->_createCredential($data, $payload));
        if (!isset($toRet[self::OK_PARAM])) {
            $vcType = $payload['credentials'][0]['type'] ?? '';
            if ($toRet['message'] == 'No template found with name: ' . $vcType) {
                $templateRes = $this->_parseResponse($this->_addVcTemplate($vcType, $data['vc_id']));
                if (isset($templateRes[self::OK_PARAM])) {
                    $payload = $this->_getCredentialBody($data['vc_id']);
                    return $this->_parseResponse($this->_createCredential($data, $payload));
                } else {
                    return $templateRes;
                }
            }
        }
        return $toRet;
    }

    private function _parseResponse(Response $res): array
    {
        if (!$res->isOk()) {
            $this->response = $this->response->withStatus($res->getStatusCode());
        }
        $toRet = json_decode($res->getStringBody(), true);
        if ($toRet) {
            if (!Configure::read('debug')) {
                unset($toRet['stacktrace']);
            }
            return $toRet;
        } else {
            $this->response = $this->response->withStatus(200);
            return [self::OK_PARAM => $res->getStringBody()];
        }
    }

    private function _addVcTemplate ($vcType, $vcId)
    {
        $endpoint = env('WALLET_ISSUER_IMPORT_TEMPLATE_ENDPOINT', '');
        if (!$endpoint) {
            throw new DetailedException('env WALLET_ISSUER_IMPORT_TEMPLATE_ENDPOINT is not defined', 500);
        }
        $vcTemplateImportEndpoint = $endpoint . '/' . $vcType;
        $domain = FullBaseUrl::host();
        $userLearningOutcomesVc = '{
          "type": [
            "VerifiableCredential",
            "VerifiableAttestation",
            "Verifiable'. $vcType. '"
         ],
         "@context": [
            "https://www.w3.org/2018/credentials/v1"
         ],
         "id": "",
         "issuer": "",
         "issuanceDate": "",
         "issued": "",
         "validFrom": "",
         "credentialSchema": {
           "id": "https://'.$domain.'/edu/api/v1/wallet/credentialSchemas/'. $vcId .'",
           "type": "JsonSchemaValidator2018"
         },
         "credentialSubject": {}
        }';
        $decode = json_decode($userLearningOutcomesVc);
        $encode = json_encode($decode);
        $options = ['headers' => ['Content-Type'=> 'application/json']];
        return $this->httpClient->post($vcTemplateImportEndpoint, $encode, $options);
    }

    private function _getCredentialBody($vcId): array
    {
        $user_id = $this->request->getParam('user_id');
        switch ($vcId) {
            case self::ASSESSMENT_SCORES:
                $res = $this->Submissions->getLastVcResults($user_id);
                $vc = new VcEuropass();
                return  $vc->setSubmission($res)->toArray();
            case self::GREAT:
                $vc = new VcProofOfResidence();
                return $vc->toArray();
            case self::HISTORY:
                $http = $this->httpClient;
                $bearer = $this->getRequest()->getHeader('AUTHORIZATION');
                $options = ['headers' => ['Authorization' => $bearer]];
                $url = 'https://' . $_SERVER['HTTP_HOST'] . '/api/v2/users/' . $user_id . '/bookings';
                $bookings = $http->get($url, [], $options)->getJson();
                $vc = new VcUserLearningOutcomes();
                return $vc->setBookings($bookings['data'])->toArray();
            default:
                throw new BadRequestException('Invalid vc_id');
        }
    }
}
