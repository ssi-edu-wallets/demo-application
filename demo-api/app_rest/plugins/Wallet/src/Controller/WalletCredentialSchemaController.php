<?php
declare(strict_types=1);

namespace Wallet\Controller;

use App\Controller\Api2Controller;
use Cake\Http\Exception\BadRequestException;
use Wallet\Lib\VCs\VcEuropass;
use Wallet\Lib\VCs\VcProofOfResidence;
use Wallet\Lib\VCs\VcUserLearningOutcomes;

class WalletCredentialSchemaController extends Api2Controller
{
    protected function getMandatoryParams(): array
    {
        return [];
    }

    public function getData($id)
    {
        switch ($id) {
            case WalletIssuanceController::ASSESSMENT_SCORES:
                $userLearningOutcomesSchema = new VcEuropass();
            break;
            case WalletIssuanceController::GREAT:
                $userLearningOutcomesSchema = new VcProofOfResidence();
                break;
            case WalletIssuanceController::HISTORY:
                $userLearningOutcomesSchema = new VcUserLearningOutcomes();
                break;
            default:
                throw new BadRequestException('Invalid schema id');
        }
        $this->return = $userLearningOutcomesSchema->vcSchema();
    }
}
