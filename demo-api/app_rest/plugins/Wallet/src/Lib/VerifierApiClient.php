<?php
declare(strict_types=1);

namespace Wallet\Lib;

use App\Lib\Exception\DetailedException;
use Cake\Http\Client;
use Cake\Http\Client\Response;
use Cake\Http\Exception\InternalErrorException;
use Cake\Http\ServerRequest;

class VerifierApiClient
{
    const ACCESS_TOKEN = 'access_token';
    const WALLET_TOKEN = 'wallet_token';
    private Client $httpClient;
    private ServerRequest $request;

    public function __construct(ServerRequest $request)
    {
        $this->request = $request;
        $this->httpClient = new Client();
    }

    public function getPresent(): string
    {
        $res = $this->httpClient->get($this->_getUrl('/present'));
        return $this->_parseRedirectResponse($res);
    }

    public function getAccessToken(string $url): string
    {
        $params = explode('?', $url)[1] ?? '';
        if (!$params) {
            throw new DetailedException('Redirection without query', 500);
        }
        $query = [];
        parse_str($params, $query);
        if (!isset($query[self::ACCESS_TOKEN])) {
            throw new DetailedException('Redirection without access_token', 500);
        }
        return $query[self::ACCESS_TOKEN];
    }

    private function _getAuthUrl(string $accessToken): string
    {
        return $this->_getEndpoint() . '/auth?'.self::ACCESS_TOKEN.'=' . $accessToken;
    }

    public function getCredential(array $data): array
    {
        $accessToken = $data[self::WALLET_TOKEN] ?? '';
        if (!$accessToken) {
            throw new DetailedException('Access_token is mandatory', 400);
        }
        $requestUrl = $this->_getAuthUrl($accessToken);
        $res = $this->httpClient->get($requestUrl);
        if (!$res->isOk()) {
            $msg = 'Invalid response from ' . $requestUrl . ' ' . $res->getStringBody();
            throw new InternalErrorException($msg, $res->getStatusCode());
        }
        $res = $res->getJson();
        if (!($res['isValid'] ?? false) || !($res['vps'] ?? false)) {
            throw new DetailedException('Credential is not valid', 400);
        }
        return $res;
    }

    public function postVerify($data): string
    {
        $options = ['type' => 'application/x-www-form-urlencoded'];
        $res = $this->httpClient->post($this->_getUrl('/verify'), $data, $options);
        return $this->_parseRedirectResponse($res);
    }

    private function _getEndpoint(): string
    {
        $url = env('WALLET_VERIFIER_ENDPOINT', '');
        if (!$url) {
            throw new InternalErrorException('WALLET_VERIFIER_ENDPOINT not defined');
        }
        return $url;
    }

    private function _getUrl(string $subPath): string
    {
        $url = $this->_getEndpoint();
        $params = $this->request->getQueryParams();
        $requestTarget = $this->request->getRequestTarget();
        $badParam = explode('?', $requestTarget)[0] ?? '';
        if ($badParam) {
            unset($params[$badParam]);
        }
        if ($params) {
            $params = '?' . http_build_query($params);
        } else {
            $params = '';
        }
        return $url . $subPath . $params;
    }

    private function _parseRedirectResponse(Response $res): string
    {
        if ($res->isRedirect()) {
            return $res->getHeaderLine('Location');
        } else {
            throw new DetailedException('Redirect expected got '.$res->getStatusCode() . ' ' . $res->getStringBody(), 500);
        }
    }
}
