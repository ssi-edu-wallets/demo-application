<?php
declare(strict_types=1);

namespace Wallet\Lib;

use App\Lib\Exception\DetailedException;
use Wallet\Lib\VCs\VcUserLearningOutcomes;
use Wallet\Lib\VCs\VcVerifiableId;

class CredentialParser
{

    private array $cred;

    public function __construct(array $credential)
    {
        $this->cred = $credential;
    }

    public function store($userId): array
    {
        $vcs = $this->cred['vps'][0]['vcs'];
        if (!$vcs) {
            throw new DetailedException('Invalid VPs format', 400);
        }
        if (isset($vcs[1]) || !isset($vcs[0])) {
            throw new DetailedException('Invalid only one VC expected', 400);
        }
        $types = $vcs[0]['type'] ?? [];
        $v = 'Verifiable';
        if (in_array($v.VcVerifiableId::getType(), $types)) {
            $vc = new VcVerifiableId();
            $vc->createUserAccount();
        } else if (in_array($v.VcUserLearningOutcomes::getType(), $types)) {
            $vc = new VcUserLearningOutcomes();
            $vc->storeInProfile($userId, $vcs[0]);
        } else {
            throw new DetailedException('Unknown VC type', 400);
        }
        return $this->cred['vps'];
    }
}
