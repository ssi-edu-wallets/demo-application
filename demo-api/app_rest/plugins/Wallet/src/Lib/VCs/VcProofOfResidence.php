<?php
declare(strict_types=1);

namespace Wallet\Lib\VCs;

class VcProofOfResidence implements VcInterface
{

    public static function getType(): string
    {
        return 'ProofOfResidence';
    }

    private function _template(array $subject): array
    {
        return [
            'credentials' => [
                [
                    'credentialData' => [
                        'credentialSubject' => $subject
                    ],
                    'type' => $this->getType()
                ]
            ]
        ];
    }
    public function toArray(): array
    {
        $subject = [
            'id' => '',
            'address' => [
                [
                    'countryName' => 'ES',
                    'locality' => 'Madrid',
                    'postalCode' => '33314',
                    'streetAddress' => 'XX Nº14'
                ]
            ],
            'dateOfBirth' => '1981-07-29',
            'familyName' => 'Ramon',
            'familyStatus' => 'Single',
            'firstNames' => 'Fernando',
            'gender' => 'Male',
            'identificationNumber' => '31433',
            'nationality' => 'ES'
        ];
        return $this->_template($subject);
    }
    /*
     * Cloned schema from https://raw.githubusercontent.com/walt-id/waltid-ssikit-vclib/master/src/test/resources/schemas/ProofOfResidence.json
     */
    public function vcSchema(): array
    {
        return [
            '$schema' => 'http://json-schema.org/draft-07/schema#',
            'type' => 'object',
            'properties' => [
                '@context' => [
                    'type' => 'array',
                    'items' => [
                        'type' => 'string'
                    ]
                ],
                'credentialSchema' => [
                    'type' => 'object',
                    'properties' => [
                        'id' => [
                            'type' => 'string'
                        ],
                        'type' => [
                            'type' => 'string'
                        ]
                    ],
                    'additionalProperties' => false
                ],
                'credentialStatus' => [
                    'type' => 'object',
                    'properties' => [
                        'id' => [
                            'type' => 'string'
                        ],
                        'type' => [
                            'type' => 'string'
                        ]
                    ],
                    'additionalProperties' => false
                ],
                'credentialSubject' => [
                    'type' => 'object',
                    'properties' => [
                        'address' => [
                            'type' => 'object',
                            'properties' => [
                                'countryName' => [
                                    'type' => 'string'
                                ],
                                'locality' => [
                                    'type' => 'string'
                                ],
                                'postalCode' => [
                                    'type' => 'string'
                                ],
                                'streetAddress' => [
                                    'type' => 'string'
                                ]
                            ],
                            'additionalProperties' => false
                        ],
                        'dateOfBirth' => [
                            'type' => 'string'
                        ],
                        'familyName' => [
                            'type' => 'string'
                        ],
                        'familyStatus' => [
                            'type' => 'string'
                        ],
                        'firstNames' => [
                            'type' => 'string'
                        ],
                        'gender' => [
                            'type' => 'string'
                        ],
                        'id' => [
                            'type' => 'string'
                        ],
                        'identificationNumber' => [
                            'type' => 'string'
                        ],
                        'nationality' => [
                            'type' => 'string'
                        ]
                    ],
                    'additionalProperties' => false
                ],
                'evidence' => [
                    'type' => 'array',
                    'items' => [
                        'type' => 'object',
                        'properties' => [
                            'documentPresence' => [
                                'type' => 'string'
                            ],
                            'evidenceDocument' => [
                                'type' => 'string'
                            ],
                            'id' => [
                                'type' => 'string'
                            ],
                            'subjectPresence' => [
                                'type' => 'string'
                            ],
                            'type' => [
                                'type' => 'array',
                                'items' => [
                                    'type' => 'string'
                                ]
                            ],
                            'verifier' => [
                                'type' => 'string'
                            ]
                        ],
                        'additionalProperties' => false
                    ]
                ],
                'expirationDate' => [
                    'type' => 'string'
                ],
                'id' => [
                    'type' => 'string'
                ],
                'issuanceDate' => [
                    'type' => 'string'
                ],
                'issued' => [
                    'type' => 'string'
                ],
                'issuer' => [
                    'type' => 'string'
                ],
                'proof' => [
                    'type' => 'object',
                    'properties' => [
                        'created' => [
                            'type' => 'string'
                        ],
                        'creator' => [
                            'type' => 'string'
                        ],
                        'domain' => [
                            'type' => 'string'
                        ],
                        'jws' => [
                            'type' => 'string'
                        ],
                        'nonce' => [
                            'type' => 'string'
                        ],
                        'proofPurpose' => [
                            'type' => 'string'
                        ],
                        'type' => [
                            'type' => 'string'
                        ],
                        'verificationMethod' => [
                            'type' => 'string'
                        ]
                    ],
                    'additionalProperties' => false
                ],
                'title' => [
                    'type' => 'string'
                ],
                'type' => [
                    'type' => 'array',
                    'items' => [
                        'type' => 'string'
                    ]
                ],
                'validFrom' => [
                    'type' => 'string'
                ]
            ],
            'required' => ['@context', 'type'],
            'additionalProperties' => false
        ];
    }
}
