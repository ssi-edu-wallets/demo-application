<?php
declare(strict_types=1);

namespace Wallet\Lib\VCs;

use App\Lib\Exception\DetailedException;
use Profile\Lib\Consts\ProfileSkillStatus;
use Profile\Model\Entity\ProfilesSkill;
use Profile\Model\Entity\UserProfile;
use Profile\Model\Table\UserProfilesTable;

class VcUserLearningOutcomes implements VcInterface
{
    private $_bookings;

    public function setBookings(array $bookings): VcUserLearningOutcomes
    {
        $this->_bookings = $bookings;
        return $this;
    }
    public static function getType(): string
    {
        return 'UserLearningOutcomes';
    }
    private function _template(array $subject): array
    {
        return [
            'credentials' => [
                [
                    'credentialData' => [
                        'credentialSubject' => $subject
                    ],
                    'type' => $this->getType()
                ]
            ]
        ];
    }
    private function _learningActivitySpecification($learningActivity): array
    {
        $activitySpecifications = [];
        foreach ($learningActivity as $activitySpecification) {
            $activity = [
                'name' => $activitySpecification['display_name']
            ];
            if ($activitySpecification['esco_uri'] ?? '') {
                $activity['relatedESCOSkill'] = $activitySpecification['esco_uri'];
            }
            array_push($activitySpecifications, $activity);
        }
        return $activitySpecifications;
    }
    private function _learningActivity(): array
    {
        $learningActivities = [];
        foreach ($this->_bookings as $learningActivity) {
            $activity = [
                'title' => $learningActivity['service']['title']
            ];

            if ($learningActivity['started']) {
                $activity['startedAtTime'] = $learningActivity['started'];
            }

            if ($learningActivity['completed']) {
                $activity['endedAtTime'] = $learningActivity['completed'];
            }

            if ($learningActivity['service']['tag_list'] ?? []) {
                $learningOutcomes = $this->_learningActivitySpecification($learningActivity['service']['tag_list']);
                $activity['specifiedBy']['teaches'] = [];
                foreach ($learningOutcomes as $learningOutcome) {
                    array_push($activity['specifiedBy']['teaches'], ['learningOutcome' => $learningOutcome]);
                }
            }
            array_push($learningActivities, $activity);
        }
        return $learningActivities;
    }
    public function toArray(): array
    {
        $subject = [
            'id' => '',
            'title' => 'User learning outcomes',
            'performed' => $this->_learningActivity()
        ];
        return $this->_template($subject);
    }
    public function storeInProfile($userId, $vc)
    {
        $Profiles = UserProfilesTable::load();
        /** @var UserProfile $toSave */
        $toSave = $Profiles->newEmptyEntity();
        $toSave->user_id = $userId;
        $profile = $Profiles->findUserProfileByUserId($userId)->first();
        if ($profile) {
            $toSave->current_occupation = $profile->current_occupation;
            $toSave->target_occupation = $profile->target_occupation;
            $toSave->main_goal = $profile->main_goal;
            $toSave->goal_description = $profile->goal_description;
            $toSave->industry = $profile->industry;
            $toSave->profiles_skills = $profile->profiles_skills;
        }
        if (!isset($vc['credentialSubject']['performed'][0])) {
            throw new DetailedException('Invalid credential schema');
        }
        $skills = [];
        /** @var ProfilesSkill $skill */
        foreach ($toSave->getProfilesSkills() as $skill) {
            $skills[$skill->skill] = [
                'skill' => $skill->skill,
                'esco_uri' => $skill->esco_uri,
                'status' => $skill->status,
            ];
        }
        $titles = [];
        foreach ($vc['credentialSubject']['performed'] as $learningActivity) {
            if ($learningActivity['title'] ?? false) {
                $titles[$learningActivity['title']] = $learningActivity['title'];
            }
            $teaches = $learningActivity['specifiedBy']['teaches'] ?? [];
            foreach ($teaches as $tag) {
                $outcome = $tag['learningOutcome'] ?? false;
                if ($outcome) {
                    $skills[$outcome['name']] = [
                        'skill' => $outcome['name'],
                        'esco_uri' => $outcome['relatedESCOSkill'] ?? '',
                        'status' => ProfileSkillStatus::HAVE,
                    ];
                }
            }
        }
        if ($titles) {
            if ($toSave->goal_description) {
                $toSave->goal_description .= "\n" . "\n";
            }
            $toSave->goal_description .= __('I have participated in ') . implode(', ', $titles) .  '.';
            if ($skills) {
                $data = ['profiles_skills' => $skills];
                $Profiles->patchEntity($toSave, $data, ['associated' => ['ProfilesSkills']]);
            }
            $Profiles->addDeletingOldOrFail($toSave);
        }
    }
    public function vcSchema(): array
    {
        return [
            '$schema' => 'https://json-schema.org/draft/2020-12/schema',
            'title' => 'User learning outcomes verifiable accreditation',
            'description' => 'Schema of a user learning outcomes verifiable accreditation',
            'type' => 'object',
            'allOf' => [
                [
                    '$ref' => 'https://api-pilot.ebsi.eu/trusted-schemas-registry/v2/schemas/0xeb6d8131264327f3cbc5ddba9c69cb9afd34732b3b787e4b3e3507a25d3079e9'
                ],
                [
                    'properties' => [
                        'credentialSubject' => [
                            'description' => 'Defines additional properties on credentialSubject to describe the body of the verifiable credential',
                            'type' => 'object',
                            'properties' => [
                                'id' => [
                                    'description' => 'Defines the did of the credential subject',
                                    'type' => 'string'
                                ],
                                'title' => [
                                    'description' => 'Title of the credential subject',
                                    'type' => 'string'
                                ],
                                'performed' => [
                                    'description' => 'Defines the learning activity that a person participated in or attended',
                                    'type' => 'array',
                                    'items' => [
                                        '$ref' => '#/$defs/performed'
                                    ]
                                ]
                            ],
                            'required' => [
                                'id',
                                'title',
                                'performed'
                            ]
                        ]
                    ]
                ]
            ],
            '$defs' => [
                'performed' => [
                    'description' => 'Defines the learning activity that a person participated in or attended',
                    'type' => 'object',
                    'properties' => [
                        'title' => [
                            'description' => 'Defines a title of the learning achievement',
                            'type' => 'string'
                        ],
                        'startedAtTime' => [
                            'description' => 'The date the learner started the activity',
                            'type' => 'DateTime'
                        ],
                        'endedAtTime' => [
                            'description' => 'The date the learner ended the activity',
                            'type' => 'DateTime'
                        ],
                        'specifiedBy' => [
                            'definition' => 'The specification of this learning activity',
                            'type' => 'object',
                            'properties' => [
                                'teaches' => [
                                    'definition' => 'The expected learning outcomes this learning activity specification can lead or contribute to',
                                    'type' => 'array',
                                    'items' => [
                                        '$ref' => '#/$defs/teaches'
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'required' => [
                        'title'
                    ]
                ],
                'teaches' => [
                    'definition' => 'The expected learning outcomes this learning activity specification can lead or contribute to',
                    'type' => 'object',
                    'properties' => [
                        'learningOutcome' => [
                            'description' => 'The learning outcome of the learning specification',
                            'type' => 'object',
                            'properties' => [
                                'name' => [
                                    'description' => 'A legible, descriptive name for the learning outcome',
                                    'type' => 'string'
                                ],
                                'relatedESCOSkill' => [
                                    'description' => 'A URI to the related ESCO Skill',
                                    'type' => 'string'
                                ]
                            ],
                            'required' => [
                                'name'
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }
}
