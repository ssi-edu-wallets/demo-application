<?php
declare(strict_types=1);

namespace Wallet\Lib\VCs;

interface VcInterface
{
    public static function getType(): string;
    public function toArray(): array;
    public function vcSchema(): array;
}
