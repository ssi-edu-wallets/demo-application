<?php
declare(strict_types=1);

namespace Wallet\Lib\VCs;

class VcVerifiableId implements VcInterface
{
    public static function getType(): string
    {
        return 'Id';
    }
    public function toArray(): array
    {
        return [];
    }
    public function createUserAccount()
    {

    }
    public function vcSchema(): array
    {
        return [];
    }
}
