<?php
declare(strict_types=1);

namespace Wallet;

use App\Controller\Api2Controller;
use Cake\Core\BasePlugin;
use Cake\Routing\RouteBuilder;

class WalletPlugin extends BasePlugin
{
    const ROUTE_PREFIX = Api2Controller::ROUTE_PREFIX . '/wallet';

    public function routes(RouteBuilder $routes): void
    {
        $routes->plugin(
            $this->name,
            ['path' => WalletPlugin::ROUTE_PREFIX],
            function (RouteBuilder $builder) {
                $builder->connect('/users/{user_id}/issuance/*', \Wallet\Controller\WalletIssuanceController::route());
                $builder->connect('/credentialSchemas/*', \Wallet\Controller\WalletCredentialSchemaController::route());
                $builder->connect('/verifier/present/*', \Wallet\Controller\VerifierPresentProxyController::route());
                $builder->connect('/verifier/verify/*', \Wallet\Controller\VerifierVerifyProxyController::route());
                $builder->connect('/verifier/store/*', \Wallet\Controller\VerifierStoreController::route());
            }
        );
        parent::routes($routes);
    }
}
