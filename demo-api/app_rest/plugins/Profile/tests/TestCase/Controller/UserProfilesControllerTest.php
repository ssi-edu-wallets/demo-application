<?php
declare(strict_types=1);

namespace Profile\Test\TestCase\Controller;

use App\Controller\Api2Controller;
use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\UsersFixture;
use App\Test\TestCase\Controller\Api2CommonErrorsTest;
use Cake\TestSuite\Fixture\FixtureStrategyInterface;
use Cake\TestSuite\Fixture\TruncateStrategy;
use Profile\Lib\Consts\MainGoals;
use Profile\Lib\Consts\ProfileSkillStatus;
use Profile\Model\Table\UserProfilesTable;
use Profile\Test\Fixture\ProfilesSkillsFixture;
use Profile\Test\Fixture\UserProfilesFixture;

class UserProfilesControllerTest extends Api2CommonErrorsTest
{
    protected $fixtures = [
        UsersFixture::LOAD,
        OauthAccessTokensFixture::LOAD,
        UserProfilesFixture::LOAD,
        ProfilesSkillsFixture::LOAD
    ];

    protected function getFixtureStrategy(): FixtureStrategyInterface
    {
        return new TruncateStrategy();
    }
    protected function _getEndpoint(): string
    {
        return Api2Controller::ROUTE_PREFIX. '/profiles/';
    }

    public function testGetSellerProfile()
    {
        $expectedData = [
            'id' => 1,
            'user_id' => 2,
            'current_occupation' => 'Occupation 1',
            'target_occupation' => 'Programmer',
            'main_goal' => 'grow_current_role',
            'goal_description' => 'Occupation goal 1',
            'profile' => 'Profile of the user with the id 2',
            'industry' => 'IT',
            'profiles_skills' => [
                [
                    'id' => 1,
                    'skill' => 'Spacial vision',
                    'esco_uri' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525',
                    'status' => ProfileSkillStatus::WANT,
                ],
                [
                    'id' => 2,
                    'skill' => 'Logic',
                    'esco_uri' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525',
                    'status' => ProfileSkillStatus::HAVE,
                ],
            ],
            'created' => '2021-01-18T10:39:23+00:00',
            'modified' => '2021-01-18T10:41:31+00:00'
        ];

        $this->get($this->_getEndpoint(). UsersFixture::SELLER_ID);

        $this->assertJsonResponseOK();
        $return = json_decode($this->_getBodyAsString(), true)['data'];;
        $this->assertEquals($expectedData, $return);
    }

    public function testGetBuyerProfile_shouldThrowForbiddenException()
    {
        $this->get($this->_getEndpoint(). UsersFixture::BUYER_ID);
        $this->assertException('Forbidden', 403, 'Resource not allowed with this token');
    }
    public function testAddNewSellerProfileAndSoftDeleteIfAlreadyHasProfiles()
    {
        $profilesSkills = $this->_getNewProfilesSkills();
        $data = [
            'user_id' => 2,
            'current_occupation' => 'Frontend developer',
            'target_occupation' => 'Backend developer',
            'main_goal' => MainGoals::SWITCH_ROLE,
            'goal_description' => 'become a backend developer',
            'industry' => 'IT',
            'profiles_skills' => $profilesSkills
        ];
        $expected = [
            'id' => 4,
            'user_id' => 2,
            'current_occupation' => 'Frontend developer',
            'main_goal' => MainGoals::SWITCH_ROLE,
            'goal_description' => 'become a backend developer',
            'profile' => null,
            'target_occupation' => 'Backend developer',
            'industry' => 'IT',
            'profiles_skills' => $profilesSkills
        ];
        $this->post($this->_getEndpoint(), $data);
        $body = $this->assertJsonResponseOK()['data'];
        unset($body['created']);
        unset($body['modified']);
        $this->assertEquals($expected, $body);
    }

    public function testAddNewSellerProfile_shouldThrowForbiddenException()
    {
        $data = [
            'user_id' => 3,
            'current_occupation' => 'Occupation 4',
            'goal_description' => 'Occupation goal 4',
            'profile' => 'Profile of the user with the id 2'
        ];

        $this->post($this->_getEndpoint(), $data);
        $this->assertException('Forbidden', 403, 'Resource not allowed with this token');
    }
    public function testDeleteSellerProfile()
    {
        $this->delete($this->_getEndpoint(). UsersFixture::SELLER_ID);
        $this->assertResponseOk($this->_getBodyAsString());

        $profile = UserProfilesTable::load()->findUserProfileByUserId(UsersFixture::SELLER_ID)
            ->first();

        $this->assertNull($profile);
    }

    public function testDeleteSellerProfile_shouldThrowForbiddenException()
    {
        $this->delete($this->_getEndpoint() . UsersFixture::BUYER_ID);
        $this->assertException('Forbidden', 403, 'Resource not allowed with this token');
    }

    private function _getNewProfilesSkills(): array
    {
        return [
            [
                'id' => 3,
                'skill' => 'PHP 8',
                'esco_uri' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525',
                'status' => ProfileSkillStatus::WANT,
            ],
            [
                'id' => 4,
                'skill' => 'VueJS',
                'esco_uri' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525',
                'status' => ProfileSkillStatus::HAVE,
            ],
            [
                'id' => 5,
                'skill' => 'SQL',
                'esco_uri' => 'http://data.europa.eu/esco/occupation/8b6388a4-4904-471b-9331-d3b1211f5525',
                'status' => ProfileSkillStatus::WANT,
            ],
        ];
    }
}
