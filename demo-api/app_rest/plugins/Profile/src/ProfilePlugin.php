<?php
declare(strict_types=1);

namespace Profile;

use App\Controller\Api2Controller;
use Cake\Core\BasePlugin;
use Cake\Routing\RouteBuilder;

class ProfilePlugin extends BasePlugin
{

    public function routes(RouteBuilder $routes): void
    {
        $routes->plugin(
            $this->name,
            ['path' => Api2Controller::ROUTE_PREFIX],
            function (RouteBuilder $builder) {
                $builder->connect('/profiles/*', \Profile\Controller\UserProfilesController::route());
            }
        );
        parent::routes($routes);
    }
}
