<?php
declare(strict_types=1);

namespace Profile\Model\Table;

use App\Model\Table\AppTable;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Profile\Model\Entity\UserProfile;

class UserProfilesTable extends AppTable
{
    public static function load(): self
    {
        /** @var self $table */
        $table = parent::load();
        return $table;
    }

    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');
        $this->hasMany('ProfilesSkills',
            [
                'className' => ProfilesSkillsTable::nameWithPlugin()
            ])
            ->setForeignKey('user_profile_id');
    }

    public function findUserProfileByUserId($userId) : Query
    {
        return $this->find()
            ->where(['user_id'=> $userId])
            ->contain('ProfilesSkills');
    }

    public function addDeletingOldOrFail(UserProfile $toSave)
    {
        $connection = ConnectionManager::get('default');
        $connection->transactional(function () use ($toSave) {
            $this->deleteAllByUser($toSave->user_id);
            $this->saveOrFail($toSave);
        });
    }

    public function deleteAllByUser($uid): int
    {
        return $this->updateAll(['deleted' => date('Y-m-d H:i:s')], ['user_id' => $uid]);
    }

}
