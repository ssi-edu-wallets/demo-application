<?php
declare(strict_types=1);

namespace Profile\Model\Table;

use App\Model\Table\AppTable;

class ProfilesSkillsTable extends AppTable
{
    public static function load(): self
    {
        /** @var self $table */
        $table = parent::load();
        return $table;
    }

    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');
        $this->belongsTo('UserProfiles',
            [
                'className' => UserProfilesTable::nameWithPlugin()
            ])
            ->setForeignKey('user_profile_id');
    }
}
