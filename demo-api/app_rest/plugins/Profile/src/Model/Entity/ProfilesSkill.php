<?php
declare(strict_types=1);

namespace Profile\Model\Entity;

use Cake\ORM\Entity;

/**
 * @property integer $user_profile_id
 * @property string $skill
 * @property string $esco_uri
 * @property string $status
 */
class ProfilesSkill extends Entity
{
    protected $_accessible = [
        '*' => false,
        'id' => false,

        'skill' => true,
        'esco_uri' => true,
        'status' => true,
    ];

    protected $_hidden = [
        'user_profile_id',
        'created',
        'modified',
        'deleted'
    ];

    protected $_virtual = [
    ];
}
