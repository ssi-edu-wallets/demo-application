<?php
declare(strict_types=1);

namespace Profile\Lib\Consts;

class ProfileSkillStatus
{
    const WANT = 'want';
    const HAVE = 'have';
}
