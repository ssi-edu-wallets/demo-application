<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddSubSsoId extends AbstractMigration
{
    public function change()
    {
        $this->table(\App\Model\Table\AppTable::TABLE_PREFIX . 'users', ['collation' => 'utf8mb4_general_ci'])
            ->addColumn('sub_sso_id', 'string', [
                'default' => null,
                'after' => 'access_level',
                'null' => true,
            ])
            ->update();
    }
}
